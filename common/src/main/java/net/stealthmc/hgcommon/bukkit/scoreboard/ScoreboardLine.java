package net.stealthmc.hgcommon.bukkit.scoreboard;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.Team;

import java.util.concurrent.atomic.AtomicBoolean;

@Getter(AccessLevel.PACKAGE)
public class ScoreboardLine {

	private final Team team;
	private final ScoreboardLineManager lineManager;

	@Setter(AccessLevel.PACKAGE)
	private int score;

	private AtomicBoolean disposed = new AtomicBoolean(false);

	public ScoreboardLine(ScoreboardLineManager manager, Team team, int score) {
		this.lineManager = manager;
		this.team = team;
		this.score = score;
	}

	public ScoreboardLine update(String line) {
		if (disposed.get()) {
			throw new IllegalStateException("This scoreboard line has been disposed!");
		}
		if (line.length() > 32) {
			throw new IllegalArgumentException("Line length is too big! Max 32, was " + line.length());
		}
		boolean len16 = line.length() > 16;
		team.setPrefix(line.substring(0, len16 ? 16 : line.length()));
		if (!len16) {
			team.setSuffix("");
			return this;
		}
		team.setSuffix(line.substring(16));
		return this;
	}

	public void dispose() {
		if (isDisposed()) {
			return;
		}

		lineManager.getScoreboard().getScoreboard().resetScores(lineManager.getTeamEntry(getTeam()));
		team.unregister();
		disposed.set(true);

		//call after disposed has been set to true to avoid infinite recursion!
		lineManager.disposeLine(this);
	}

	public boolean isDisposed() {
		return disposed.get();
	}

	/**
	 * Checks if this line is effectively empty, i.e. if the display string is empty.
	 *
	 * @return true if empty
	 */
	public boolean isEmpty() {
		return ChatColor.stripColor(getText()).isEmpty();
	}

	/**
	 * Returns the text this line is displaying. This consists of the underlying's team prefix, entry and suffix.
	 *
	 * @return the resulting text
	 */
	public String getText() {
		return team.getPrefix() + team.getEntries().stream().findAny().orElse("") + team.getSuffix();
	}

	/**
	 * Returns the line directly above this line. If it did not exist, it will be created.
	 *
	 * @return the line above.
	 */
	public ScoreboardLine getAbove() {
		return lineManager.getLine(score + 1);
	}

	/**
	 * Inserts a line above this line. The line returned will be a blank line above this line.
	 *
	 * @return a blank line above this line
	 */
	public ScoreboardLine insertAbove() {
		lineManager.incrementScoreAbove(score);
		return getAbove();
	}

	/**
	 * Returns the first line after a closed block of coherent lines above this line.
	 *
	 * @return the free line.
	 */
	public ScoreboardLine aboveBlock() {
		int ret = score + 1;
		while (lineManager.existsLine(ret)) {
			++ret;
		}
		return lineManager.getLine(ret);
	}

	/**
	 * Makes a blank line above the current line, if the line directly above this line is not blank.
	 * Returns itself.
	 *
	 * @return the object
	 */
	public ScoreboardLine makeSpaceAbove() {
		String text = getAbove().getText();
		if (ChatColor.stripColor(text).isEmpty()) {
			return this;
		}
		insertAbove();
		return this;
	}

	/**
	 * Makes a blank line under the current line, if the line directly below this line does not exist.
	 * Returns itself.
	 *
	 * @return the object
	 */
	public ScoreboardLine makeSoftSpaceAbove() {
		if (lineManager.existsLine(score + 1)) {
			return this;
		}
		insertAbove();
		return this;
	}

	/**
	 * Returns the line below this line. If it did not exist, it will be created.
	 *
	 * @return the line below
	 */
	public ScoreboardLine getBelow() {
		return lineManager.getLine(score - 1);
	}

	/**
	 * Inserts a line below this line. The line returned will be a blank line below this line.
	 *
	 * @return a blank line below this line
	 */
	public ScoreboardLine insertBelow() {
		lineManager.decrementScoreBelow(score);
		return getBelow();
	}

	/**
	 * Returns the first line after a closed block of coherent lines below this lone.
	 *
	 * @return the free line.
	 */
	public ScoreboardLine belowBlock() {
		int ret = score - 1;
		while (lineManager.existsLine(ret)) {
			--ret;
		}
		return lineManager.getLine(ret);
	}

	/**
	 * Makes a blank line under the current line, if the line directly below this line is not blank.
	 * Returns itself.
	 *
	 * @return the object
	 */
	public ScoreboardLine makeSpaceBelow() {
		if (getBelow().isEmpty()) {
			return this;
		}
		insertBelow();
		return this;
	}


	/**
	 * Makes a blank line under the current line, if the line directly below this line does not exist.
	 * Returns itself.
	 *
	 * @return the object
	 */
	public ScoreboardLine makeSoftSpaceBelow() {
		if (lineManager.existsLine(score - 1)) {
			return this;
		}
		insertBelow();
		return this;
	}

	public int getLineNumber() {
		return score;
	}

}
