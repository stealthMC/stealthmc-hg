package net.stealthmc.hgcommon.bukkit.scoreboard;

import com.google.common.collect.Sets;
import lombok.AccessLevel;
import lombok.Getter;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.Set;
import java.util.UUID;

@Getter(AccessLevel.PACKAGE)
public class PlayerScoreboard {

	private final Scoreboard scoreboard;
	private final Objective objective;

	@Getter
	private final ScoreboardLineManager lineManager;

	private Set<UUID> playerSet = Sets.newHashSet();

	public PlayerScoreboard(String title, Player... players) {
		scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		this.objective = scoreboard.registerNewObjective("bhk_sb", "dummy", "");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(PlayerUtils.colorize(title));
		for (Player player : players) {
			player.setScoreboard(scoreboard);
			playerSet.add(player.getUniqueId());
		}
		lineManager = new ScoreboardLineManager(this);
	}

	public void updateTitle(String name) {
		objective.setDisplayName(PlayerUtils.colorize(name));
	}

	public ScoreboardLine getLine(int score) {
		return lineManager.getLine(score);
	}

	public void dispose() {
		lineManager.dispose();
		objective.unregister();
		PlayerUtils.forEveryUUIDPlayer(playerSet,
				player -> player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard()));
	}

}
