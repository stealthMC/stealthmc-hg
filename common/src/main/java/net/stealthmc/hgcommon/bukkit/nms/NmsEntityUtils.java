package net.stealthmc.hgcommon.bukkit.nms;

import lombok.Getter;
import lombok.experimental.UtilityClass;
import org.bukkit.Location;
import org.bukkit.entity.Entity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@UtilityClass
public class NmsEntityUtils {

	@Getter
	private Class<?> entityClass;
	private Method entitySetInvisibleMethod; //Entity#setInvisible(boolean)
	private Method entitySetCustomNameMethod; //Entity#setCustomName(String)
	private Method entitySetCustomNameVisibleMethod; //Entity#setCustomNameVisible(boolean)
	private Method entitySetLocationMethod; //Entity#setLocation(double, double, double, float, float)
	private Method entityGetIdMethod; //Entity#getId()
	private Method entityGetDataWatcherMethod; //Entity#getDataWatcher
	private Method entitySetSilentMethod; //Entity#b(boolean)

	private Class<?> craftEntityClass;
	private Class<?> entityTypesClass;
	private Method craftEntityGetHandleMethod; //CraftEntity#getHandle()

	private Class<?> iChatBaseComponentClass;
	private Class<?> chatMessageClass;
	private Constructor<?> chatMessageConstructor;

	@Getter
	private Class<?> entityLivingClass;

	private Class<?> entityEnderDragonClass;
	private Object entityEnderDragonType;
	private Constructor<?> entityEnderDragonConstructor;

	private Class<?> entityWitherClass;
	private Object entityWitherType;
	private Constructor<?> entityWitherConstructor;

	@Getter
	private Class<?> dataWatcherClass;

	static {
		try {
			entityClass = NmsUtils.getNMSClass("Entity");
			entityTypesClass = NmsUtils.getNMSClass("EntityTypes");
			entityLivingClass = NmsUtils.getNMSClass("EntityLiving");
			entityEnderDragonClass = NmsUtils.getNMSClass("EntityEnderDragon");
			entityWitherClass = NmsUtils.getNMSClass("EntityWither");
			dataWatcherClass = NmsUtils.getNMSClass("DataWatcher");
			craftEntityClass = NmsUtils.getCraftBukkitClass("entity.CraftEntity");
			iChatBaseComponentClass = NmsUtils.getNMSClass("IChatBaseComponent");
			chatMessageClass = NmsUtils.getNMSClass("ChatMessage");

			chatMessageConstructor = chatMessageClass.getConstructor(String.class, Object[].class);

			entityEnderDragonConstructor = entityEnderDragonClass.getConstructor(entityTypesClass, NmsWorldUtils.getNmsWorldClass());
			entityEnderDragonType = entityTypesClass.getDeclaredField("ENDER_DRAGON").get(null);

			entityWitherConstructor = entityWitherClass.getConstructor(entityTypesClass, NmsWorldUtils.getNmsWorldClass());
			entityWitherType = entityTypesClass.getDeclaredField("WITHER").get(null);

			entitySetInvisibleMethod = entityClass.getMethod("setInvisible", boolean.class);
			entitySetCustomNameMethod = entityClass.getMethod("setCustomName", iChatBaseComponentClass);
			entitySetCustomNameVisibleMethod = entityClass.getMethod("setCustomNameVisible", boolean.class);
			entitySetLocationMethod = entityClass.getMethod("setLocation", double.class, double.class, double.class, float.class, float.class);
			entityGetIdMethod = entityClass.getMethod("getId");
			entityGetDataWatcherMethod = entityClass.getMethod("getDataWatcher");
			entitySetSilentMethod = entityClass.getMethod("setSilent", boolean.class);
			craftEntityGetHandleMethod = craftEntityClass.getMethod("getHandle");
		} catch (ClassNotFoundException | NoSuchMethodException | NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public @Nullable
	Object newVirtualEnderdragon(@Nonnull Object nmsWorld) {
		try {
			return entityEnderDragonConstructor.newInstance(entityEnderDragonType, nmsWorld);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	public @Nullable
	Object newVirtualWither(@Nonnull Object nmsWorld) {
		try {
			return entityWitherConstructor.newInstance(entityWitherType, nmsWorld);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	public @Nullable
	Integer getEntityId(@Nonnull Object entity) {
		try {
			return (Integer) entityGetIdMethod.invoke(entity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setCustomName(@Nonnull Object entity, @Nonnull String customName) {
		try {
			entitySetCustomNameMethod.invoke(entity, chatMessageConstructor.newInstance(customName));
		} catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
			e.printStackTrace();
		}
	}

	public void setCustomNameVisible(@Nonnull Object entity, boolean visible) {
		try {
			entitySetCustomNameVisibleMethod.invoke(entity, visible);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Important: Ignores world in location parameter.
	 *
	 * @param entity   the entity
	 * @param location the location
	 */
	public void setLocation(@Nonnull Object entity, @Nonnull Location location) {
		try {
			entitySetLocationMethod.invoke(entity, location.getX(), location.getY(), location.getZ(),
					location.getPitch(), location.getYaw());
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public void setInvisible(@Nonnull Object entity, boolean invisible) {
		try {
			entitySetInvisibleMethod.invoke(entity, invisible);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public @Nullable
	Object getEntityDataWatcher(@Nonnull Object entity) {
		try {
			return entityGetDataWatcherMethod.invoke(entity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setSilent(@Nonnull Object entity, boolean silent) {
		try {
			entitySetSilentMethod.invoke(entity, silent);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public Object craftEntityGetHandle(@Nonnull Entity entity) {
		try {
			return craftEntityGetHandleMethod.invoke(entity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}

}
