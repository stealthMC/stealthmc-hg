package net.stealthmc.hgcommon.bukkit.nms;

import lombok.experimental.UtilityClass;
import org.bukkit.Bukkit;
import org.bukkit.Server;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@UtilityClass
public class NmsServerUtils {

	private Class<?> craftServerClass;
	private Method craftServerGetHandleMethod;

	private Class<?> dedicatedPlayerListClass;
	private Method dedicatedPlayerListGetServerMethod;

	private Class<?> minecraftServerClass;
	private Method minecraftServerSetMotdMethod;

	static {
		try {
			craftServerClass = NmsUtils.getCraftBukkitClass("CraftServer");
			craftServerGetHandleMethod = craftServerClass.getMethod("getHandle");

			dedicatedPlayerListClass = NmsUtils.getNMSClass("DedicatedPlayerList");
			dedicatedPlayerListGetServerMethod = dedicatedPlayerListClass.getMethod("getServer");

			minecraftServerClass = NmsUtils.getNMSClass("MinecraftServer");
			minecraftServerSetMotdMethod = minecraftServerClass.getMethod("setMotd", String.class);
		} catch (ClassNotFoundException | NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public void setMotd(String motd) {
		Server server = Bukkit.getServer();
		Object craftServer = craftServerClass.cast(server);

		try {
			Object dedicatedPlayerList = craftServerGetHandleMethod.invoke(craftServer);
			Object minecraftServer = dedicatedPlayerListGetServerMethod.invoke(dedicatedPlayerList);
			minecraftServerSetMotdMethod.invoke(minecraftServer, motd);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

}
