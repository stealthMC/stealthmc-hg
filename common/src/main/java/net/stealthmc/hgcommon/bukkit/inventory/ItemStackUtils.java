package net.stealthmc.hgcommon.bukkit.inventory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import lombok.experimental.UtilityClass;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class ItemStackUtils {

	public final ItemStack INVENTORY_DEFAULT_ITEM = createItemStack(Material.LIGHT_GRAY_STAINED_GLASS_PANE, ChatColor.BLACK + "");
	public final ItemStack INVENTORY_DARK_ITEM = createItemStack(Material.BLACK_STAINED_GLASS_PANE, ChatColor.BLACK + "");
	public final ItemStack INVENTORY_BACK_ITEM = createItemStack(Material.IRON_DOOR, PlayerUtils.colorize("&cClose Menu"));

	public ItemStack createItemStack(Material material, @Nullable String name, @Nullable String... lore) {
		return createItemStack(material, (byte) 0, 1, name, lore);
	}

	public ItemStack createItemStack(Material material, byte data, @Nullable String name, @Nullable String... lore) {
		return createItemStack(material, data, 1, name, lore);
	}

	public ItemStack createItemStack(Material material, byte data, int amount, @Nullable String name, @Nullable String... lore) {

		@SuppressWarnings("deprecation")
		ItemStack item = new ItemStack(material, amount, data);

		String loreString = null;

		if (lore != null) {
			loreString = String.join("\n", lore);
		}

		setNameAndLore(item, name, loreString);
		return item;
	}

	public void setName(ItemStack item, String name) {

		if (item.hasItemMeta()) {
			return;
		}

		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.RESET + name);
		item.setItemMeta(meta);
	}

	public void addLore(ItemStack item, String... lore) {

		if (!item.hasItemMeta()) {
			return;
		}

		ItemMeta meta = item.getItemMeta();

		List<String> loreList = Lists.newArrayList();

		if (meta.hasLore()) {
			loreList = meta.getLore();
		}

		Arrays.stream(lore)
				.map(string -> ChatColor.RESET + string)
				.forEach(loreList::add);

		meta.setLore(loreList);
		item.setItemMeta(meta);

	}

	public void setNameAndLore(@Nonnull ItemStack item, @Nullable String name, @Nullable String lore) {
		setNameAndLore(item, name, lore, false);
	}

	public void setNameAndLore(@Nonnull ItemStack item, @Nullable String name, @Nullable String lore, boolean hideItemFlags) {
		Preconditions.checkNotNull(item, "item");

		ItemMeta meta = item.getItemMeta();
		if (meta == null) {
			return;
		}
		if (name != null) {
			meta.setDisplayName(name);
		}
		if (lore != null) {
			meta.setLore(Arrays.asList(StringUtils.splitPreserveAllTokens(lore, '\n')));
		}
		if (hideItemFlags) {
			meta.addItemFlags(ItemFlag.values());
		}
		item.setItemMeta(meta);
	}

	public ItemStack getTextItemStack(ItemStack item, List<String> description) {
		if (description == null || description.isEmpty()) {
			return item;
		}
		description = Lists.newArrayList(description);
		String displayName = description.get(0);
		description.remove(0);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(PlayerUtils.colorize(displayName));
		if (description.isEmpty()) {
			item.setItemMeta(meta);
			return item;
		}
		meta.setLore(
				description.stream()
						.map(PlayerUtils::colorize)
						.collect(Collectors.toList())
		);
		item.setItemMeta(meta);
		return item;
	}

	public boolean isEmpty(ItemStack itemStack) {
		return itemStack == null || itemStack.getType() == Material.AIR;
	}

}
