package net.stealthmc.hgcommon.bukkit;

import net.stealthmc.hgcommon.handler.GlobalPlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

public class BukkitGlobalPlayer extends GlobalPlayer {

	private final UUID uuid;
	private final String name;

	public BukkitGlobalPlayer(UUID uuid, String name) {
		this.uuid = uuid;
		this.name = name;
	}

	public BukkitGlobalPlayer(Player player) {
		this(player.getUniqueId(), player.getName());
	}

	@Override
	public UUID getUniqueId() {
		return uuid;
	}

	@Override
	public String getName() {
		return name;
	}
}
