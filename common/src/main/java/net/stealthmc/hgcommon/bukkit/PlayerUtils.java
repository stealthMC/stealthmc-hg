package net.stealthmc.hgcommon.bukkit;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.experimental.UtilityClass;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.stealthmc.hgcommon.bukkit.nms.NmsPlayerUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@UtilityClass
public class PlayerUtils {

	private Cache<UUID, ActionPriority> actionCache = Caffeine.newBuilder()
			.expireAfterWrite(3L, TimeUnit.SECONDS)
			.build();

	/**
	 * Nachricht ohne weitere Verarbeitung abschicken
	 *
	 * @param sender  Empfänger
	 * @param message Nachricht
	 */
	public void sendMessagePlain(CommandSender sender, String message) {
		sender.sendMessage(message);
	}

	/**
	 * Farbcodes übersetzen und Nachricht abschicken, mit {@link BaseComponent} wenn Spieler
	 *
	 * @param sender  Empfänger
	 * @param message Nachricht
	 */
	public void sendMessage(CommandSender sender, String message) {
		message = colorize(message);
		if (sender instanceof Player) {
			BaseComponent[] baseComponents = TextComponent.fromLegacyText(message);
			sendMessage((Player) sender, baseComponents);
			return;
		}
		sendMessagePlain(sender, message);
	}

	/**
	 * Empfänger Nachricht mit {@link BaseComponent} schicken wenn Spieler, zu Plaintext umwandeln
	 * wenn kein Spieler
	 *
	 * @param sender  Empfänger
	 * @param message Nachricht
	 */
	public void sendMessage(CommandSender sender, BaseComponent[] message) {
		if (sender instanceof Player) {
			sendMessage((Player) sender, message);
			return;
		}
		String plainText = TextComponent.toPlainText(message);
		sendMessagePlain(sender, plainText);
	}

	/**
	 * Spieler die Nachricht mit {@link BaseComponent} schicken
	 *
	 * @param player  Empfänger
	 * @param message Nachricht
	 */
	public void sendMessage(Player player, BaseComponent[] message) {
		player.sendMessage(message);
	}

	/**
	 * Empfänger Eine Reihe von Nachrichten schicken
	 *
	 * @param sender  Empfänger
	 * @param message Nachrichten
	 */
	public void sendMessage(CommandSender sender, List<BaseComponent[]> message) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			message.forEach(baseComponents -> sendMessage(player, baseComponents));
		} else {
			message.forEach(baseComponents -> sendMessage(sender, baseComponents));
		}
	}

	public String colorize(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}

	public void forEveryUUIDPlayer(Collection<UUID> uuids, Consumer<Player> f) {
		for (UUID uuid : uuids) {
			Player player = Bukkit.getPlayer(uuid);
			if (player != null) {
				f.accept(player);
			}
		}
	}

	public void giveOrDropAt(Player player, ItemStack itemStack) {
		giveOrDropAt(player, itemStack, itemStack.getAmount());
	}

	public void sendAction(Player player, String action) {
		sendAction(player, action, ActionPriority.LOWEST);
	}

	public void sendAction(Player player, String action, ActionPriority actionPriority) {
		ActionPriority previousPriority = actionCache.asMap().getOrDefault(player.getUniqueId(), ActionPriority.LOWEST);
		if (actionPriority.ordinal() < previousPriority.ordinal()) {
			return;
		}
		actionCache.asMap().put(player.getUniqueId(), actionPriority);
		NmsPlayerUtils.sendActionBar(player, action);
	}

	public void giveOrDropAt(Player player, ItemStack itemStack, int amount) {
		for (int i = 0; i < player.getInventory().getSize(); i++) {
			ItemStack invContent = player.getInventory().getItem(i);
			if (amount <= 0) {
				return;
			}
			if (invContent == null) {
				int newAmount = Math.min(itemStack.getMaxStackSize(), amount);
				ItemStack toAdd = itemStack.clone();
				toAdd.setAmount(newAmount);
				amount -= newAmount;

				player.getInventory().setItem(i, toAdd);
				continue;
			}
			if (!invContent.isSimilar(itemStack)) {
				continue;
			}
			int newAmount = Math.min(invContent.getMaxStackSize(), invContent.getAmount() + amount);
			amount -= newAmount - invContent.getAmount();
			invContent.setAmount(newAmount);
		}

		if (amount <= 0) {
			return;
		}

		ItemStack toDrop = itemStack.clone();
		toDrop.setAmount(amount);

		World world = player.getWorld();
		world.dropItemNaturally(player.getLocation(), toDrop);
	}

	public enum ActionPriority {
		LOWEST,
		LOW,
		NORMAL,
		HIGH,
		HIGHEST,
		OVERRIDE
	}

	public class SpreadPlayer {
		private static final Random random = ThreadLocalRandom.current();

		public static void spreadPlayers(double x, double z, double distance, double range, World world, List<Player> players, boolean loadChunk) {
			final double xRangeMin = x - range;
			final double zRangeMin = z - range;
			final double xRangeMax = x + range;
			final double zRangeMax = z + range;

			final int spreadSize = players.size();

			final Location[] locations = getSpreadLocations(world, spreadSize, xRangeMin, zRangeMin, xRangeMax, zRangeMax);
			final int rangeSpread = range(world, distance, xRangeMin, zRangeMin, xRangeMax, zRangeMax, locations);

			Validate.isTrue(rangeSpread != -1, String.format("Could not spread %d %s around %s,%s (too many players for space)", spreadSize, "players", x, z));

			spread(world, players, locations, loadChunk);
		}

		private static int range(World world, double distance, double xRangeMin, double zRangeMin, double xRangeMax, double zRangeMax, Location[] locations) {
			boolean flag = true;
			double max;

			int i;

			for (i = 0; i < 10000 && flag; ++i) {
				flag = false;
				max = Float.MAX_VALUE;

				Location loc1;
				int j;

				for (int k = 0; k < locations.length; ++k) {
					Location loc2 = locations[k];

					j = 0;
					loc1 = new Location(world, 0, 0, 0);

					for (int l = 0; l < locations.length; ++l) {
						if (k != l) {
							Location loc3 = locations[l];
							double dis = loc2.distanceSquared(loc3);

							max = Math.min(dis, max);
							if (dis < distance) {
								++j;
								loc1.add(loc3.getX() - loc2.getX(), 0, 0);
								loc1.add(loc3.getZ() - loc2.getZ(), 0, 0);
							}
						}
					}

					if (j > 0) {
						loc2.setX(loc2.getX() / j);
						loc2.setZ(loc2.getZ() / j);
						double d7 = Math.sqrt(loc1.getX() * loc1.getX() + loc1.getZ() * loc1.getZ());

						if (d7 > 0.0D) {
							loc1.setX(loc1.getX() / d7);
							loc2.add(-loc1.getX(), 0, -loc1.getZ());
						} else {
							double x = xRangeMin >= xRangeMax ? xRangeMin : random.nextDouble() * (xRangeMax - xRangeMin) + xRangeMin;
							double z = zRangeMin >= zRangeMax ? zRangeMin : random.nextDouble() * (zRangeMax - zRangeMin) + zRangeMin;
							loc2.setX(x);
							loc2.setZ(z);
						}

						flag = true;
					}

					boolean swap = false;

					if (loc2.getX() < xRangeMin) {
						loc2.setX(xRangeMin);
						swap = true;
					} else if (loc2.getX() > xRangeMax) {
						loc2.setX(xRangeMax);
						swap = true;
					}

					if (loc2.getZ() < zRangeMin) {
						loc2.setZ(zRangeMin);
						swap = true;
					} else if (loc2.getZ() > zRangeMax) {
						loc2.setZ(zRangeMax);
						swap = true;
					}
					if (swap) {
						flag = true;
					}
				}

				if (!flag) {
					Location[] locs = locations;
					int i1 = locations.length;

					for (j = 0; j < i1; ++j) {
						loc1 = locs[j];
						if (world.getHighestBlockYAt(loc1) == 0) {
							double x = xRangeMin >= xRangeMax ? xRangeMin : random.nextDouble() * (xRangeMax - xRangeMin) + xRangeMin;
							double z = zRangeMin >= zRangeMax ? zRangeMin : random.nextDouble() * (zRangeMax - zRangeMin) + zRangeMin;
							locations[i] = (new Location(world, x, 0, z));
							loc1.setX(x);
							loc1.setZ(z);
							flag = true;
						}
					}
				}
			}

			if (i >= 10000) {
				return -1;
			} else {
				return i;
			}
		}

		private static double spread(World world, List<Player> list, Location[] locations, boolean loadChunk) {
			double distance = 0.0D;
			int i = 0;

			for (Player player : list) {
				Location location = locations[i++];
				Location teleportLocation = new Location(world, Math.floor(location.getX()) + 0.5D, world.getHighestBlockYAt((int) location.getX(), (int) location.getZ()) + 2, Math.floor(location.getZ()) + 0.5D);

				if (loadChunk) {
					Chunk teleportChunk = teleportLocation.getChunk();

					if (!teleportChunk.isLoaded()) {
						teleportChunk.load();
					}
				}

				player.teleport(teleportLocation);
				double value = Double.MAX_VALUE;

				for (Location item : locations) {
					if (location != item) {
						double d = location.distanceSquared(item);
						value = Math.min(d, value);
					}
				}

				distance += value;
			}

			distance /= list.size();
			return distance;
		}

		private static Location[] getSpreadLocations(World world, int size, double xRangeMin, double zRangeMin, double xRangeMax, double zRangeMax) {
			Location[] locations = new Location[size];

			for (int i = 0; i < size; ++i) {
				double x = xRangeMin >= xRangeMax ? xRangeMin : random.nextDouble() * (xRangeMax - xRangeMin) + xRangeMin;
				double z = zRangeMin >= zRangeMax ? zRangeMin : random.nextDouble() * (zRangeMax - zRangeMin) + zRangeMin;
				locations[i] = (new Location(world, x, 0, z));
			}

			return locations;
		}
	}

}
