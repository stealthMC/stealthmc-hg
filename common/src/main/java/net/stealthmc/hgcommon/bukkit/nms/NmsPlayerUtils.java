package net.stealthmc.hgcommon.bukkit.nms;

import com.google.common.collect.Lists;
import lombok.experimental.UtilityClass;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

@UtilityClass
public class NmsPlayerUtils {

	private Class<?> craftPlayerClass;
	private Method craftPlayerGetHandleMethod;


	private Class<?> entityPlayerClass;
	private Field playerConnectionField;


	private Class<?> iChatBaseComponentClass;


	private Class<?> packetPlayOutChatClass;
	private Constructor<?> packetPlayOutChatConstructor;


	private Class<?> packetPlayOutSpawnEntityLivingClass;
	private Constructor<?> packetPlayOutSpawnEntityLivingConstructor;


	private Class<?> packetPlayOutEntityTeleportClass;
	private Constructor<?> packetPlayOutEntityTeleportConstructor;


	private Class<?> packetPlayOutEntityDestroyClass;
	private Constructor<?> packetPlayOutEntityDestroyConstructor;


	private Class<?> packetPlayOutEntityMetadataClass;
	private Constructor<?> packetPlayOutEntityMetadataConstructor;


	private Class<?> chatComponentTextClass;
	private Constructor<?> chatComponentTextConstructor;


	private Class<?> playerConnectionClass;
	private Method playerConnectionSendPacketMethod;


	private Class<?> packetClass;


	static {
		try {
			craftPlayerClass = NmsUtils.getCraftBukkitClass("entity.CraftPlayer");

			packetPlayOutSpawnEntityLivingClass = NmsUtils.getNMSClass("PacketPlayOutSpawnEntityLiving");
			packetPlayOutEntityTeleportClass = NmsUtils.getNMSClass("PacketPlayOutEntityTeleport");
			packetPlayOutEntityDestroyClass = NmsUtils.getNMSClass("PacketPlayOutEntityDestroy");
			packetPlayOutEntityMetadataClass = NmsUtils.getNMSClass("PacketPlayOutEntityMetadata");
			iChatBaseComponentClass = NmsUtils.getNMSClass("IChatBaseComponent");
			chatComponentTextClass = NmsUtils.getNMSClass("ChatComponentText");
			playerConnectionClass = NmsUtils.getNMSClass("PlayerConnection");
			packetClass = NmsUtils.getNMSClass("Packet");
			entityPlayerClass = NmsUtils.getNMSClass("EntityPlayer");

			packetPlayOutSpawnEntityLivingConstructor = packetPlayOutSpawnEntityLivingClass.getConstructor(NmsEntityUtils.getEntityLivingClass());
			packetPlayOutEntityTeleportConstructor = packetPlayOutEntityTeleportClass.getConstructor(NmsEntityUtils.getEntityClass());
			packetPlayOutEntityDestroyConstructor = packetPlayOutEntityDestroyClass.getConstructor(int[].class);
			packetPlayOutEntityMetadataConstructor = packetPlayOutEntityMetadataClass.getConstructor(int.class, NmsEntityUtils.getDataWatcherClass(), boolean.class);
			chatComponentTextConstructor = chatComponentTextClass.getConstructor(String.class);

			craftPlayerGetHandleMethod = craftPlayerClass.getMethod("getHandle");
			playerConnectionSendPacketMethod = playerConnectionClass.getMethod("sendPacket", packetClass);

			playerConnectionField = entityPlayerClass.getField("playerConnection");
		} catch (ClassNotFoundException | NoSuchMethodException | NoSuchFieldException e) {
			e.printStackTrace();
		}
	}

	public void sendPacket(Player player, Object packet) {
		try {
			Object entityPlayer = craftPlayerGetHandleMethod.invoke(player);
			Object playerConnection = playerConnectionField.get(entityPlayer);

			playerConnectionSendPacketMethod.invoke(playerConnection, packet);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public void sendActionBar(Player player, String message) {
		player.sendActionBar(message);
	}

	public void spawnVirtualEntity(Player player, Object entity) {
		try {
			Object packet = packetPlayOutSpawnEntityLivingConstructor.newInstance(entity);

			sendPacket(player, packet);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public void updateVirtualEntityLocation(Player player, Object entity) {
		try {
			Object packet = packetPlayOutEntityTeleportConstructor.newInstance(entity);

			sendPacket(player, packet);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public void despawnVirtualEntities(Player player, Object... entities) {
		try {
			List<Integer> ids = Lists.newArrayList();
			for (Object entity : entities) {
				Integer id = NmsEntityUtils.getEntityId(entity);
				if (id != null) {
					ids.add(id);
				}
			}
			int[] arr = ids.stream()
					.mapToInt(i -> i)
					.toArray();
			Object packet = packetPlayOutEntityDestroyConstructor.newInstance((Object) arr);

			sendPacket(player, packet);
		} catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public void updateEntityMetadata(Player player, Object entity) {
		Object dataWatcher = NmsEntityUtils.getEntityDataWatcher(entity);
		Integer entityId = NmsEntityUtils.getEntityId(entity);
		if (dataWatcher == null || entityId == null) {
			return;
		}
		try {
			Object packet = packetPlayOutEntityMetadataConstructor.newInstance(entityId, dataWatcher, true);

			sendPacket(player, packet);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

}
