package net.stealthmc.hgcommon.util;

import java.util.Objects;

public interface TerConsumer<T, U, V> {

	void accept(T t, U u, V v);

	default TerConsumer<T, U, V> andThen(TerConsumer<? super T, ? super U, ? super V> after) {
		Objects.requireNonNull(after);

		return (l, r, m) -> {
			accept(l, r, m);
			after.accept(l, r, m);
		};
	}

}
