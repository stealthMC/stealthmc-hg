package net.stealthmc.hgcommon.util;

import lombok.experimental.UtilityClass;

import java.util.Comparator;
import java.util.function.ToIntFunction;

@UtilityClass
public class StreamUtils {

	public <T> Comparator<T> comparingIntDescending(ToIntFunction<T> keyExtractor) {
		return (o1, o2) -> Integer.compare(keyExtractor.applyAsInt(o2), keyExtractor.applyAsInt(o1));
	}

	public <T> T printAndReturn(T obj) {
		System.out.println(obj);
		return obj;
	}

}
