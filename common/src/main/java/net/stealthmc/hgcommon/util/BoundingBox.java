package net.stealthmc.hgcommon.util;

public class BoundingBox {

	private double x1 = 0, x2 = 0,
			y1 = 0, y2 = 0,
			z1 = 0, z2 = 0;

	public BoundingBox(double x1, double y1, double z1, double width, double height, double depth) {
		this.x1 = x1;
		this.x2 = x1 + width;
		if (x2 < x1) {
			this.x1 = this.x2;
			this.x2 = x1;
		}

		this.y1 = y1;
		this.y2 = y1 + height;
		if (y2 < y1) {
			this.y1 = y2;
			this.y2 = y1;
		}

		this.z1 = z1;
		this.z2 = z1 + depth;
		if (z2 < z1) {
			this.z1 = z2;
			this.z2 = z1;
		}
	}

	public boolean contains(double x, double y, double z) {
		return x1 <= x && x <= x2 &&
				y1 <= y && y <= y2 &&
				z1 <= z && z <= z2;
	}

}
