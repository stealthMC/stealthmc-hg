package net.stealthmc.hgcommon.util;

import com.google.common.collect.Maps;
import lombok.experimental.UtilityClass;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.stealthmc.hgcommon.handler.GlobalPlayer;
import net.stealthmc.hgcommon.handler.PluginHandler;
import org.apache.commons.lang.ArrayUtils;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@UtilityClass
public class CommandUtils {

	public final DecimalFormat NO_DECIMAL_PLACES_FORMAT = new DecimalFormat("#");
	private Map<String, Boolean> booleanValues = Maps.newHashMap();

	static {
		booleanValues.put("ja", true);
		booleanValues.put("1", true);
		booleanValues.put("true", true);
		booleanValues.put("nein", false);
		booleanValues.put("0", false);
		booleanValues.put("false", false);
	}

	public List<String> copyPartialMatches(String token, Collection<String> entries) {
		return entries.stream()
				.filter(entry -> entry.startsWith(token))
				.collect(Collectors.toList());
	}

	public List<String> completeOnlinePlayers(String token) {
		return copyPartialMatches(token, PluginHandler.getHgMain().getOnlinePlayers()
				.stream()
				.map(GlobalPlayer::getName)
				.collect(Collectors.toList()));
	}

	public List<String> completeBooleanValues(String token) {
		return copyPartialMatches(token, getBooleanValues());
	}

	public String[] remainingArgs(String[] args, int useUp) {
		if (args.length - useUp <= 0) {
			return new String[0];
		}
		String[] result = new String[args.length - useUp];
		System.arraycopy(args, useUp, result, 0, result.length);
		return result;
	}

	public String joinArgs(String[] args, int fromIndex, int to, String delim) {
		return String.join(delim, Arrays.stream(ArrayUtils.subarray(args, fromIndex, to))
				.map(s -> (CharSequence) s)
				.toArray(CharSequence[]::new));
	}

	public Integer getInteger(String arg) {
		try {
			return Integer.parseInt(arg);
		} catch (Exception ignored) {
			return null;
		}
	}

	public Double getDouble(String arg) {
		try {
			return Double.parseDouble(arg);
		} catch (Exception ignored) {
			return null;
		}
	}

	public Boolean getBoolean(String arg) {
		return booleanValues.get(arg);
	}

	public Set<String> getBooleanValues() {
		return booleanValues.keySet();
	}

	public String formatBoolean(boolean b) {
		return b ? ChatColor.GREEN + "true" : ChatColor.RED + "false";
	}

	public List<BaseComponent[]> getSyntax(String alias, List<String> subcommands) {
		Menu menu = new Menu(ChatColor.GREEN + "Usage:");
		subcommands.forEach(cmd -> {
			String scmd = "/" + alias + " " + cmd;
			Menu sub = new Menu(ChatColor.RED + scmd, ClickEvent.Action.SUGGEST_COMMAND, scmd);
			menu.addSub(sub);
		});
		return menu.toComponents();
	}

}
