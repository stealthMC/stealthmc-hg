package net.stealthmc.hgcommon.util;

import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.function.Supplier;

@UtilityClass
public class ListUtils {

	public <T> List<T> prependCopy(Supplier<List<T>> listSupplier, T arg, List<T> list) {
		List<T> prepended = listSupplier.get();
		prepended.add(arg);
		prepended.addAll(list);
		return prepended;
	}

	public <T> List<T> appendCopy(Supplier<List<T>> listSupplier, T arg, List<T> list) {
		List<T> appended = listSupplier.get();
		appended.addAll(list);
		appended.add(arg);
		return appended;
	}

}
