package net.stealthmc.hgcommon.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PhrasePair {

	private String singular;
	private String plural;

	public String get(int amount) {
		return amount == 1 ? singular : plural;
	}

}
