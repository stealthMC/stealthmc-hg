package net.stealthmc.hgcommon;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Permission {

	/**
	 * Permission to join the game at any time but ending phase <br/>
	 */
	public final String JOIN_RUNNING_GAMES = "hg.join_running_games";

	/**
	 * Permission to rejoin games after initial start if phase allows for it
	 */
	public final String REJOIN_GAMES = "hg.rejoin_games";

	/**
	 * Permission to spectate games after death
	 */
	public final String SPECTATE_GAMES = "hg.spectate_games";

	/**
	 * Permission to automatically respawn in games when gamephase allows for it
	 */
	public final String RESPAWN_IN_GAMES = "hg.respawn";

	/**
	 * Permission to use /hg
	 */
	public final String HG_COMMAND = "hg.command";

	/**
	 * Permission to use /hg keepworld
	 */
	public final String HG_KEEPWORLD = "hg.command.keepworld";

	/**
	 * Permission to use /hg timer
	 */
	public final String HG_TIMER = "hg.command.timer";

	/**
	 * Permission to use /hg respawn
	 */
	public final String HG_RESPAWN = "hg.command.respawn";

	/**
	 * Permission to use /hg build
	 */
	public final String HG_BUILD = "hg.command.build";

	/**
	 * Permission to use /health
	 */
	public final String HG_HEALTH = "hg.command.health";

	/**
	 * Permission to use /hunger
	 */
	public final String HG_HUNGER = "hg.command.hunger";

	/**
	 * Used for various stuff, e.g. being able to break blocks placed by Kits
	 * Overrides for /spawn and /sspawn
	 */
	public final String HG_GAME_ADMIN = "hg.gameadmin";

	/**
	 * Used for /publicchat to message everyone when dead
	 */
	public final String PUBLICCHAT = "hg.publicchat";

	/**
	 * Used for /hgsettings to view current game settings
	 */
	public final String HGSETTINGS_OVERVIEW = "hgsettings.overview";

	/**
	 * Used for /hgsettings to change settings
	 */
	public final String HGSETTINGS_ADMIN = "hgsettings.admin";

	/**
	 * Used for /gm or /gamemode
	 */
	public final String HG_GAMEMODE = "hg.gamemode";

	/**
	 * Used for /spawn
	 */
	public final String HG_SPAWN = "hg.spawn";

	/**
	 * Used for /sspawn
	 */
	public final String HG_SSPAWN = "hg.sspawn";

	/**
	 * Used for /pvp to toggle PVP in the given world/game.
	 */
	public final String HG_PVPTOGGLE = "hg.pvptoggle";

	/**
	 * Used for /hh and will give you full Food/Health
	 */
	public final String HG_HH = "hg.hh";

}
