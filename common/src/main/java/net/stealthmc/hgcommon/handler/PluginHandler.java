package net.stealthmc.hgcommon.handler;

import lombok.Getter;

public class PluginHandler {

	@Getter
	private static IHGMain hgMain;

	public static void setHgMain(IHGMain hgMain) {
		PluginHandler.hgMain = hgMain;
	}

}
