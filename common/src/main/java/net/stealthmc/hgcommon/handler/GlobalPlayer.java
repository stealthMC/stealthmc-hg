package net.stealthmc.hgcommon.handler;

import java.util.UUID;

public abstract class GlobalPlayer {

	public abstract UUID getUniqueId();

	public abstract String getName();

}
