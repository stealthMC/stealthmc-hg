package net.stealthmc.hgcore.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class RespawnCommand implements TabExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 0) {
			onSelfRespawn(sender);
			return true;
		}

		if (args.length == 1) {
			onRespawnOther(sender, args[0]);
			return true;
		}

		onSyntax(sender, label);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
		String token = args[args.length - 1];

		if (args.length == 1) {
			return CommandUtils.completeOnlinePlayers(token);
		}

		return ImmutableList.of();
	}

	private void onSyntax(CommandSender sender, String label) {
		PlayerUtils.sendMessage(sender, CommandUtils.getSyntax(label, ImmutableList.of(
				"respawn [<Player>]"
		)));
	}

	private void onSelfRespawn(CommandSender sender) {
		if (!(sender instanceof Player)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNotAPlayer());
			return;
		}
		HGPlayer hgPlayer = HGPlayer.from((Player) sender);
		if (!hgPlayer.isSpectating()) {
			PlayerUtils.sendMessage(sender, ChatColor.RED + "You are still alive!");
			return;
		}
		GameHandler.getInstance().respawnPlayer((Player) sender);
	}

	private void onRespawnOther(CommandSender sender, String arg) {
		Player target = Bukkit.getPlayerExact(arg);
		if (target == null) {
			PlayerUtils.sendMessage(sender, ChatColor.RED + "The player " + ChatColor.WHITE + arg +
					ChatColor.RED + " does not exist.");
			return;
		}
		HGPlayer hgTarget = HGPlayer.from(target);
		if (!hgTarget.isSpectating()) {
			PlayerUtils.sendMessage(sender, ChatColor.WHITE + target.getName() + ChatColor.RED + " is still alive!");
			return;
		}
		GameHandler.getInstance().respawnPlayer(target);
		PlayerUtils.sendMessage(sender, ChatColor.GREEN + "The player " + ChatColor.WHITE + target.getName() +
				ChatColor.GREEN + " was respawned.");
		PlayerUtils.sendMessage(target, ChatColor.GREEN + "You have been respawned.");
	}

}
