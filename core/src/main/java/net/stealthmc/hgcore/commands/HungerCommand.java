package net.stealthmc.hgcore.commands;

import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class HungerCommand implements TabExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
		if (!(sender instanceof Player)) {
			PlayerUtils.sendMessage(sender, HGBukkitMain.getInstance().getMessages().getNotAPlayer());
			return true;
		}

		Player player = (Player) sender;

		int foodLevel = 25;
		float saturation = player.getSaturation();

		if (args.length >= 1) {
			Integer argFoodLevel = CommandUtils.getInteger(args[0]);
			if (argFoodLevel == null) {
				PlayerUtils.sendMessage(player, ChatColor.RED + "Invalid value for " +
						ChatColor.WHITE + "food level" + ChatColor.RED + "!");
				return true;
			}
			if (argFoodLevel > 25) {
				PlayerUtils.sendMessage(player, ChatColor.RED + "The maximum value for " +
						ChatColor.WHITE + "food level" + ChatColor.RED + "is" +
						ChatColor.WHITE + "25" + ChatColor.RED + "!");
				return true;
			}
			foodLevel = argFoodLevel;
		}
		if (args.length >= 2) {
			Double argSaturation = CommandUtils.getDouble(args[1]);
			if (argSaturation == null) {
				PlayerUtils.sendMessage(player, ChatColor.RED + "Invalid value for " +
						ChatColor.WHITE + "saturation" + ChatColor.RED + "!");
				return true;
			}
			saturation = (float) (double) argSaturation;
		}

		player.setFoodLevel(foodLevel);
		PlayerUtils.sendMessage(player, ChatColor.GREEN + String.format("Your new food level is " +
				ChatColor.WHITE + "%d", foodLevel));

		if (saturation == player.getSaturation()) {
			return true;
		}

		player.setSaturation(saturation);
		PlayerUtils.sendMessage(player, ChatColor.GREEN + String.format("Your new saturation is " +
				ChatColor.WHITE + "%.2f", saturation));

		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
		return null;
	}
}
