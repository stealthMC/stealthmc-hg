package net.stealthmc.hgcore.listeners;

import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgcore.service.ServiceHandler;
import net.stealthmc.hgcore.service.predefined.ToggleableService;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class ChatListener implements Listener, ToggleableService {

	public static final String SERVICE_KEY = "death-chat";

	private static final String CHAT_FORMAT = ChatColor.GRAY + "%s" + ChatColor.RESET + ": %s";

	private static final Set<UUID> staffChats = new HashSet<>();
	private static boolean enabled = true;

	public ChatListener() {
		ServiceHandler.registerService(SERVICE_KEY, this);
	}

	public static void setStaffChattingOnce(UUID playerId) {
		staffChats.add(playerId);
	}

	@Override
	public Boolean get() {
		return enabled;
	}

	@Override
	public void set(Boolean bool) {
		ChatListener.enabled = bool;
	}


	@EventHandler
	public void onAsyncChat(AsyncPlayerChatEvent event) {
		Map<UUID, Boolean> spectating;

		synchronized (HGPlayer.SPECTATE_LOCK) {
			spectating = HGPlayer.allKnownPlayers().stream()
					.collect(Collectors.toMap(HGPlayer::getEntityId, HGPlayer::isSpectating));
		}

		boolean spectator = spectating.getOrDefault(event.getPlayer().getUniqueId(), false);

		boolean staff = staffChats.remove(event.getPlayer().getUniqueId());
		boolean sendToEveryone = staff || !enabled;

		if (spectator) {
			event.setFormat(ChatColor.DARK_GRAY + "[" + ChatColor.GRAY + "DEAD" + ChatColor.DARK_GRAY + "] " + CHAT_FORMAT);
		} else {
			event.setFormat(CHAT_FORMAT);
		}

		if (sendToEveryone || !spectator) {
			return;
		}


		//sender ist spectator
		try {
			event.getRecipients().removeIf(player -> !HGPlayer.from(player).isSpectating());
		} catch (UnsupportedOperationException exc) {
			HGBukkitMain.getInstance().getLogger().log(Level.WARNING, "Unable to clear Chat recipients!", exc);
		}
	}
}
