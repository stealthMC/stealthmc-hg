package net.stealthmc.hgcore.listeners;

import net.stealthmc.hgcore.game.GameHandler;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.spigotmc.event.entity.EntityDismountEvent;

public class EntityListener implements Listener {

	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		GameHandler.getInstance().handleEntityDamage(event);
	}

	@EventHandler
	public void onEntityDamgeByEntity(EntityDamageByEntityEvent event) {
		GameHandler.getInstance().handleEntityDamageByEntity(event);
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		GameHandler.getInstance().handleEntityDeath(event);
	}

	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		GameHandler.getInstance().handleProjectileLaunch(event);
	}

	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		GameHandler.getInstance().handleProjectileHit(event);
	}

	@EventHandler
	public void onDismount(EntityDismountEvent event) {
		GameHandler.getInstance().handleDismount(event);
	}

	@EventHandler
	public void onPlayerExpChange(PlayerExpChangeEvent event) {
		GameHandler.getInstance().handlePlayerExpChange(event);
	}

	@EventHandler
	public void onExpBottle(ExpBottleEvent event) {
		GameHandler.getInstance().handleExpBottle(event);
	}

	@EventHandler
	public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event)  {
		GameHandler.getInstance().handleEntityTargetLivingEntity(event);
	}

}
