package net.stealthmc.hgcore.listeners;

import net.stealthmc.hgcore.commands.GameModeCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class GameModeListener implements Listener {

	public static final Set<UUID> EVENT_IGNORED_UUIDS = new HashSet<>();

	@EventHandler(priority = EventPriority.NORMAL)
	public void onGameModeChanged(PlayerGameModeChangeEvent event) {
		Player player = event.getPlayer();
		if (GameModeListener.EVENT_IGNORED_UUIDS.remove(player.getUniqueId())) {
			return;
		}
		GameModeCommand.PREVIOUS_MODES.put(player.getUniqueId(), player.getGameMode());
	}

}
