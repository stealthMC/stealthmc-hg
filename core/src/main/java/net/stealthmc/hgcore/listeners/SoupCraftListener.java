package net.stealthmc.hgcore.listeners;

import net.stealthmc.hgcore.service.ServiceHandler;
import net.stealthmc.hgcore.service.predefined.ToggleableService;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.*;

public class SoupCraftListener implements Listener, ToggleableService {

	public static final String SERVICE_KEY = "soup-craft-only-table";

	private boolean enabled = false; //DEFAULT OFF

	public SoupCraftListener() {
		ServiceHandler.registerService(SERVICE_KEY, this);
	}

	@EventHandler
	public void onPrepareCraftEvent(PrepareItemCraftEvent event) {
		if (!enabled) {
			return;
		}

		InventoryHolder holder = event.getInventory().getHolder();

		if (!(holder instanceof Player)) {
			return;
		}

		Player player = (Player) holder;

		InventoryView openInventory = player.getOpenInventory();
		if (openInventory == null) {
			return;
		}

		Inventory topInventory = openInventory.getTopInventory();
		if (topInventory == null) {
			return;
		}

		if (!(topInventory instanceof CraftingInventory)) {
			return;
		}

		if (((CraftingInventory) topInventory).getMatrix().length > 5) {
			/*	Only disable crafting for player inventory:	*/
			/*	5 = 4 recipe slots + 1 result slot		 	*/
			return;
		}

		Recipe recipe = event.getRecipe();
		ItemStack result = recipe.getResult();

		if (result == null) {
			return;
		}

		if (result.getType() != Material.MUSHROOM_STEW) {
			return;
		}

		((CraftingInventory) topInventory).setResult(null);
	}

	@Override
	public Boolean get() {
		return enabled;
	}

	@Override
	public void set(Boolean bool) {
		enabled = bool;
	}
}
