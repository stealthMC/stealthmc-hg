package net.stealthmc.hgcore.listeners;

import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import net.stealthmc.hgcommon.bukkit.inventory.ItemStackUtils;
import net.stealthmc.hgcommon.util.Pair;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.service.HGService;
import net.stealthmc.hgcore.service.ServiceHandler;
import net.stealthmc.hgcore.service.predefined.ServiceProvider;
import net.stealthmc.hgcore.service.predefined.ToggleableService;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Furnace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

public class FurnaceSmeltListener implements Listener, ServiceProvider {

	public static final String SERVICE_NAME = "furnace";

	public static final String IRON_SUBSERVICE_NAME = "iron_slower";

	public static final String GOLD_SUBSERVICE_NAME = "gold_faster";

	private static final short DEFAULT_SMELT_TIME = 200;

	@Getter
	private static int ironSmeltTime = 400;

	@Getter
	private static int goldSmeltTime = 50;

	@Getter
	private static boolean ironEnabled = true;

	@Getter
	private static boolean goldEnabled = true;

	private static Map<Material, Pair<Supplier<Boolean>, IntSupplier>> SMELT_TIMES = new HashMap<>();

	private static Map<String, HGService> subServices = new HashMap<>();

	private static Map<Furnace, AtomicInteger> burningFurnaces = new HashMap<>();

	private static FurnaceUpdateTask furnaceUpdateTask = new FurnaceUpdateTask();

	static {
		SMELT_TIMES.put(Material.IRON_ORE, Pair.of(FurnaceSmeltListener::isIronEnabled, FurnaceSmeltListener::getIronSmeltTime));
		SMELT_TIMES.put(Material.GOLD_ORE, Pair.of(FurnaceSmeltListener::isGoldEnabled, FurnaceSmeltListener::getGoldSmeltTime));

		subServices.put(IRON_SUBSERVICE_NAME, new ToggleableService() {
			@Override
			public Boolean get() {
				return ironEnabled;
			}

			@Override
			public void set(Boolean bool) {
				ironEnabled = bool;
			}
		});

		subServices.put(GOLD_SUBSERVICE_NAME, new ToggleableService() {
			@Override
			public Boolean get() {
				return goldEnabled;
			}

			@Override
			public void set(Boolean bool) {
				goldEnabled = bool;
			}
		});
	}

	public FurnaceSmeltListener() {
		ServiceHandler.registerService(SERVICE_NAME, this);
		furnaceUpdateTask.runTaskTimer(HGBukkitMain.getInstance(), 1L, 1L);
	}

	@Override
	public Map<String, HGService> get() {
		return subServices;
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onInventoryClick(InventoryClickEvent event) {
		Inventory clicked = event.getInventory();
		if (clicked == null) {
			return;
		}

		if (!(clicked instanceof FurnaceInventory)) {
			return;
		}

		Furnace furnace = ((FurnaceInventory) clicked).getHolder();
		if (furnace == null) {
			return;
		}

		Bukkit.getScheduler().runTaskLater(HGBukkitMain.getInstance(), () -> {
			ItemStack source = furnace.getInventory().getSmelting();
			handleFurnaceStart(furnace, source, false);
		}, 1L);
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onSmelt(FurnaceSmeltEvent event) {
		Block block = event.getBlock();
		BlockState state = block.getState();
		if (!(state instanceof Furnace)) {
			return;
		}
		Furnace furnace = (Furnace) state;

		ItemStack smelted = event.getSource();

		handleFurnaceStart(furnace, smelted, true);
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onFurnaceBreak(BlockBreakEvent event) {
		BlockState state = event.getBlock().getState();
		if (!(state instanceof Furnace)) {
			return;
		}
		if (!burningFurnaces.containsKey(state)) {
			return;
		}
		burningFurnaces.remove(state);
	}

	private void handleFurnaceStart(Furnace furnace, ItemStack smelting, boolean forceReset) {
		if (smelting == null) {
			return;
		}

		Pair<Supplier<Boolean>, IntSupplier> supplierPair = SMELT_TIMES.get(smelting.getType());

		if (supplierPair == null || !supplierPair.getKey().get()) {
			//service does not exist or is disabled
			burningFurnaces.remove(furnace);
			furnace.setCookTime((short) 0);
			return;
		}

		if (!forceReset && burningFurnaces.containsKey(furnace)) {
			return;
		}

		burningFurnaces.put(furnace, new AtomicInteger(0));
	}

	private static class FurnaceUpdateTask extends BukkitRunnable {

		@Override
		public void run() {
			ImmutableMap.copyOf(burningFurnaces).forEach((furnace, integer) -> {
				ItemStack smelting = furnace.getInventory().getSmelting();
				if (smelting == null) {
					burningFurnaces.remove(furnace);
					return;
				}

				if (furnace.getCookTime() < 1) {
					return;
				}

				Pair<Supplier<Boolean>, IntSupplier> supplierPair = SMELT_TIMES.get(smelting.getType());
				if (supplierPair == null) {
					burningFurnaces.remove(furnace);
					return;
				}

				if (ItemStackUtils.isEmpty(furnace.getInventory().getFuel())) {
					integer.set(0);
					furnace.setCookTime((short) 0);
					return;
				}

				int ticksCooked = integer.getAndIncrement();
				double percentage = (double) ticksCooked / supplierPair.getRight().getAsInt();

				int totalCookTime = (int) (percentage * DEFAULT_SMELT_TIME);
				furnace.setCookTime((short) totalCookTime);
			});
		}

	}

}
