package net.stealthmc.hgcore.listeners;

import net.stealthmc.hgcore.game.WeaponDamageHandler;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;

public class ItemCreateListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onItemCraft(PrepareItemCraftEvent event) {
		CraftingInventory inventory = event.getInventory();
		ItemStack result = inventory.getResult();

		if (result == null) {
			return;
		}

		result = WeaponDamageHandler.processItemStack(result);
		inventory.setResult(result);
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onCreative(InventoryCreativeEvent event) {
		ItemStack cursor = event.getCursor();
		if (cursor != null) {
			cursor = WeaponDamageHandler.processItemStack(cursor);
			event.setCursor(cursor);
		}

		ItemStack current = event.getCurrentItem();
		if (current != null) {
			current = WeaponDamageHandler.processItemStack(current);
			event.setCurrentItem(current);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onItemSpawn(ItemSpawnEvent event) {
		Item spawned = event.getEntity();
		ItemStack itemStack = spawned.getItemStack();
		itemStack = WeaponDamageHandler.processItemStack(itemStack);
		spawned.setItemStack(itemStack);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onItemDrop(PlayerDropItemEvent event) {
		Item dropped = event.getItemDrop();
		ItemStack droppedItemStack = dropped.getItemStack();
		droppedItemStack = WeaponDamageHandler.processItemStack(droppedItemStack);
		dropped.setItemStack(droppedItemStack);
	}

}
