package net.stealthmc.hgcore.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.stealthmc.hgcommon.bukkit.scoreboard.PlayerScoreboard;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


/**
 * Fired by ScoreboardHandler when initially registering a Scoreboard for a player
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class PlayerScoreboardRegisteredEvent extends Event {

	private static HandlerList handlerList = new HandlerList();
	private Player player;
	private PlayerScoreboard playerScoreboard;

	//required by API
	public static HandlerList getHandlerList() {
		return handlerList;
	}

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}

}
