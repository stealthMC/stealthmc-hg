package net.stealthmc.hgcore.service.predefined;

import net.stealthmc.hgcore.service.HGMutableService;
import net.stealthmc.hgcore.service.helper.MutableServiceInputValidator;
import net.stealthmc.hgcore.service.helper.ServiceValueFormatter;

public interface IntegerService extends HGMutableService<Integer> {

	Integer get();

	void set(Integer value);

	@Override
	default MutableServiceInputValidator<Integer> getInputValidator() {
		return new MutableServiceInputValidator.IntegerValidator(this);
	}

	@Override
	default ServiceValueFormatter<Integer> getServiceValueFormatter() {
		return new ServiceValueFormatter.IntegerFormatter(this);
	}
}
