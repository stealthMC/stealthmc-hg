package net.stealthmc.hgcore.service;

import net.stealthmc.hgcore.service.helper.MutableServiceInputValidator;

public interface HGMutableService<T> extends HGService<T> {

	void set(T t);

	MutableServiceInputValidator<T> getInputValidator();

}
