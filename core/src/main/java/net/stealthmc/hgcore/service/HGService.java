package net.stealthmc.hgcore.service;

import net.stealthmc.hgcore.service.helper.ServiceValueFormatter;

public interface HGService<T> {

	T get();

	ServiceValueFormatter<T> getServiceValueFormatter();

}
