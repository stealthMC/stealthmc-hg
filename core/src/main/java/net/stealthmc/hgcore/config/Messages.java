package net.stealthmc.hgcore.config;

import lombok.Getter;
import net.cubespace.Yamler.Config.Config;
import net.cubespace.Yamler.Config.ConfigMode;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.stealthmc.hgcommon.util.MapUtils;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Getter
public class Messages extends Config {

	private String noPermission = "&cYou don't have permission to do this.";
	private String notAPlayer = "&9HG &7» &cYou must be a player to do this.";
	private String cannotConnectWorldPhase = "&cThe server is not yet ready.";
	private String cannotConnectPlaying = "&cThe game has already begun!";
	private String cannotRejoin = "&cYou have to be &6VIP &cto rejoin games!";
	private String playerJoined = "&b%s joined the game.";
	private String playerWelcome = "&8&l&m-----------------------\n" +
			"&cWelcome to the Hardcore Games.\n" +
			"&cChoose your kit now.\n" +
			"&cLast man standing wins!\n" +
			"&8&l&m-----------------------\n" +
			"";
	private String noCompassTargetFound = "&9HG &7» &cNo compass target was found.";
	private String compassTargetFound = "&9HG &7» &aYour compass is now pointing towards &c%s";
	private String maxBuildHeight = "&9HG &7» &cThe maximum building height is &e%d";
	private String scoreboard_title = "&a&lstealthmc";
	private String scoreboard_timerWaitingName = "Waiting for players";
	private String scoreboard_timeFormatWaiting = "&a%s";
	private String scoreboard_timerStartingName = "Starting in...";
	private String scoreboard_timeFormatStarting = "&a%s";
	private String scoreboard_timerImmortalityName = "Immortality phase:";
	private String scoreboard_timeFormatImmortality = "&e%s";
	private String scoreboard_timerStartName = "Time remaining";
	private String scoreboard_timeFormatStart = "&e%s";
	private String scoreboard_timerPlayingName = "Time remaining";
	private String scoreboard_timeFormatPlaying = "&c%s";
	private String scoreboard_showdownShrinkIn = "Shrinking border in:";
	private String scoreboard_showdownShrinkTimeFormat = "&c%s";
	private String scoreboard_showdownEnd = "Game ending";
	private String scoreboard_shodownEndTimeFormat = "&c%s";
	private String scoreboard_timerShutdown = "Restart";
	private String scoreboard_timeFormatShutdown = "&4%s";
	private String scoreboard_killCountName = "&bKills";
	private String scoreboard_killCountFormat = " %d";
	private String scoreboard_playerCountName = "&ePlayers";
	private String scoreboard_playerCountFormat = " %d/%d";
	private String scoreboard_borderSizeName = "&dBorder Size";
	private String scoreboard_borderSizeFormat = " %dx%d";
	private String playerWon = "&9HG &7» &aCongratulations to the winner: &c%s";
	private String noWinner = "&9HG &7» &cUnfortunately, this round had no winner!";
	private String youDiedKick = "&cSorry, you died.";
	private String shutdownKick = "&cThis game is over!";
	private String youRespawned = "&9HG &7» &aYou respawned!";
	private String showdownName = "&cFINAL FIGHT";
	//private int playerCount = HGBukkitMain.getInstance().getOnlinePlayers().size();
	private String worldBorderAnnouncement = "&9HG &7» &aWorldborder is &b<size>&ax&b<size> &ain size!";

	private Map<Integer, String> notifications_waiting = MapUtils.of(HashMap::new,
			30, "&9HG &7» &c30 &aseconds until the Tournament starts!",
			5, "&9HG &7» &c5 &aseconds until the Tournament starts!",
			3, "&9HG &7» &c3 &aseconds until the Tournament starts!",
			2, "&9HG &7» &c2 &aseconds until the Tournament starts!",
			1, "&9HG &7» &c1 &aseconds until the Tournament starts!",
			0, "&8&l&m-----------------------\n" +
					"&9HG &7» &cThe Tournament has begun!\n" +
					//"&9HG &7» &cThere are &6" + playerCount + "&c players participating.\n" +
					"&9HG &7» &cEveryone is invincible for 2 minutes.\n" +
					"&9HG &7» &cGood Luck!\n" +
					"&8&l&m-----------------------"
	);
	private Map<Integer, String> notifications_halted = MapUtils.of(HashMap::new,
			10, "&9HG &7» &c10 &aseconds until release!",
			5, "&9HG &7» &c5 &aseconds until release!",
			3, "&9HG &7» &c3 &aseconds until release!",
			2, "&9HG &7» &c2 &aseconds until release!",
			1, "&9HG &7» &c1 &asecond until release!",
			0, "&9HG &7» &cYou are free now, run!"
	);
	private Map<Integer, String> notifications_immortality = MapUtils.of(HashMap::new,
			120, "&9HG &7» &c2 &aminutes until immortality wears off!",
			60, "&9HG &7» &c1 &aminute until immortality wears off!",
			30, "&9HG &7» &c30 &aseconds until immortality wears off!",
			15, "&9HG &7» &c15 &aseconds until immortality wears off!",
			5, "&9HG &7» &c5 &aeconds until immortality wears off!",
			3, "&9HG &7» &c3 &aseconds until immortality wears off!",
			2, "&9HG &7» &c2 &aseconds until immortality wears off!",
			1, "&9HG &7» &c1 &asecond until immortality wears off!",
			0, "&9HG &7» &cImmortality is now off!"
	);
	private Map<Integer, String> notifications_showdownStart = MapUtils.of(HashMap::new,
			300, "&9HG &7» &c5 &aminutes until the " + showdownName + " &astarts!",
			60, "&9HG &7» &c60 &aseconds until the " + showdownName + " &astarts!",
			10, "&9HG &7» &c10 &aseconds until the " + showdownName + " &astarts!",
			5, "&9HG &7» &c5 &aseconds until the " + showdownName + " &astarts!",
			4, "&9HG &7» &c4 &aseconds until the " + showdownName + " &astarts!",
			3, "&9HG &7» &c3 &aseconds until the " + showdownName + " &astarts!",
			2, "&9HG &7» &c2 &aseconds until the " + showdownName + " &astarts!",
			1, "&9HG &7» &c1 &asecond until the " + showdownName + " &astarts!",
			0, "&9HG &7» &cGood luck in the " + showdownName + " &cplayers!"
	);
	private Map<Integer, String> notifications_borderShrink = MapUtils.of(HashMap::new,
			30, "&9HG &7» &c30 &aseconds until the border shrinks!",
			10, "&9HG &7» &c10 &aseconds until the border shrinks!",
			5, "&9HG &7» &c5 &aseconds until the border shrinks!",
			3, "&9HG &7» &c3 &aseconds until the border shrinks!",
			2, "&9HG &7» &c2 &aseconds until the border shrinks!",
			1, "&9HG &7» &c1 &asecond until the border shrinks!",
			0, "&9HG &7» &cBorder is now shrinking!"
	);

	public Messages(JavaPlugin plugin) throws InvalidConfigurationException {
		this.CONFIG_FILE = new File(plugin.getDataFolder(), "messages.yml");
		this.CONFIG_MODE = ConfigMode.DEFAULT;
		super.init();
	}

}
