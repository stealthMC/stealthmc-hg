package net.stealthmc.hgcore.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AdapterPriority {

	int DEFAULT_PRIORITY = 1000;

	int priority() default DEFAULT_PRIORITY;

}
