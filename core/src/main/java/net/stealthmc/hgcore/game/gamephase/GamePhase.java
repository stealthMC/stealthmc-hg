package net.stealthmc.hgcore.game.gamephase;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum GamePhase {

	/**
	 * Worldgen
	 */
	WORLD(false, false, false, false, false),
	/**
	 * Lobbyphase, Waiting for players
	 */
	LOBBY(true, true, false, false, false),
	/**
	 * Spieler get frozzen and tpd to spawn, Game will begin shortly
	 */
	PRE_START(false, true, false, false, false),
	/**
	 * Immortality Phase, game is running
	 */
	IMMORTALITY(false, true, true, false, false),
	/**
	 * Game is running but players can still be respawned/vip can join.
	 */
	START(false, true, true, true, true),
	/**
	 * Players cant be respawned/VIP cant join anymore.
	 */
	PLAYING(false, false, true, true, true),
	/**
	 * Final Figh(Pit) could be skipped if ppl die before.
	 */
	SHOWDOWN(false, false, true, true, true),
	/**
	 * Endphase, Server shutdown, delete world, restart.
	 */
	END(false, false, false, false, false),
	/**
	 * Debugging
	 */
	DEBUG(false, false, true, true, true);

	private final boolean joiningEnabled;
	private final boolean respawningEnabled;
	private final boolean entityDamageEnabled;
	private final boolean playerDamageEnabled;
	private  final boolean mobTargetingPlayer;

	public GamePhase nextPhase() {
		return GamePhase.values()[(this.ordinal() + 1) % GamePhase.values().length];
	}

}
