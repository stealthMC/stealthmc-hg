package net.stealthmc.hgcore.game;

import lombok.experimental.UtilityClass;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;

@UtilityClass
public class WeaponDamageHandler {

	private final Map<Material, Function<ItemStack, ItemStack>> WEAPON_DAMAGE_MODIFIERS = new HashMap<>();

	private final Function<ItemStack, ItemStack> DEFAULT_FUNCTION = itemStack -> itemStack;
	//DIAMOND
	private final ItemAttributes DIAMOND_SWORD = getDamageModifier(6.0);
	private final ItemAttributes DIAMOND_AXE = getDamageModifier(5.0);
	private final ItemAttributes DIAMOND_SPADE = getDamageModifier(4.5);
	private final ItemAttributes DIAMOND_PICKAXE = getDamageModifier(4.0);
	//IRON
	private final ItemAttributes IRON_SWORD = getDamageModifier(5.0);
	private final ItemAttributes IRON_AXE = getDamageModifier(4.0);
	private final ItemAttributes IRON_SPADE = getDamageModifier(3.5);
	private final ItemAttributes IRON_PICKAXE = getDamageModifier(3.0);
	//GOLD
	private final ItemAttributes GOLD_SWORD = getDamageModifier(4.5);
	private final ItemAttributes GOLD_AXE = getDamageModifier(3.5);
	private final ItemAttributes GOLD_SPADE = getDamageModifier(3.0);
	private final ItemAttributes GOLD_PICKAXE = getDamageModifier(2.5);
	//STONE
	private final ItemAttributes STONE_SWORD = getDamageModifier(4.0);
	private final ItemAttributes STONE_AXE = getDamageModifier(3.0);
	private final ItemAttributes STONE_SPADE = getDamageModifier(2.5);
	private final ItemAttributes STONE_PICKAXE = getDamageModifier(2.0);
	//WOOD
	private final ItemAttributes WOOD_SWORD = getDamageModifier(3.0);
	private final ItemAttributes WOOD_AXE = getDamageModifier(2.0);
	private final ItemAttributes WOOD_SPADE = getDamageModifier(1.5);
	private final ItemAttributes WOOD_PICKAXE = getDamageModifier(1.0);
	//MISCs
	private final ItemAttributes ADMIN_ENDER = getDamageModifier(1337.0);

	static {
		//DIAMOND
		WEAPON_DAMAGE_MODIFIERS.put(Material.DIAMOND_SWORD, setDamageFlatFunction(DIAMOND_SWORD));
		WEAPON_DAMAGE_MODIFIERS.put(Material.DIAMOND_AXE, setDamageFlatFunction(DIAMOND_AXE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.DIAMOND_SHOVEL, setDamageFlatFunction(DIAMOND_SPADE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.DIAMOND_PICKAXE, setDamageFlatFunction(DIAMOND_PICKAXE));
		//IRON
		WEAPON_DAMAGE_MODIFIERS.put(Material.IRON_SWORD, setDamageFlatFunction(IRON_SWORD));
		WEAPON_DAMAGE_MODIFIERS.put(Material.IRON_AXE, setDamageFlatFunction(IRON_AXE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.IRON_SHOVEL, setDamageFlatFunction(IRON_SPADE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.IRON_PICKAXE, setDamageFlatFunction(IRON_PICKAXE));
		//GOLD
		WEAPON_DAMAGE_MODIFIERS.put(Material.GOLDEN_SWORD, setDamageFlatFunction(GOLD_SWORD));
		WEAPON_DAMAGE_MODIFIERS.put(Material.GOLDEN_AXE, setDamageFlatFunction(GOLD_AXE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.GOLDEN_SHOVEL, setDamageFlatFunction(GOLD_SPADE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.GOLDEN_PICKAXE, setDamageFlatFunction(GOLD_PICKAXE));
		//STONE
		WEAPON_DAMAGE_MODIFIERS.put(Material.STONE_SWORD, setDamageFlatFunction(STONE_SWORD));
		WEAPON_DAMAGE_MODIFIERS.put(Material.STONE_AXE, setDamageFlatFunction(STONE_AXE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.STONE_SHOVEL, setDamageFlatFunction(STONE_SPADE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.STONE_PICKAXE, setDamageFlatFunction(STONE_PICKAXE));
		//WOOD
		WEAPON_DAMAGE_MODIFIERS.put(Material.WOODEN_SWORD, setDamageFlatFunction(WOOD_SWORD));
		WEAPON_DAMAGE_MODIFIERS.put(Material.WOODEN_AXE, setDamageFlatFunction(WOOD_AXE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.WOODEN_SHOVEL, setDamageFlatFunction(WOOD_SPADE));
		WEAPON_DAMAGE_MODIFIERS.put(Material.WOODEN_PICKAXE, setDamageFlatFunction(WOOD_PICKAXE));
		//MISCs
		WEAPON_DAMAGE_MODIFIERS.put(Material.ENDER_CHEST, setDamageFlatFunction(ADMIN_ENDER));
	}

	private Function<ItemStack, ItemStack> setDamageFlatFunction(ItemAttributes attributes) {
		return attributes::apply;
	}

	private ItemAttributes getDamageModifier(double damage) {
		ItemAttributes attributes = new ItemAttributes();
		AttributeModifier modifier = new AttributeModifier(Attribute.ATTACK_DAMAGE, "Damage",
				Slot.MAIN_HAND, 0, damage, UUID.randomUUID());
		attributes.addModifier(modifier);
		return attributes;
	}

	public @Nullable
	ItemStack processItemStack(@Nonnull ItemStack input) {
		return WEAPON_DAMAGE_MODIFIERS.getOrDefault(input.getType(), DEFAULT_FUNCTION).apply(input);
	}

}
