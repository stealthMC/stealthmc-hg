package net.stealthmc.hgcore.game.gamephase;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.game.GameHandler;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.World;

import java.util.HashMap;
import java.util.Map;

public class GameRuleGamePhaseUpdateHandler implements GamePhaseUpdateHandler {

	private static final Map<GamePhase, Map<GameRule, String>> gameRuleValues = new HashMap<>();

	static {
		//Erst in Lobby Phase - in WORLD ist Welt noch nicht gesetzt
		registerGameRuleValue(GamePhase.LOBBY, GameRule.DO_FIRE_TICK, "false");
		registerGameRuleValue(GamePhase.IMMORTALITY, GameRule.DO_FIRE_TICK, "true");

		registerGameRuleValue(GamePhase.LOBBY, GameRule.DO_MOB_SPAWNING, "false");
		registerGameRuleValue(GamePhase.IMMORTALITY, GameRule.DO_MOB_SPAWNING, "true");

		registerGameRuleValue(GamePhase.LOBBY, GameRule.DO_MOB_LOOT, "false");
		registerGameRuleValue(GamePhase.IMMORTALITY, GameRule.DO_MOB_LOOT, "true");

		registerGameRuleValue(GamePhase.LOBBY, GameRule.DO_WEATHER_CYCLE, "false");
		registerGameRuleValue(GamePhase.IMMORTALITY, GameRule.DO_WEATHER_CYCLE, "true");
	}

	private static void registerGameRuleValue(GamePhase phase, GameRule gameRule, String value) {
		Map<GameRule, String> gameRuleMap = gameRuleValues.computeIfAbsent(phase, gamePhase -> new HashMap<>());
		gameRuleMap.put(gameRule, value);
	}

	@Override
	public void onGamePhaseUpdate(GameHandler gameHandler) {
		GamePhase phase = gameHandler.getCurrentPhase();
		Map<GameRule, String> gameRuleMap = gameRuleValues.get(phase);
		if (gameRuleMap == null) {
			return;
		}

		World world = gameHandler.getGameWorld();

		if (world == null) {
			return;
		}
		Bukkit.getConsoleSender().sendMessage("");
		Bukkit.getConsoleSender().sendMessage(CC.darkGray + "---------------------[ " + CC.bGold + "stealthmc Gamerules" + CC.darkGray + " ]---------------------");
		for (Map.Entry<GameRule, String> entry : gameRuleMap.entrySet()) {
			world.setGameRule(entry.getKey(), entry.getValue());
			String defaultGameRule = entry.getKey().toString();
			String fixedGameRuleStart = defaultGameRule.replace("GameRule{key=", "");
			String fixedGameRuleEnd = fixedGameRuleStart.replace(", type=class java.lang.Boolean}", "");
			Bukkit.getConsoleSender().sendMessage(CC.darkGray + "Phase: " + CC.red + phase.toString() + CC.darkGray + " | GameRule: " + CC.red + fixedGameRuleEnd + CC.darkGray + " | Value: " + CC.red + entry.getValue().toUpperCase());
		}
		Bukkit.getConsoleSender().sendMessage(CC.darkGray + "----------------------------------------------------------------");
		Bukkit.getConsoleSender().sendMessage("");
	}

}
