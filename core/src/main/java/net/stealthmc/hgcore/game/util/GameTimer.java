package net.stealthmc.hgcore.game.util;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.stealthmc.hgcore.HGBukkitMain;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;

@Getter
public abstract class GameTimer {

	@Setter
	private boolean running = false;

	private Map<Integer, Runnable> callBacks = new HashMap<>();

	@Setter
	private Runnable tickHandler;

	@Getter(AccessLevel.PRIVATE)
	private BukkitTask task;

	//Time in ticks
	private int time;

	public void addCallback(int time, Runnable callBack) {
		callBacks.put(time, callBack);
	}

	public void start() {
		running = true;
		task = Bukkit.getScheduler().runTaskTimer(HGBukkitMain.getInstance(), this::onTickInternal, 20L, 20L);
	}

	public void stop() {
		running = false;
		if (task != null) {
			task.cancel();
		}
	}

	public void setTime(int time) {
		setTimeNoUpdate(time);
		update();
	}

	public void setTimeNoUpdate(int time) {
		this.time = time;
	}

	protected void update() {
		int time = getTime();
		getCallBacks().getOrDefault(time, () -> {
		}).run();

		if (getTickHandler() != null) {
			getTickHandler().run();
		}
	}

	private void onTickInternal() {
		if (running) {
			onTick();
		}
	}

	protected abstract void onTick();

	public static class CountdownTimer extends GameTimer {

		@Override
		protected void onTick() {
			setTimeNoUpdate(getTime() - 1);
			update();
		}

	}

	public static class StopwatchTimer extends GameTimer {

		@Override
		protected void onTick() {
			setTimeNoUpdate(getTime() + 1);
			update();
		}
	}

}
