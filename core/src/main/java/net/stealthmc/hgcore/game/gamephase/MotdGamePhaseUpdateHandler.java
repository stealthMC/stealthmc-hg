package net.stealthmc.hgcore.game.gamephase;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.nms.NmsServerUtils;
import net.stealthmc.hgcommon.util.MapUtils;
import net.stealthmc.hgcore.game.GameHandler;

import java.util.HashMap;
import java.util.Map;

public class MotdGamePhaseUpdateHandler implements GamePhaseUpdateHandler {

	private static final String statusString = CC.bYellow + "Status";
	private static final String arrowString = CC.gray + " » ";
	private static final String damageString = CC.gold + "Damage: ";
	private static final String respawnString = CC.gold + "Respawn: ";
	private static final String RejoinString = CC.gold + "Rejoin: ";
	private static final String offString = CC.red + "OFF";
	private static final String onString = CC.green + "ON";
	private static final String splitString = CC.gray + " ｜ ";

	private static final Map<GamePhase, String> PHASE_MOTDS = MapUtils.of(HashMap::new,
			GamePhase.WORLD, CC.red + "Generating mcpvpONE-HG world... \n" +
					CC.red + "Do not join yet!",

			GamePhase.LOBBY, CC.green + "Waiting for player... \n" +
					CC.green + "Join now and play Hardcore Games!",

			GamePhase.PRE_START, CC.yellow + "Starting soon... \n" +
					CC.yellow + "Last chance for you!",

			GamePhase.IMMORTALITY, CC.red + "Game in progress... \n" +
					statusString + arrowString + damageString + offString + splitString + respawnString + onString + splitString + RejoinString + onString,

			GamePhase.START, CC.red + "Game in progress... \n" +
					statusString + arrowString + damageString + onString + splitString + respawnString + onString + splitString + RejoinString + onString,

			GamePhase.PLAYING, CC.red + "Game in progress... \n" +
					statusString + arrowString + damageString + onString + splitString + respawnString + offString + splitString + RejoinString + offString,

			GamePhase.SHOWDOWN, CC.red + "Game in progress... \n" +
					CC.red + "FINAL FIGHT IS HAPPENING!",

			GamePhase.END, CC.darkRed + "Game finished... \n" +
					CC.darkRed + "Server is now restarting!",

			GamePhase.DEBUG, "Debugging.");

	@Override
	public void onGamePhaseUpdate(GameHandler gameHandler) {
		String motd = PHASE_MOTDS.get(GameHandler.getInstance().getCurrentPhase());
		if (motd == null) {
			return;
		}

		NmsServerUtils.setMotd(motd);
	}

}
