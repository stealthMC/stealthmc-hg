package net.stealthmc.hgcore.game;

import com.google.common.collect.Sets;
import lombok.experimental.UtilityClass;
import net.stealthmc.hgcore.HGBukkitMain;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import java.util.Iterator;
import java.util.Set;

@UtilityClass
public class CraftingRecipeHandler {

	public final ShapelessRecipe CACTUS_SOUP_RECIPE;

	public final ShapelessRecipe COCOA_SOUP_RECIPE;
	private Set<Recipe> addedRecipes = Sets.newHashSet();

	static {
		CACTUS_SOUP_RECIPE = new ShapelessRecipe(new NamespacedKey(HGBukkitMain.getInstance(), "CactusSoup"), new ItemStack(Material.MUSHROOM_STEW));
		CACTUS_SOUP_RECIPE.addIngredient(Material.CACTUS);
		CACTUS_SOUP_RECIPE.addIngredient(Material.CACTUS);
		CACTUS_SOUP_RECIPE.addIngredient(Material.BOWL);

		COCOA_SOUP_RECIPE = new ShapelessRecipe(new NamespacedKey(HGBukkitMain.getInstance(), "CocoaSoup"), new ItemStack(Material.MUSHROOM_STEW));
		COCOA_SOUP_RECIPE.addIngredient(Material.COCOA_BEANS);
		COCOA_SOUP_RECIPE.addIngredient(Material.BOWL);
	}

	public void registerSoupRecipes() {
		registerCactiSoupRecipe();
		registerCocoaSoupRecipe();
	}

	private void registerCactiSoupRecipe() {
		registerRecipe(CACTUS_SOUP_RECIPE);
	}

	private void registerCocoaSoupRecipe() {
		registerRecipe(COCOA_SOUP_RECIPE);
	}

	public void registerRecipe(Recipe recipe) {
		if (addedRecipes.add(recipe)) {
			Bukkit.addRecipe(recipe);
		}
	}

	public void removeAddedRecipes() {
		Iterator<Recipe> recipeIterator = Bukkit.recipeIterator();
		Recipe current;
		while (recipeIterator.hasNext()) {
			current = recipeIterator.next();
			if (addedRecipes.contains(current)) {
				recipeIterator.remove();
			}
		}
		addedRecipes.clear();
	}

	public boolean areEqual(ShapedRecipe recipe1, ShapedRecipe recipe2) {
		if (!recipe1.getResult().equals(recipe2.getResult())) {
			return false;
		}
		int len = recipe1.getShape().length; //are rectangular
		if (len != recipe2.getShape().length) {
			return false;
		}
		for (int a = 0; a < len; a++) {
			for (int b = 0; b < len; b++) {
				ItemStack i1 = recipe1.getIngredientMap().getOrDefault(recipe1.getShape()[a].charAt(b), null);
				ItemStack i2 = recipe2.getIngredientMap().getOrDefault(recipe2.getShape()[a].charAt(b), null);
				if (i1 == null && i2 == null) {
					continue;
				}
				if (i1 == null || i2 == null) {
					return false;
				}
				if (!i1.equals(i2)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean areEqual(ShapelessRecipe recipe1, ShapelessRecipe recipe2) {
		if (!recipe1.getResult().equals(recipe2.getResult())) {
			return false;
		}
		return recipe2.getIngredientList().containsAll(recipe1.getIngredientList())
				&& recipe1.getIngredientList().containsAll(recipe2.getIngredientList());
	}

}
