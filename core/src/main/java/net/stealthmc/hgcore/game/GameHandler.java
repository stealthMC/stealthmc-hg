package net.stealthmc.hgcore.game;

import com.destroystokyo.paper.event.player.PlayerInitialSpawnEvent;
import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Sets;
import lombok.Getter;
import net.stealthmc.hgcommon.Permission;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.bukkit.inventory.InventoryMenu;
import net.stealthmc.hgcommon.bukkit.scoreboard.PlayerScoreboard;
import net.stealthmc.hgcommon.bukkit.scoreboard.ScoreboardLine;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.config.Messages;
import net.stealthmc.hgcore.game.entity.HGEntity;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgcore.game.gamephase.GamePhase;
import net.stealthmc.hgcore.game.gamephase.GamePhaseUpdateHandler;
import net.stealthmc.hgcore.game.util.BorderManager;
import net.stealthmc.hgcore.game.util.GameTimer;
import org.apache.commons.lang.Validate;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import org.spigotmc.event.entity.EntityDismountEvent;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("UnstableApiUsage")
public class GameHandler {

	public static final int INITIAL_SCOREBOARD_TIMER_TITLE_LINE = 15;
	public static final int INITIAL_SCOREBOARD_PLAYER_COUNT_LINE = 10;
	public static final double WORLDBORDER_INITIAL_SIZE = 1000;
	public static final double WORLDBORDER_DAMAGE_RADIUS = 5;
	@Getter(lazy = true)
	private static final GameHandler instance = new GameHandler();
	private static final double SPREAD_DISTANCE = 0.5;
	private static final int SPREAD_MAX_RANGE = 31;
	private static final double WORLDBORDER_SHOWDOWN_SIZE = 100;
	private static final double WORLDBORDER_SHRINKED_MINSIZE = 30;
	private static final double WORLDBORDER_SHRINK_BLOCKS_PER_MINUTE = 5;
	private static final double SPREAD_SHOWDOWN_DISTANCE = 10;
	private static final int SPREAD_SHOWDOWN_RANGE = 40;
	private static final Messages messages = HGBukkitMain.getInstance().getMessages();
	private static final int LOBBY_SECONDS = 3 * 60; //7 Minuten, 20 Ticks * 60 Sekunden * 7
	private static final double COMPASS_CLOSE_RANGE = 20.0; //Spieler innerhalb dieses Radius werden zuletzt beachtet
	private static final int MAX_BUILD_HEIGHT = 128;

	@Getter
	private GamePhase currentPhase = GamePhase.WORLD;

	@Getter
	private World gameWorld;

	private String latestScoreboardTimerName = null;

	private Set<HGPlayer> handleablePlayerSet = Sets.newHashSet();    //Spieler, die vom Plugin berücksichtig werden sollen

	//Spieler, die dem Spiel noch beitreten dürfen, wenn sie gerade gegangen sind
	private Cache<UUID, Boolean> recentlyLeft = CacheBuilder.newBuilder()
			.expireAfterWrite(1L, TimeUnit.MINUTES)
			.build();

	@Getter
	private GameTimer gameTimer;

	@Getter
	private long gameStartMillis = 0;

	private boolean allPlayersHalted = false;

	private boolean timerStarted = false;

	@Getter
	private List<Runnable> secondsHandler = new CopyOnWriteArrayList<>();

	@Getter
	private Queue<Runnable> secondsSyncedOneTimeRunnables = new ArrayDeque<>();

	@Getter
	private List<Runnable> ticksHandler = new CopyOnWriteArrayList<>();

	@Getter
	private Set<UUID> permittedBuilders = new HashSet<>();

	@Getter
	private Map<UUID, ScoreboardLine> playerTimerLines = new HashMap<>();

	@Getter
	private Map<UUID, ScoreboardLine> playerPlayerCountLines = new HashMap<>();

	@Getter
	private Set<GamePhaseUpdateHandler> gamePhaseUpdateHandlers = new HashSet<>();

	private Map<UUID, ScoreboardLine> killCounterLines = new HashMap<>();

	private Map<UUID, ScoreboardLine> borderSizeLines = new HashMap<>();

	private BorderManager borderManager;

	private BukkitRunnable secondsExecutor = new BukkitRunnable() {
		@Override
		public void run() {
			secondsHandler.forEach(Runnable::run);
		}
	};

	private BukkitRunnable ticksExecutor = new BukkitRunnable() {
		@Override
		public void run() {
			ticksHandler.forEach(Runnable::run);
		}
	};

	private Map<UUID, Inventory> activeInventories = new HashMap<>();

	/**
	 * Setzt die Spielwelt, falls diese noch nicht initialisiert wurde.
	 * Wechselt außerdem gleich in die nächste Phase.
	 *
	 * @param world Spielwelt
	 */
	public void setGameWorld(@Nonnull World world) {
		if (gameWorld == null) {
			gameWorld = world;
		}

		if (currentPhase == GamePhase.WORLD) {
			setCurrentPhase(currentPhase.nextPhase());
		}

		borderManager = new BorderManager(world);
		borderManager.setBorderDiameterChangedConsumer(this::onBorderSizeChanged);
		borderManager.setDiameter(borderManager.calculateDiameter(0));

		secondsExecutor.runTaskTimer(HGBukkitMain.getInstance(), 20L, 20L);
		ticksExecutor.runTaskTimer(HGBukkitMain.getInstance(), 1L, 1L);
		setupExecutors();
	}

	private void setupExecutors() {
		setupBorderDamageExecutor();
		setupCompassTargetExecutor();
		setupSpectatorRetainerExecutor();
		setupSyncedOneTimeRunnableExecutor();
	}

	private void setupBorderDamageExecutor() {
		secondsHandler.add(() -> {
			if (!currentPhase.isEntityDamageEnabled()) {
				return;
			}
			getPlayingPlayersStream()
					.filter(borderManager::isNearBorder)
					.forEach(nearby -> {
						nearby.damage(4);
					});
		});
	}

	private void setupCompassTargetExecutor() {
		secondsHandler.add(() -> {
			forPlayingPlayers(player -> {
				HGPlayer hgPlayer = HGPlayer.from(player);
				if (hgPlayer.getPlayerCompassTarget() == null) {
					return;
				}
				HGPlayer hgTarget = HGPlayer.from(hgPlayer.getPlayerCompassTarget());
				Player target = hgTarget.getPlayer();
				if (target == null || !target.isOnline() || target.isDead() || target.getGameMode() == GameMode.SPECTATOR) {
					return;
				}
				player.setCompassTarget(target.getLocation());
			});
		});
	}

	private void setupSpectatorRetainerExecutor() {
		ticksHandler.add(() -> {
			WorldBorder border = gameWorld.getWorldBorder();
			double radius = border.getSize() / 2;
			forSpectatingPlayers(p -> {
				Location playerLocation = p.getLocation();
				if (!playerLocation.getWorld().equals(gameWorld)) {
					return;
				}
				double distX = playerLocation.getX() - border.getCenter().getX();
				double distZ = playerLocation.getZ() - border.getCenter().getZ();
				if (Math.abs(distX) < radius - 1
						&& Math.abs(distZ) < radius - 1) {
					return;
				}
				Location origin = new Location(gameWorld, border.getCenter().getX(), playerLocation.getY(),
						border.getCenter().getZ());
				Vector dir = origin.toVector().subtract(playerLocation.toVector()).normalize().multiply(10);
				p.teleport(playerLocation.add(dir));
			});
		});
	}

	private void setupSyncedOneTimeRunnableExecutor() {
		secondsHandler.add(() -> {
			while (this.secondsSyncedOneTimeRunnables.peek() != null) {
				secondsSyncedOneTimeRunnables.poll().run();
			}
		});
	}

	private void onBorderSizeChanged(double newDiameter) {
		if (!currentPhase.isJoiningEnabled()) {
			return;
		}

		forHandleAblePlayers(this::updateBorderLine);
	}

	private void updateBorderLine(Player player) {
		UUID playerId = player.getUniqueId();
		ScoreboardLine killLine = killCounterLines.get(playerId);

		if (killLine == null) {
			return;
		}

		ScoreboardLine borderLine = borderSizeLines.compute(playerId, (pId, line) -> {
			if (line == null || line.isDisposed()) {
				return killLine.belowBlock();
			}
			return line;
		});

		borderLine.update(PlayerUtils.colorize(messages.getScoreboard_borderSizeName()));

		int radius = (int) borderManager.getDiameter();

		borderLine = borderLine.makeSoftSpaceBelow().getBelow();
		String format = String.format(messages.getScoreboard_borderSizeFormat(), radius, radius);
		borderLine.update(PlayerUtils.colorize(format));

		borderLine.makeSpaceBelow();
	}

	private void removeBorderLine(Player player) {
		ScoreboardLine borderLine = borderSizeLines.get(player.getUniqueId());

		if (borderLine == null) {
			return;
		}

		borderLine.dispose();
		borderLine = borderLine.getBelow();
		borderLine.dispose();
		borderLine = borderLine.getBelow();
		borderLine.dispose();
	}

	public void handleLogin(PlayerLoginEvent event) {
		Messages messages = HGBukkitMain.getInstance().getMessages();
		if (currentPhase == GamePhase.WORLD) {
			event.disallow(PlayerLoginEvent.Result.KICK_OTHER, PlayerUtils.colorize(messages.getCannotConnectWorldPhase()));
			return;
		}
		if (currentPhase == GamePhase.END) {
			event.disallow(PlayerLoginEvent.Result.KICK_OTHER, PlayerUtils.colorize(messages.getShutdownKick()));
			return;
		}
		HGPlayer hgPlayer = HGPlayer.from(event.getPlayer());
		boolean canRejoin = event.getPlayer().hasPermission(Permission.REJOIN_GAMES)
				|| currentPhase.isJoiningEnabled()
				|| recentlyLeft.asMap().containsKey(event.getPlayer().getUniqueId());
		if (currentPhase.isRespawningEnabled() && !canRejoin && hgPlayer.isInitialPlayer()) {
			event.disallow(PlayerLoginEvent.Result.KICK_OTHER, PlayerUtils.colorize(messages.getCannotRejoin()));
			return;
		}
		if (currentPhase.isRespawningEnabled() && canRejoin && hgPlayer.isInitialPlayer()) {
			return;
		}
		boolean canJoinRunningGames = event.getPlayer().hasPermission(Permission.JOIN_RUNNING_GAMES);
		if (!currentPhase.isJoiningEnabled() && !canJoinRunningGames) {
			event.disallow(PlayerLoginEvent.Result.KICK_OTHER, PlayerUtils.colorize(messages.getCannotConnectPlaying()));
		}
	}

	public void handleInitialSpawn(PlayerInitialSpawnEvent event) {
		Location spawnLocation = getCenterRandomLocation(SPREAD_MAX_RANGE);
		HGPlayer hgPlayer = HGPlayer.from(event.getPlayer());
		handleablePlayerSet.add(hgPlayer);

		Location logoutLocation = hgPlayer.getLogoutLocation();
		if (logoutLocation != null) {
			spawnLocation = logoutLocation;
		}

		if (currentPhase.isJoiningEnabled()) {
			hgPlayer.setInitialPlayer(true);
			addPlayingPlayer(hgPlayer);
			event.setSpawnLocation(spawnLocation);
			event.getPlayer().setLastDamageCause(null);
			borderManager.setDiameter(borderManager.calculateDiameter(countPlayersAlive()));
		}
	}

	public boolean hasActiveInventory(UUID player) {
		return activeInventories.containsKey(player);
	}

	public Inventory getActiveInventory(UUID player){
		return activeInventories.get(player);
	}

	public void setActiveInventory(UUID player, Inventory inv){
		activeInventories.put(player, inv);
	}

	public Location getCenterSpawnLocation() {
		return gameWorld.getHighestBlockAt(0, 0).getLocation().add(0.5,3,0.5);
	}

	public Location getRandomLocation() {
		return getCenterRandomLocation((int) borderManager.getDiameter());
	}

	public Location getCenterRandomLocation(int radius) {
		Random random = ThreadLocalRandom.current();
		int x = random.nextInt(radius * 2) - radius + borderManager.getCenterX();
		int z = random.nextInt(radius * 2) - radius + borderManager.getCenterZ();
		return gameWorld.getHighestBlockAt(x, z).getLocation().add(0.5,3,0.5);
	}

	public void handleJoin(PlayerJoinEvent event) {
		if (!isHandleablePlayer(event.getPlayer())) {
			return;
		}

		hideHiddenPlayers(event.getPlayer());

		HGPlayer hgPlayer = HGPlayer.from(event.getPlayer());

		if (currentPhase.isJoiningEnabled() || (hgPlayer.isInitialPlayer() && !hgPlayer.isSpectating())) {
			resetPlayerState(event.getPlayer());
		} else {
			addLatePlayer(event.getPlayer());
		}

		if (allPlayersHalted) {
			haltPlayer(event.getPlayer());
		}

		if (countPlayersAlive() >= 2) {
			//maybe someone wants to just spectate, they are already in spectator mode.
			//once the timer has timerStarted however, it is not stopped.
			startLobbyTimer();
		}

		registerScoreboardTimerImmediate(event.getPlayer(),
				latestScoreboardTimerName == null ? PlayerUtils.colorize(messages.getScoreboard_timerWaitingName())
						: latestScoreboardTimerName);

		PlayerUtils.sendMessage(event.getPlayer(), messages.getPlayerWelcome());
		forHandleAblePlayers(this::updatePlayerCountScoreboard);
		updateKillsLine(event.getPlayer());

		Bukkit.getScheduler().runTaskLater(HGBukkitMain.getInstance(),
				() -> forHandleAblePlayers(this::updateBorderLine), 1L);
	}

	public void handleQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if (!isHandleablePlayer(player)) {
			return;
		}
		boolean mayRejoin = System.currentTimeMillis() - gameStartMillis <= 1000 * 5 * 60; //5 Minuten seit Spielstart
		mayRejoin = mayRejoin && currentPhase.ordinal() < GamePhase.SHOWDOWN.ordinal(); //Nicht im Showdown
		if (mayRejoin && event.getPlayer().getLastDamageCause() == null) {
			recentlyLeft.asMap().put(event.getPlayer().getUniqueId(), true);
		}
		HGPlayer hgPlayer = HGPlayer.from(player);
		handleablePlayerSet.remove(hgPlayer);
		hgPlayer.setLogoutLocation(player.getLocation());
		ScoreboardHandler.unregisterPlayer(event.getPlayer());
		releasePlayer(player);
		checkAndHandleGameOver();
		forHandleAblePlayers(this::updatePlayerCountScoreboard);
	}

	public void handleEntityDamage(EntityDamageEvent event) {
		if (!currentPhase.isEntityDamageEnabled()) {
			event.setCancelled(true);
			return;
		}
		if (!currentPhase.isPlayerDamageEnabled() && event.getEntity() instanceof Player) {
			event.setCancelled(true);
			return;
		}
		UUID[] participants = new UUID[]{event.getEntity().getUniqueId()};
		HGEntity.allKnownEntities().forEach(hgEntity -> hgEntity.onDamage(event, participants));
	}

	public void handleEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player) {
			handlePlayerDamageEntity((Player) event.getDamager(), event.getEntity(), event);
			return;
		}
		if (event.getEntity() instanceof Player) {
			handleNonPlayerDamagePlayer(event.getDamager(), (Player) event.getEntity(), event);
			return;
		}

		//Entity damage entity
		if (!currentPhase.isEntityDamageEnabled()) {
			event.setCancelled(true);
			return;
		}

		UUID[] participants = new UUID[]{event.getEntity().getUniqueId(), event.getDamager().getUniqueId()};
		HGEntity.allKnownEntities().forEach(e -> e.onEntityDamage(event, participants));
	}

	public void handlePlayerDamageEntity(Player damager, Entity entity, EntityDamageByEntityEvent event) {
		if (!isHandleablePlayer(damager)) {
			return;
		}

		if (!entity.getPassengers().isEmpty()) {
			Entity passenger = entity.getPassengers().get(0);
			if (passenger != null && passenger.getType() == EntityType.ARMOR_STAND) {
				event.setCancelled(true);
				return;
			}
		}

		boolean entityIsPlayer = entity instanceof Player;
		if (!entityIsPlayer) {
			if (!currentPhase.isEntityDamageEnabled()) {
				event.setCancelled(true);
				return;
			}
		} else {
			if (!currentPhase.isPlayerDamageEnabled()) {
				event.setCancelled(true);
				return;
			}
		}

		UUID[] participants = new UUID[]{event.getEntity().getUniqueId(), event.getDamager().getUniqueId()};

		HGEntity.allKnownEntities().forEach(hgEntity -> hgEntity.onEntityDamage(event, participants));

		if (event.isCancelled()) {
			return;
		}

		if (entityIsPlayer) {
			HGPlayer hgPlayer = HGPlayer.from((Player) entity);
			hgPlayer.setLastDamager(damager.getUniqueId());
		}
	}

	public void handleNonPlayerDamagePlayer(Entity damager, Player player, EntityDamageByEntityEvent event) {
		if (!isHandleablePlayer(player)) {
			return;
		}
		if (!currentPhase.isPlayerDamageEnabled()) {
			event.setCancelled(true);
			return;
		}

		UUID[] participants = new UUID[]{damager.getUniqueId(), player.getUniqueId()};

		HGEntity.allKnownEntities().forEach(hgEntity -> hgEntity.onEntityDamage(event, participants));
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onEntityDamage(event, participants));
	}

	public void handlePlayerDamageByBlock(EntityDamageByBlockEvent event) {
		Preconditions.checkArgument(event.getEntity() instanceof Player);
		if (!isHandleablePlayer((Player) event.getEntity())) {
			return;
		}
		if (!currentPhase.isEntityDamageEnabled()) {
			event.setCancelled(true);
			//noinspection UnnecessaryReturnStatement
			return;
		}
	}

	public void handleDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();

		UUID[] participants = new UUID[]{event.getEntity().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onDeath(event, participants));

		Player killer = player.getKiller();
		if (killer != null) {
			HGPlayer hgKiller = HGPlayer.from(killer);
			hgKiller.setKills(hgKiller.getKills() + 1);
			updateKillsLine(killer);
		}

		if (currentPhase.isRespawningEnabled() && player.hasPermission(Permission.RESPAWN_IN_GAMES)) {
			respawnPlayer(event.getEntity());
			return;
		}

		if (!player.hasPermission(Permission.SPECTATE_GAMES)) {
			player.kickPlayer(PlayerUtils.colorize(messages.getYouDiedKick()));
			checkAndHandleGameOver();
			return;
		}

		player.setBedSpawnLocation(player.getLocation(), true);
		player.spigot().respawn();
		addSpectatingPlayer(event.getEntity());
		forHandleAblePlayers(this::updatePlayerCountScoreboard);
		checkAndHandleGameOver();
	}

	public void handleHunger(FoodLevelChangeEvent event) {
		Preconditions.checkArgument(event.getEntity() instanceof Player);
		if (!isHandleablePlayer((Player) event.getEntity())) {
			return;
		}
		if (!currentPhase.isEntityDamageEnabled()) {
			event.setFoodLevel(24);
			//noinspection UnnecessaryReturnStatement
			return;
		}
	}

	public void handleDismount(EntityDismountEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (HGPlayer.allKnownPlayers().stream()
				.map(HGPlayer::getPlayerHalter)
				.filter(Objects::nonNull)
				.anyMatch(halter -> halter.equals(event.getDismounted()))) {
			event.setCancelled(true);
			return;
		}

		UUID[] participants = new UUID[]{event.getDismounted().getUniqueId(), event.getEntity().getUniqueId()};
		HGEntity.allKnownEntities().forEach(e -> e.onDismount(event, participants));
	}

	public void handleBlockBreak(BlockBreakEvent event) {
		if (permittedBuilders.contains(event.getPlayer().getUniqueId())) {
			return;
		}
		if (currentPhase.isJoiningEnabled()) {
			event.setCancelled(true);
			return;
		}
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onBlockBreak(event, participants));
	}

	public void handleBlockPlace(BlockPlaceEvent event) {
		if (permittedBuilders.contains(event.getPlayer().getUniqueId())) {
			return;
		}
		if (currentPhase.isJoiningEnabled()) {
			event.setCancelled(true);
			return;
		}
		if (event.getBlockPlaced().getY() >= MAX_BUILD_HEIGHT) {
			event.setCancelled(true);
			event.setBuild(false);
			PlayerUtils.sendMessage(event.getPlayer(), String.format(messages.getMaxBuildHeight(), MAX_BUILD_HEIGHT));
			return;
		}
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onBlockPlace(event, participants));
	}

	public void handleInteract(PlayerInteractEvent event) {
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId()};

		//In case the event1 incoming is already cancelled, which would disrupt flow.
		Event.Result handStatus = event.useItemInHand();
		Event.Result blockStatus = event.useInteractedBlock();

		event.setUseInteractedBlock(Event.Result.DEFAULT);
		event.setUseItemInHand(Event.Result.DEFAULT);

		//Handle privileged Game Action (Soup, Compass)
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
			handleRightClick(event);
		}
		if (event.isCancelled()) {    //DEPRECATED
			// https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/event/player/PlayerInteractEvent.html#isCancelled--
			return;
		}

		//Handle secondary Action (Kit Selector, Specific Kit Action)
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onInteract(event, participants));
		if (event.isCancelled()) { //DEPRECATED
			return;
		}

		//Handle tertiary Action (Building)
		if (getCurrentPhase().isJoiningEnabled() && !permittedBuilders.contains(event.getPlayer().getUniqueId())) {
			event.setCancelled(true);
			return;
		}

		//None of this code reacted
		event.setUseItemInHand(handStatus);
		event.setUseInteractedBlock(blockStatus);
	}

	public void handleInteractEntity(PlayerInteractEntityEvent event) {
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId(), event.getRightClicked().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onInteractEntity(event, participants));
	}

	public void handleExpBottle(ExpBottleEvent event) {
		UUID[] participants = new UUID[]{event.getEntity().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onExpBottle(event, participants));
	}

	public void handlePlayerExpChange(PlayerExpChangeEvent event) {
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onPlayerExpChange(event, participants));
	}

	public void handleEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {

		if (!currentPhase.isMobTargetingPlayer()) {
			event.setTarget(null);
			event.setCancelled(true);
		}

	}

	private void handleRightClick(PlayerInteractEvent event) {
		ItemStack itemStack = event.getItem();
		if (itemStack == null) {
			return;
		}
		if (itemStack.getType() == Material.MUSHROOM_STEW) {
			if (!handleSoupHeal(event.getPlayer())) {
				return;
			}
			event.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.BOWL));
			event.getPlayer().updateInventory();
			event.setCancelled(true);
			return;
		}
		if (itemStack.getType() == Material.COMPASS) {
			handleCompassTargetChange(event.getPlayer());
			event.setCancelled(true);
		}
	}

	private boolean handleSoupHeal(Player player) {
		if (player.getHealth() < Objects.requireNonNull(player.getAttribute(Attribute.GENERIC_MAX_HEALTH)).getDefaultValue()) {
			player.setHealth(Math.min(Objects.requireNonNull(player.getAttribute(Attribute.GENERIC_MAX_HEALTH)).getDefaultValue(), player.getHealth() + 6));
			return true;
		}
		if (player.getFoodLevel() < 20) {
			float saturation = player.getFoodLevel() + 6 - 20;
			player.setFoodLevel(Math.min(20, player.getFoodLevel() + 6));
			if (saturation > 0) {
				player.setSaturation(saturation);
			}
			return true;
		}
		return false;
	}

	private void handleCompassTargetChange(Player player) {
		Location playerLocation = player.getLocation();
		UUID targetId = getPlayingPlayersStream()
				.filter(p -> !p.equals(player))
				.filter(p -> p.getLocation().getWorld().equals(playerLocation.getWorld()))
				.sorted(Comparator.comparingDouble(p -> p.getLocation().distanceSquared(playerLocation)))
				.sorted((p1, p2) -> {
					double dist = p1.getLocation().distanceSquared(p2.getLocation());
					if (dist <= COMPASS_CLOSE_RANGE * COMPASS_CLOSE_RANGE) {
						return 1;
					}
					return 0;
				})
				.map(Player::getUniqueId)
				.findFirst()
				.orElse(null);
		if (targetId == null) {
			PlayerUtils.sendMessage(player, messages.getNoCompassTargetFound());
			return;
		}
		Player target = Bukkit.getPlayer(targetId);
		if (target == null) {
			PlayerUtils.sendMessage(player, messages.getNoCompassTargetFound());
			return;
		}
		PlayerUtils.sendMessage(player, String.format(messages.getCompassTargetFound(), target.getName()));
		HGPlayer.from(player).setPlayerCompassTarget(targetId);
	}

	public void handleTeleport(PlayerTeleportEvent event) {
		Player player = event.getPlayer();
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onTeleport(event, participants));
		if (event.isCancelled()) {
			return;
		}
		if (!isHandleablePlayer(player)) {
			return;
		}
		if (player.getGameMode() != GameMode.SPECTATOR) {
			return;
		}
		if (player.getSpectatorTarget() == null) {
			return;
		}
		if (!(player.getSpectatorTarget() instanceof Player)) {
			return;
		}
		event.setCancelled(true);
		openInventoryOfPlayer(event.getPlayer(), (Player) player.getSpectatorTarget());
		player.setSpectatorTarget(null);
	}

	public void handleCraftItem(CraftItemEvent event) {
		UUID[] participants = new UUID[]{event.getWhoClicked().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onCraftItem(event, participants));
	}

	public void handleDropItem(PlayerDropItemEvent event) {
		if (currentPhase.isJoiningEnabled()) {
			event.setCancelled(true);
			return;
		}
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onItemDrop(event, participants));
	}

	public void handleFish(PlayerFishEvent event) {
		UUID[] participants = Stream.of(event.getPlayer(), event.getCaught(), event.getHook())
				.filter(Objects::nonNull)
				.map(Entity::getUniqueId)
				.toArray(UUID[]::new);
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onFish(event, participants));
	}

	public void handleItemDamage(PlayerItemDamageEvent event) {
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onItemDamage(event, participants));
	}

	public void handleMove(PlayerMoveEvent event) {
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onMove(event, participants));
	}

	public void handleSneak(PlayerToggleSneakEvent event) {
		UUID[] participants = new UUID[]{event.getPlayer().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onSneak(event, participants));
	}

	public void handleInventoryClick(InventoryClickEvent event) {
		UUID[] participants = new UUID[]{event.getWhoClicked().getUniqueId()};
		HGPlayer.allKnownPlayers().forEach(hgPlayer -> hgPlayer.onInventoryClick(event, participants));
	}

	public void handleProjectileLaunch(ProjectileLaunchEvent event) {
		Projectile projectile = event.getEntity();
		ProjectileSource source = projectile.getShooter();

		UUID[] participants = Stream.of(projectile, source)
				.filter(o -> o instanceof Entity)
				.map(o -> (Entity) o)
				.map(Entity::getUniqueId)
				.toArray(UUID[]::new);

		HGEntity.allKnownEntities().forEach(hgEntity -> hgEntity.onProjectileLaunch(event, participants));
	}

	public void handleProjectileHit(ProjectileHitEvent event) {
		Projectile projectile = event.getEntity();
		ProjectileSource source = projectile.getShooter();

		UUID[] participants = Stream.of(projectile, source)
				.filter(o -> o instanceof Entity)
				.map(o -> (Entity) o)
				.map(Entity::getUniqueId)
				.toArray(UUID[]::new);

		HGEntity.allKnownEntities().forEach(hgEntity -> hgEntity.onProjectileHit(event, participants));
	}

	public void handleEntityDeath(EntityDeathEvent event) {
		UUID[] participants = new UUID[]{event.getEntity().getUniqueId()};

		HGEntity.allKnownEntities().forEach(hgEntity -> hgEntity.onEntityDeath(event, participants));
	}

	private void openInventoryOfPlayer(Player to, Player from) {
		InventoryMenu menu = new InventoryMenu(from.getName(), 5, null, null);
		for (int i = 0; i < from.getInventory().getSize(); i++) {
			ItemStack itemStack = from.getInventory().getItem(i);
			menu.addItemAndClickHandler(itemStack, i, null);
		}
		menu.addItemAndClickHandler(from.getInventory().getArmorContents()[3], 36, null);
		menu.addItemAndClickHandler(from.getInventory().getArmorContents()[2], 37, null);
		menu.addItemAndClickHandler(from.getInventory().getArmorContents()[1], 38, null);
		menu.addItemAndClickHandler(from.getInventory().getArmorContents()[0], 39, null);
		menu.open(to);
	}

	public boolean isHandleablePlayer(Player player) {
		HGPlayer hgPlayer = HGPlayer.from(player);
		return handleablePlayerSet.contains(hgPlayer);
	}

	public boolean isAlive(Player player) {
		HGPlayer hgPlayer = HGPlayer.from(player);
		return !hgPlayer.isSpectating();
	}

	public void addPlayingPlayer(HGPlayer hgPlayer) {
		Player player = hgPlayer.getPlayer();
		hgPlayer.setSpectating(false);
		if (player != null) {
			broadcast(String.format(messages.getPlayerJoined(), player.getName()));
		}
	}

	public void addLatePlayer(Player player) {
		HGPlayer hgPlayer = HGPlayer.from(player);
		if (!hgPlayer.isInitialPlayer()) {
			Bukkit.getScheduler().runTaskLater(HGBukkitMain.getInstance(), () ->
					addSpectatingPlayer(player), 1L);
			return;
		}
		addPlayingPlayer(HGPlayer.from(player));
	}

	/**
	 * Resets the given player's state:
	 * <p>
	 * Sets the player's gamemode according to the current game phase
	 * <p>
	 * Sets (max) health to 20
	 * Sets food and saturation to 20/24
	 * Clears inventory and armor
	 * Reset experience
	 *
	 * @param player
	 */
	public void resetPlayerState(Player player) {
		if (!currentPhase.isJoiningEnabled()) {
			if (player.getGameMode() == GameMode.CREATIVE) {
				return;
			} else {
				player.setGameMode(GameMode.SURVIVAL);
			}
		} else {
			player.setGameMode(GameMode.ADVENTURE);
		}
		player.setHealth(20);
		player.setSaturation(24);
		player.setFoodLevel(24);
		player.getInventory().clear();
		player.getInventory().setArmorContents(new ItemStack[4]);
		player.setExp(0f);
	}

	public void addStartingItems(Player player) {
		player.getInventory().setItem(8, new ItemStack(Material.COMPASS));
		player.getInventory().setItem(0, new ItemStack(Material.WOODEN_SWORD));

		HGPlayer hgPlayer = HGPlayer.from(player);
		hgPlayer.onBattleReady();
	}

	public void addSpectatingPlayer(Player player) {
		player.setGameMode(GameMode.SPECTATOR);
		hidePlayer(player);
		HGPlayer hgPlayer = HGPlayer.from(player);
		hgPlayer.setSpectating(true);
	}

	public void broadcast(String message) {
		forHandleAblePlayers(player -> PlayerUtils.sendMessage(player, message));
	}

	public void hideHiddenPlayers(Player player) {
		handleablePlayerSet.stream()
				.filter(HGPlayer::isHidden)
				.map(HGPlayer::getPlayer)
				.filter(Objects::nonNull)
				.forEach(player::hidePlayer); //DEPRECATED
	}

	public void hidePlayer(Player toHide) {
		HGPlayer hgPlayer = HGPlayer.from(toHide);
		hgPlayer.setHidden(true);
		forHandleAblePlayers(player -> player.hidePlayer(HGBukkitMain.getInstance(), toHide));
	}

	public void unhidePlayer(Player toUnhide) {
		HGPlayer hgPlayer = HGPlayer.from(toUnhide);
		hgPlayer.setHidden(false);
		forHandleAblePlayers(player -> player.showPlayer(HGBukkitMain.getInstance(), toUnhide));
	}

	public void respawnPlayer(Player player) {
		resetPlayerState(player);
		unhidePlayer(player);

		Location randomLoc = getCenterRandomLocation(30);

		player.setBedSpawnLocation(randomLoc, true);
		player.teleport(randomLoc);
		player.spigot().respawn();
		PlayerUtils.sendMessage(player, messages.getYouRespawned());

		HGPlayer hgPlayer = HGPlayer.from(player);
		hgPlayer.setSpectating(false);

		if (!currentPhase.isJoiningEnabled()) {
			hgPlayer.onBattleReady();
		}

		forHandleAblePlayers(this::updatePlayerCountScoreboard);
	}

	/**
	 * Startet den Lobbytimer. Ist dieser abgelaufen, startet das Spiel.
	 */
	public void startLobbyTimer() {
		if (timerStarted) {
			return;
		}
		gameStartMillis = System.currentTimeMillis();
		startLobbyTimerAt(LOBBY_SECONDS);
		timerStarted = true;
	}

	/**
	 * Startet den Lobbytimer mit gegebener Restzeit. Falls ein Lobbytimer bereits läuft,
	 * wird dieser angehalten und ersetzt.
	 *
	 * @param seconds Ticks
	 */
	public void startLobbyTimerAt(int seconds) {
		startScoreboardTimer(PlayerUtils.colorize(messages.getScoreboard_timerWaitingName()),
				formatTimeFunction(messages.getScoreboard_timeFormatWaiting()),
				this::sendWaitingMessages,
				this::nextPhase,
				seconds);
	}

	private void startScoreboardTimer(String timerName, Function<Integer, String> timeFormat, Runnable onUpdate, Runnable onTimerEnd, int seconds) {
		latestScoreboardTimerName = timerName;
		forHandleAblePlayers(player -> registerScoreboardTimerImmediate(player, timerName));

		if (gameTimer != null) {
			gameTimer.stop();
		}

		if (currentPhase != GamePhase.DEBUG) {
			gameTimer = new GameTimer.CountdownTimer();
		} else {
			gameTimer = new GameTimer.StopwatchTimer();
		}

		gameTimer.setTime(seconds);

		gameTimer.setTickHandler(() -> {
			updateScoreboardTimers(timeFormat);
			if (onUpdate != null) {
				onUpdate.run();
			}
		});

		if (onTimerEnd != null) {
			gameTimer.addCallback(0, onTimerEnd);
		}

		updateScoreboardTimers(timeFormat);

		gameTimer.start();
	}

	private void updateScoreboardTimers(Function<Integer, String> timeFormat) {
		int seconds = gameTimer != null ? gameTimer.getTime() : 0;

		forHandleAblePlayers(player -> {
			ScoreboardLine timerLine = playerTimerLines.get(player.getUniqueId()).getBelow();
			timerLine.update(timeFormat.apply(seconds));

			//line below must exist and must be free, for aesthetic purpose
			timerLine.makeSpaceBelow();
		});
	}

	public void registerScoreboardTimerImmediate(Player player, String timerName) {
		PlayerScoreboard scoreboard = ScoreboardHandler.getPlayerScoreboard(player);

		ScoreboardLine timerLine = playerTimerLines.compute(player.getUniqueId(), (uuid, scoreboardLine) -> {
			if (scoreboardLine == null || scoreboardLine.isDisposed()) {
				scoreboardLine = scoreboard.getLine(INITIAL_SCOREBOARD_TIMER_TITLE_LINE);
			}
			return scoreboardLine;
		});
		timerLine.update(timerName);
		timerLine.makeSoftSpaceAbove();
		timerLine.makeSoftSpaceBelow();

		updatePlayerCountScoreboard(player);
	}

	public void updateKillsLine(Player player) {
		PlayerScoreboard scoreboard = ScoreboardHandler.getPlayerScoreboard(player);

		ScoreboardLine killsLine = killCounterLines.compute(player.getUniqueId(), (uuid, scoreboardLine) -> {
			if (scoreboardLine == null || scoreboardLine.isDisposed()) {
				//Über der Spieleranzeige, aber +2 da Platz für Kills-Zeile und Variablenzeile
				scoreboardLine = scoreboard.getLine(INITIAL_SCOREBOARD_PLAYER_COUNT_LINE + 2);
			}
			return scoreboardLine;
		});

		killsLine.update(PlayerUtils.colorize(messages.getScoreboard_killCountName()));
		killsLine.makeSpaceAbove();
		killsLine = killsLine.getBelow();

		HGPlayer hgPlayer = HGPlayer.from(player);

		killsLine.update(String.format(messages.getScoreboard_killCountFormat(), hgPlayer.getKills()));
		killsLine.makeSpaceBelow();
	}

	public void updatePlayerCountScoreboard(Player player) {
		PlayerScoreboard scoreboard = ScoreboardHandler.getPlayerScoreboard(player);

		ScoreboardLine playerCountLine = playerPlayerCountLines.compute(player.getUniqueId(), (uuid, scoreboardLine) -> {
			if (scoreboardLine == null || scoreboardLine.isDisposed()) {
				scoreboardLine = scoreboard.getLine(INITIAL_SCOREBOARD_PLAYER_COUNT_LINE);
			}
			return scoreboardLine;
		});

		playerCountLine.update(PlayerUtils.colorize(messages.getScoreboard_playerCountName()));
		playerCountLine.makeSpaceAbove();
		playerCountLine = playerCountLine.getBelow();
		playerCountLine.update(PlayerUtils.colorize(
				String.format(messages.getScoreboard_playerCountFormat(),
						countPlayersAlive(),
						countHandleablePlayers())
		));
		playerCountLine.makeSpaceBelow();
	}

	public void setCurrentPhase(GamePhase gamePhase) {
		this.currentPhase = gamePhase;
		gamePhaseUpdateHandlers.forEach(gamePhaseUpdateHandler ->
				gamePhaseUpdateHandler.onGamePhaseUpdate(this));
	}

	public void nextPhase() {
		switch (currentPhase) {
			default:
			case WORLD:
				return;
			case LOBBY:
				startPreStartPhase();
				break;
			case PRE_START:
				startImmortalityPhase();
				break;
			case IMMORTALITY:
				startStartPhase();
				break;
			case START:
				startPlayingPhase();
				break;
			case PLAYING:
				startShowdownPhase();
				break;
			case SHOWDOWN:
				break;
			case END:
				return;
		}
		GamePhase nextPhase = currentPhase.nextPhase();
		setCurrentPhase(nextPhase);
	}

	private void sendWaitingMessages() {
		String msg = messages.getNotifications_waiting().get(gameTimer.getTime());
		if (msg != null) {
			broadcast(msg);
		}
	}

	private void sendHaltedMessages() {
		String msg = messages.getNotifications_halted().get(gameTimer.getTime());
		if (msg != null) {
			broadcast(msg);
		}
	}

	private void startPreStartPhase() {
		spreadPlayers(borderManager.getCenterX(), borderManager.getCenterZ(), SPREAD_DISTANCE, SPREAD_MAX_RANGE);
		haltAllPlayers();
		announceWorldBorder();
		forHandleAblePlayers(this::removeBorderLine);
		startScoreboardTimer(messages.getScoreboard_timerStartingName(),
				formatTimeFunction(messages.getScoreboard_timeFormatStarting()),
				this::sendHaltedMessages,
				this::nextPhase,
				10);
	}

	private void startImmortalityPhase() {
		releaseAllPlayers();
		startScoreboardTimer(messages.getScoreboard_timerImmortalityName(),
				formatTimeFunction(messages.getScoreboard_timeFormatImmortality()),
				this::sendImmortalityMessages,
				this::nextPhase,
				120);
		forPlayingPlayers(this::resetPlayerState); //final inv clear + ready
		forPlayingPlayers(this::addStartingItems);
		//forPlayingPlayers(player -> player.setGameMode(GameMode.SURVIVAL));
	}

	private void sendImmortalityMessages() {
		String msg = messages.getNotifications_immortality().get(gameTimer.getTime());
		if (msg != null) {
			broadcast(msg);
		}
	}

	private void startStartPhase() {
		checkStartPhaseOver();
		startScoreboardTimer(messages.getScoreboard_timerStartName(),
				formatTimeFunction(messages.getScoreboard_timeFormatStart()),
				this::checkStartPhaseOver,
				this::nextPhase,
				58 * 60);
	}

	private void startPlayingPhase() {
		handlePlayingPhaseLoop();
		startScoreboardTimer(messages.getScoreboard_timerPlayingName(),
				formatTimeFunction(messages.getScoreboard_timeFormatPlaying()),
				this::handlePlayingPhaseLoop,
				this::nextPhase,
				gameTimer.getTime());
	}

	private void checkStartPhaseOver() {
		if (currentPhase != GamePhase.START) {
			return;
		}
		if (checkShowdownPhase() || gameTimer.getTime() <= 55 * 60) {
			nextPhase();
		}
	}

	private void handlePlayingPhaseLoop() {
		if (currentPhase != GamePhase.PLAYING) {
			return;
		}
		if (checkShowdownPhase()) {
			gameTimer.setTimeNoUpdate(300); //This function is the update function called by this timer.
			updateScoreboardTimers(formatTimeFunction(messages.getScoreboard_timeFormatPlaying()));
		}
		String msg = messages.getNotifications_showdownStart().get(gameTimer.getTime());
		if (msg != null) {
			broadcast(msg);
		}
	}

	private boolean checkShowdownPhase() {
		int alive = countPlayersAlive();
		return alive <= 4 && gameTimer.getTime() > 300;
	}

	private void startShowdownPhase() {
		//Set a random location to be the center of the showdown.
		//The showdown area should be within the bounds of the original
		//playing arena, so we subtract the radius of the showdown area
		//from the current border radius.
		double radius = borderManager.getDiameter() / 2 - WORLDBORDER_SHOWDOWN_SIZE / 2;
		Random random = ThreadLocalRandom.current();
		borderManager.setCenter(random.nextDouble() * radius, random.nextDouble() * radius);

		spreadPlayers(borderManager.getCenterX(), borderManager.getCenterZ(),
				SPREAD_SHOWDOWN_DISTANCE, SPREAD_SHOWDOWN_RANGE);

		borderManager.setDiameter(WORLDBORDER_SHOWDOWN_SIZE);

		startScoreboardTimer(messages.getScoreboard_showdownShrinkIn(),
				formatTimeFunction(messages.getScoreboard_showdownShrinkTimeFormat()),
				null,
				this::shrinkShowdownBorder,
				300);
	}

	private void shrinkShowdownBorder() {
		if (currentPhase != GamePhase.SHOWDOWN) {
			return;
		}
		WorldBorder border = gameWorld.getWorldBorder();
		long seconds = (long) (((WORLDBORDER_SHOWDOWN_SIZE - WORLDBORDER_SHRINKED_MINSIZE) / WORLDBORDER_SHRINK_BLOCKS_PER_MINUTE) * 60L);
		border.setSize(WORLDBORDER_SHRINKED_MINSIZE, seconds);
		startScoreboardTimer(messages.getScoreboard_showdownEnd(),
				formatTimeFunction(messages.getScoreboard_shodownEndTimeFormat()),
				this::endGameSoftly,
				this::handleGameOver,
				(int) (seconds + 10));
	}

	private void endGameSoftly() {
		if (!currentPhase.isEntityDamageEnabled()) {
			return;
		}
		if (gameTimer.getTime() <= 10) {
			forPlayingPlayers(player -> player.damage(8));
		}
	}

	private boolean checkAndHandleGameOver() {
		if (checkGameOver()) {
			handleGameOver();
			return true;
		}
		return false;
	}

	private boolean checkGameOver() {
		if (currentPhase == GamePhase.WORLD || currentPhase.isJoiningEnabled()) {
			return false;
		}
		int alive = countPlayersAlive();
		return alive <= 1;
	}

	private void handleGameOver() {
		if (currentPhase == GamePhase.END) {
			return;
		}
		int alive = countPlayersAlive();
		if (alive == 1) {
			handleablePlayerSet.stream()
					.filter(hgp -> !hgp.isSpectating())
					.map(HGPlayer::getPlayer)
					.filter(Objects::nonNull)
					.limit(1)
					.findAny()
					.ifPresent(player -> {
						String msg = messages.getPlayerWon();
						msg = String.format(msg, player.getName());
						msg = PlayerUtils.colorize(msg);
						broadcast(msg);
					});
		} else {
			broadcast(messages.getNoWinner());
		}
		currentPhase = GamePhase.END;
		startScoreboardTimer(messages.getScoreboard_timerShutdown(),
				formatTimeFunction(messages.getScoreboard_timeFormatShutdown()),
				null,
				this::shutdownGame,
				30);
	}

	public void shutdownGame() {
		kickAllPlayers();
		Bukkit.getScheduler().runTaskLater(HGBukkitMain.getInstance(), Bukkit::shutdown, 20L);
	}

	public void kickAllPlayers() {
		String msg = messages.getShutdownKick();
		Bukkit.getOnlinePlayers().forEach(p -> p.kickPlayer(PlayerUtils.colorize(msg)));
	}

	private Function<Integer, String> formatTimeFunction(String timeFormat) {
		return integer -> PlayerUtils.colorize(String.format(timeFormat, HGBukkitMain.formatSeconds(integer)));
	}

	public void spreadPlayers(int x, int z, double minDistanceBetween, int range) {
		List<Player> players = new ArrayList<>(Bukkit.getOnlinePlayers());
		World world = null;

		if (!players.isEmpty()) {
			world = players.get(0).getWorld();
		}

		Validate.isTrue(world != null, "Could not get a world to spread players in.");
		PlayerUtils.SpreadPlayer.spreadPlayers(x, z, minDistanceBetween, range, world, players, true);
	}

	public void haltAllPlayers() {
		allPlayersHalted = true;
		Set<Player> playingPlayers = handleablePlayerSet.stream()
				.filter(hgp -> !hgp.isSpectating())
				.map(HGPlayer::getPlayer)
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
		playingPlayers.forEach(this::haltPlayer);
	}

	public void announceWorldBorder() {
		int size = (int) borderManager.getDiameter();
		broadcast(messages.getWorldBorderAnnouncement().replace("<size>", String.valueOf(size)));
	}

	public void haltPlayer(Player player) {
		HGPlayer hgPlayer = HGPlayer.from(player);
		hgPlayer.halt();
	}

	public void releasePlayer(Player player) {
		HGPlayer hgPlayer = HGPlayer.from(player);
		hgPlayer.release();
	}

	public void releaseAllPlayers() {
		allPlayersHalted = false;
		handleablePlayerSet.stream()
				.filter(HGPlayer::isHalted)
				.forEach(HGPlayer::release);
	}

	public Stream<Player> getHandlablePlayersStream() {
		return handleablePlayerSet.stream()
				.map(HGPlayer::getPlayer)
				.filter(Objects::nonNull);
	}

	public void forHandleAblePlayers(Consumer<Player> f) {
		getHandlablePlayersStream().forEach(f);
	}

	public int countHandleablePlayers() {
		return (int) getHandlablePlayersStream().count();
	}

	public Stream<Player> getPlayingPlayersStream() {
		return handleablePlayerSet.stream()
				.filter(hgp -> !hgp.isSpectating())
				.map(HGPlayer::getPlayer)
				.filter(Objects::nonNull)
				.filter(Player::isOnline)
				.filter(player -> !player.isDead());
	}

	public void forPlayingPlayers(Consumer<Player> f) {
		getPlayingPlayersStream().forEach(f);
	}

	public void forSpectatingPlayers(Consumer<Player> f) {
		handleablePlayerSet.stream()
				.filter(HGPlayer::isSpectating)
				.map(HGPlayer::getPlayer)
				.filter(Objects::nonNull)
				.forEach(f);
	}

	public int countPlayersAlive() {
		return (int) getPlayingPlayersStream().count();
	}

}
