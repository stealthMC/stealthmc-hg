package net.stealthmc.hgcore.game.gamephase;

import net.stealthmc.hgcore.game.GameHandler;

public interface GamePhaseUpdateHandler {

	void onGamePhaseUpdate(GameHandler gameHandler);

	default void attachTo(GameHandler gameHandler) {
		gameHandler.getGamePhaseUpdateHandlers().add(this);
		this.onGamePhaseUpdate(gameHandler);
	}

}
