package net.stealthmc.hgcore.game.entity;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import net.stealthmc.hgcore.api.AdapterPriority;
import net.stealthmc.hgcore.api.AttachedListener;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.spigotmc.event.entity.EntityDismountEvent;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter(value = AccessLevel.PACKAGE)
@EqualsAndHashCode(of = {"entityId"})
public class HGEntity {

	protected static Map<UUID, HGEntity> cache = Maps.newConcurrentMap();
	final UUID entityId;
	protected List<AttachedListener> attachedListeners = Lists.newCopyOnWriteArrayList();

	@Getter(value = AccessLevel.PRIVATE)
	@Setter(value = AccessLevel.PRIVATE)
	private WeakReference<Entity> entityWeakReference = null;

	HGEntity(UUID entityId) {
		this.entityId = entityId;
		this.getAttachedListeners().add(
				new AttachedListener(entityId) {
					@Override
					@AdapterPriority(priority = Integer.MAX_VALUE) //it will be the last thing
					public void onEntityDeath(EntityDeathEvent event) {
						if (!(event.getEntity() instanceof Player)) { //Do not dispose Players
							HGEntity.dispose(entityId);
						}
					}
				}
		);
	}

	public static HGEntity from(UUID uuid) {
		return cache.compute(uuid, (uuid1, hgEntity) -> {
			if (hgEntity == null) {
				hgEntity = new HGEntity(uuid);
			}
			return hgEntity;
		});
	}

	public static HGEntity from(Entity entity) {
		HGEntity hgEntity = from(entity.getUniqueId());
		if (hgEntity.getEntityWeakReference() == null
				|| hgEntity.getEntityWeakReference().get() == null) {
			hgEntity.setEntityWeakReference(new WeakReference<>(entity));
		}
		return hgEntity;
	}

	public static Collection<HGEntity> allKnownEntities() {
		return cache.values();
	}

	public static void dispose(UUID hgEntityId) {
		cache.remove(hgEntityId);
	}

	public void onDamage(EntityDamageEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onDamage", participants,
				new Class[]{EntityDamageEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onDamage(event));
	}

	public void onEntityDamage(EntityDamageByEntityEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onEntityDamage", participants,
				new Class[]{EntityDamageByEntityEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onEntityDamage(event));
	}

	public void onProjectileLaunch(ProjectileLaunchEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onProjectileLaunch", participants,
				new Class[]{ProjectileLaunchEvent.class}, event);
	}

	public void onProjectileHit(ProjectileHitEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onProjectileHit", participants,
				new Class[]{ProjectileHitEvent.class}, event);
	}

	public void onEntityDeath(EntityDeathEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onEntityDeath", participants,
				new Class[]{EntityDeathEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onEntityDeath(event));
	}

	public void onDismount(EntityDismountEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onDismount", participants,
				new Class[]{EntityDismountEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onDismount(event));
	}

	public void onPlayerExpChange(PlayerExpChangeEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onPlayerExpChange", participants,
				new Class[]{PlayerExpChangeEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onDismount(event));
	}

	public void onExpBottle(ExpBottleEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onExpBottle", participants,
				new Class[]{ExpBottleEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onDismount(event));
	}

	public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event, UUID[] participants) {
		AttachedListener.invokeInPriorityOrder(this.getAttachedListeners(), "onEntityTargetLivingEntity", participants,
				new Class[]{EntityTargetLivingEntityEvent.class}, event);
		//this.getAttachedListeners().forEach(a -> a.onDismount(event));
	}
}
