package net.stealthmc.hgcore.game;

import lombok.experimental.UtilityClass;
import net.stealthmc.hgcommon.util.FileUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@UtilityClass
public class WorldHandler {

	private static final int CHUNK_GEN_WIDHT = 40;

	public World createGameWorld() {
		WorldCreator creator = WorldCreator.name(HGBukkitMain.HG_WORLD_NAME);
		//creator.generator("TerrainControl");
		World world = Bukkit.createWorld(creator);
		world.setSpawnLocation(-10000, 128, -10000); //Spawnprotection loswerden
		world.setKeepSpawnInMemory(false);
		world.setAutoSave(false);


		int xzEnd = CHUNK_GEN_WIDHT / 2;
		int xzStart = -xzEnd;

		ExecutorService service = Executors.newFixedThreadPool(CHUNK_GEN_WIDHT * CHUNK_GEN_WIDHT);

		for (int x = xzStart; x <= xzEnd; x++) {
			for (int z = xzStart; z <= xzEnd; z++) {
				final int xx = x;
				final int zz = z;
				service.execute(() -> Bukkit.getScheduler().callSyncMethod(HGBukkitMain.getInstance(), () -> {
					world.loadChunk(xx, zz, true);
					//world.unloadChunk(xx, zz);
					return null;
				}));
			}
		}

		service.shutdown();

		try {
			HGBukkitMain.getInstance().getLogger().log(Level.INFO, "Waiting for world generation.");
			service.awaitTermination(15 * CHUNK_GEN_WIDHT, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			HGBukkitMain.getInstance().getLogger().log(Level.SEVERE, "Error while generating the world!", e);
			return null;
		}

		HGBukkitMain.getInstance().getLogger().log(Level.INFO, "World generation completed.");
		return world;
	}

	public boolean unloadAndDelete(World world) {
		Bukkit.unloadWorld(world, false);
		File folder = world.getWorldFolder();
		try {
			return FileUtils.deleteRecursively(folder, new File(folder, "settings"));
		} catch (Exception e) {
			HGBukkitMain.getInstance().getLogger().log(Level.SEVERE, "Error while trying to delete the World " + world.getName(), e);
		}
		return false;
	}

}
