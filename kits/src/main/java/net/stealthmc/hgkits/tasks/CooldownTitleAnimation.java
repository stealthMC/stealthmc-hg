package net.stealthmc.hgkits.tasks;

import com.destroystokyo.paper.Title;
import lombok.experimental.UtilityClass;
import net.stealthmc.hgcore.game.GameHandler;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

@UtilityClass
public class CooldownTitleAnimation {

	public void play(Player player, int cooldown) {
		Title startTitle = Title.builder()
				.title(ChatColor.RESET + "")
				.subtitle(ChatColor.RED + "" + cooldown)
				.fadeIn(0)
				.fadeOut(0)
				.stay(21)
				.build();
		Title endTitle = Title.builder()
				.title(ChatColor.RESET + "")
				.subtitle(ChatColor.RED + "" + (cooldown - 1))
				.fadeIn(0)
				.fadeOut(16)
				.stay(1)
				.build();
		player.sendTitle(startTitle);
		GameHandler.getInstance().getSecondsSyncedOneTimeRunnables().add(() -> player.sendTitle(endTitle));
	}

}
