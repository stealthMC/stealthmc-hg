package net.stealthmc.hgkits.tasks;

import net.md_5.bungee.api.ChatColor;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class LookingAtKitTask extends BukkitRunnable {

	@Override
	public void run() {
		if (GameHandler.getInstance().getCurrentPhase().isJoiningEnabled()) {
			return;
		}
		GameHandler.getInstance().forHandleAblePlayers(player -> {
			Bukkit.getScheduler().runTask(KitsMain.getInstance(), () -> {
				Player target = getLookingAt(player);
				if (target == null) {
					PlayerUtils.sendAction(player, "");
					return;
				}
				HGPlayer hgTarget = HGPlayer.from(target);
				Kit kit = hgTarget.getAttachedListeners().stream()
						.filter(a -> a instanceof Kit)
						.map(a -> (Kit) a)
						.findFirst()
						.orElse(null);
				String kitName = kit == null ? "None" : KitsMain.getInstance().getPrimaryKitNameNormalized(kit);
				PlayerUtils.sendAction(player, ChatColor.GREEN + target.getName() + ChatColor.GRAY + " » " + ChatColor.GOLD + kitName);
			});
		});
	}

	public @Nullable
	Player getLookingAt(@Nonnull Player player) {
		Location location = player.getLocation();
		Vector dir = location.getDirection().normalize();
		World world = player.getWorld();
		for (int i = 0; i < 10; i++) {
			Player lookingAt = world.getNearbyEntities(location.add(dir), 0.5, 1.25, 0.5)
					.stream()
					.filter(e -> e instanceof Player)
					.map(e -> (Player) e)
					.filter(p -> !p.getUniqueId().equals(player.getUniqueId()))
					.filter(GameHandler.getInstance()::isHandleablePlayer)
					.filter(GameHandler.getInstance()::isAlive)
					.findFirst()
					.orElse(null);
			if (lookingAt != null) {
				return lookingAt;
			}
		}
		return null;
	}

}
