package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class UrgalKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You spawn with 2 potions of strength",
			CC.gray + "that last for (0:30).",
			CC.gray + "Use them wisely!",
			CC.gray + "You will get a restock every 25 minutes.",
			CC.gray + "",
			CC.gray + "If your Inventory is full, no restock!"
	);

	private static final ItemStack ABILITY_ITEM;
	private static final int cooldownMinutes = 25 * 60;

	static {
		ABILITY_ITEM = new ItemStack(Material.SPLASH_POTION, 2);
		PotionMeta potionMeta = (PotionMeta) ABILITY_ITEM.getItemMeta();
		potionMeta.setDisplayName(CC.red + "POWER UP");
		potionMeta.setColor(Color.fromRGB(60, 0, 0));
		PotionEffect strength = new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 30 * 20, 0);
		PotionEffect blindness = new PotionEffect(PotionEffectType.BLINDNESS, 2 * 20, 0);
		potionMeta.addCustomEffect(strength, true);
		potionMeta.addCustomEffect(blindness, true);
		ABILITY_ITEM.setItemMeta(potionMeta);
	}

	private int cooldownSeconds = cooldownMinutes;

	public UrgalKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.POTION, 16425, CC.gold + "Urgal", description);
		getStartingItems().add(ABILITY_ITEM);

		GameHandler.getInstance().getSecondsHandler().add(() -> {
			if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
				return;
			}
			cooldownSeconds = Math.max(0, cooldownSeconds - 1);
			if (cooldownSeconds != 0) {
				return;
			}
			if (playerToAdapt == null) {
				return;
			}
			Player player = Bukkit.getPlayer(playerToAdapt);
			if (player == null) {
				return;
			}
			if (!KitsMain.getInstance().hasKit(player, this.getClass())) {
				return;
			}
			if (player.getInventory().firstEmpty() == -1) {
				PlayerUtils.sendMessage(player, CC.red + "Your Inventory was full! Your restock will be dropped where you stand.\n");
				player.getWorld().dropItemNaturally(player.getLocation().add(0, 1, 0), ABILITY_ITEM);
			} else {
				player.getInventory().addItem(ABILITY_ITEM);
			}
			cooldownSeconds = cooldownMinutes;
		});
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
