package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.api.AdapterPriority;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class PurgerKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "It allows you to kill any kind",
			CC.gray + "of mob with one hit!",
			CC.gray + "Doesn't work on players...",
			CC.gray + "Monsters deal to you 2x the damage!"
	);

	public PurgerKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.BONE, 0, CC.gold + "Purger", description);
	}


	@Override
	@AdapterPriority(priority = 2000)
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}
		if (event.getEntity() instanceof Player) {
			return;
		}
		if (event.isCancelled()) {
			return;
		}
		if (!event.getDamager().getUniqueId().equals(super.getEntityId())) {
			return;
		}
		LivingEntity entity = (LivingEntity) event.getDamager();
		event.setDamage(entity.getHealth() * 10);
		entity.getWorld().playEffect(entity.getLocation().add(0, 2, 0), Effect.SMOKE, 0);
		entity.getWorld().spawnParticle(Particle.HEART, entity.getLocation().add(0, 2, 0), 2, 5);
		entity.getWorld().createExplosion(entity.getLocation(), 1, false, false);
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (!((event).getEntity() instanceof LivingEntity)) {
			return;
		}
		if (!((event).getEntity() instanceof Player)) {
			return;
		}
		if (!event.getEntity().getUniqueId().equals(super.getEntityId())) {
			return;
		}
		if (!(event.getEntity() instanceof Monster)) {
			return;
		}
		event.setDamage(event.getFinalDamage() * 2);
	}
}
