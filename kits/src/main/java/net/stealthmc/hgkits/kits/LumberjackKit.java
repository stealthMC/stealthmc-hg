package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class LumberjackKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "If you break Logs with any kind",
			CC.gray + "of Axe, you will cause a cascade",
			CC.gray + "reaction, work well on Trees!"
	);

	private static final List<Material> AXES = ImmutableList.of(
			Material.WOODEN_AXE,
			Material.STONE_AXE,
			Material.IRON_AXE,
			Material.GOLDEN_AXE,
			Material.DIAMOND_AXE
	);

	private static final List<Material> LOG_MATERIALS = ImmutableList.of(
			Material.BIRCH_LOG,
			Material.STRIPPED_BIRCH_LOG,
			Material.ACACIA_LOG,
			Material.STRIPPED_ACACIA_LOG,
			Material.DARK_OAK_LOG,
			Material.STRIPPED_DARK_OAK_LOG,
			Material.JUNGLE_LOG,
			Material.STRIPPED_JUNGLE_LOG,
			Material.OAK_LOG,
			Material.STRIPPED_OAK_LOG,
			Material.SPRUCE_LOG,
			Material.STRIPPED_SPRUCE_LOG
	);

	private static final List<BlockFace> DIRECTIONS = ImmutableList.of(
			BlockFace.UP,
			BlockFace.DOWN,
			BlockFace.NORTH,
			BlockFace.EAST,
			BlockFace.SOUTH,
			BlockFace.WEST
	);
	private static ItemStack ABILITY_ITEM;

	static {
		ABILITY_ITEM = Kit.createItemStack(Material.GOLDEN_AXE, CC.red + "Tree Chopper");
		ItemMeta meta = ABILITY_ITEM.getItemMeta();
		meta.addEnchant(Enchantment.DIG_SPEED, 2, true);
		ABILITY_ITEM.setItemMeta(meta);
	}

	public LumberjackKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.GOLDEN_AXE, 0, CC.gold + "Lumberjack", description);
		getStartingItems().add(ABILITY_ITEM);
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		ItemStack inHand = event.getPlayer().getInventory().getItemInMainHand();
		if (!AXES.contains(inHand.getType())) {
			return;
		}
		if (LOG_MATERIALS.contains(event.getBlock().getType())) {
			deforest(event.getBlock());
		}
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
	}

	private void deforest(Block origin) {
		if (!LOG_MATERIALS.contains(origin.getType())) {
			return;
		}

		ItemStack drop = new ItemStack(origin.getType(), 1);
		Location center = LocationUtils.toCenterLocation(origin);

		origin.getWorld().dropItemNaturally(center, drop);
		origin.getWorld().playSound(center, Sound.BLOCK_WOOD_BREAK, 1f, 1f);

		origin.setType(Material.AIR);

		for (BlockFace face : DIRECTIONS) {
			Block adjacent = origin.getRelative(face);
			if (!LOG_MATERIALS.contains(adjacent.getType())) {
				continue;
			}
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () -> {
				deforest(adjacent);
			}, 1L);
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		if (event.getItem().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

}
