package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/*

Coded by Fundryi

 */

public class CultivatorKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Allows you to instantly grow",
			CC.gray + "Trees from sapling or Wheat from seeds!",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	public CultivatorKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.OAK_SAPLING, 0, CC.gold + "Cultivator", description);
		getStartingItems().add(new ItemStack(Material.OAK_SAPLING, 5));
	}

	@Override
	public void onBlockPlace(BlockPlaceEvent event) {
		onCultivatorCrops(event);
		if (!event.getBlock().getType().name().endsWith("SAPLING")) {
			return;
		}
		onOakSapling(event);
		onBirchSapling(event);
		onJungleSapling(event);
		onSpruceSapling(event);
		onDarkOakSapling(event);
		onAcaciaSapling(event);
	}

	private void onCultivatorCrops(BlockPlaceEvent event) {
		Block block = event.getBlock();
		BlockData data = block.getBlockData();
		if (data instanceof Ageable) {
			Ageable ag = (Ageable) data;
			ag.setAge(ag.getMaximumAge());
			block.setBlockData(ag);
		}
	}

	private boolean treeHasNoSpace(int treeHeights, Player player, Block block) {
		boolean treeHasSpace = true;

		for (int y = 0; y < treeHeights; y++) {
			BlockBreakEvent e = new BlockBreakEvent(block, player);
			Bukkit.getPluginManager().callEvent(e);
			if (e.isCancelled())
				treeHasSpace = false;
		}

		if (!treeHasSpace) {
			PlayerUtils.sendMessage(player, CC.red + "Sorry, but it seems like you can't grow this tree here.");
		}

		return !treeHasSpace;
	}

	private void onOakSapling(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		ItemStack mainHand = player.getInventory().getItemInMainHand();
		if (block.getType() == Material.OAK_SAPLING) {
			if (treeHasNoSpace(5, player, block)) {
				event.setCancelled(true);
				return;
			}

			if (mainHand.getAmount() >= 1) {
				System.out.println(player.getWorld().generateTree(block.getLocation(), TreeType.TREE));
				mainHand.setAmount(mainHand.getAmount() - 1);
			} else {
				mainHand.setType(Material.AIR);
			}
		}
	}

	private void onBirchSapling(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		ItemStack mainHand = player.getInventory().getItemInMainHand();
		if (block.getType() == Material.BIRCH_SAPLING) {
			if (treeHasNoSpace(8, player, block)) {
				event.setCancelled(true);
				return;
			}

			if (mainHand.getAmount() >= 1) {
				player.getWorld().generateTree(block.getLocation(), TreeType.TALL_BIRCH);
				mainHand.setAmount(mainHand.getAmount() - 1);
			} else {
				mainHand.setType(Material.AIR);
			}
		}
	}

	private void onJungleSapling(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		ItemStack mainHand = player.getInventory().getItemInMainHand();
		if (block.getType() == Material.JUNGLE_SAPLING) {
			if (treeHasNoSpace(8, player, block)) {
				event.setCancelled(true);
				return;
			}

			if (mainHand.getAmount() >= 1) {
				player.getWorld().generateTree(block.getLocation(), TreeType.SMALL_JUNGLE);
				mainHand.setAmount(mainHand.getAmount() - 1);
			} else {
				mainHand.setType(Material.AIR);
			}
		}
	}

	private void onSpruceSapling(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		ItemStack mainHand = player.getInventory().getItemInMainHand();
		if (block.getType() == Material.SPRUCE_SAPLING) {
			if (treeHasNoSpace(10, player, block)) {
				event.setCancelled(true);
				return;
			}

			if (mainHand.getAmount() >= 1) {
				player.getWorld().generateTree(block.getLocation(), TreeType.TALL_REDWOOD);
				mainHand.setAmount(mainHand.getAmount() - 1);
			} else {
				mainHand.setType(Material.AIR);
			}
		}
	}

	private void onDarkOakSapling(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		ItemStack mainHand = player.getInventory().getItemInMainHand();
		if (block.getType() == Material.DARK_OAK_SAPLING) {
			if (treeHasNoSpace(8, player, block)) {
				event.setCancelled(true);
				return;
			}

			if (mainHand.getAmount() >= 4) {
				player.getWorld().generateTree(block.getLocation(), TreeType.DARK_OAK);
				mainHand.setAmount(mainHand.getAmount() - 4);
			} else {
				PlayerUtils.sendMessage(player, "Sorry, but you need at least 4 Dark Oak Saplings to grow this kind of tree.");
				mainHand.setType(Material.AIR);
			}
		}
	}

	private void onAcaciaSapling(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		ItemStack mainHand = player.getInventory().getItemInMainHand();
		if (block.getType() == Material.ACACIA_SAPLING) {
			if (treeHasNoSpace(5, player, block)) {
				event.setCancelled(true);
				return;
			}

			if (mainHand.getAmount() >= 1) {
				player.getWorld().generateTree(block.getLocation(), TreeType.ACACIA);
				mainHand.setAmount(mainHand.getAmount() - 1);
			}
		}
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		Material typeBlock = block.getType();
		if (!(block.getType().name().endsWith("LEAVES"))) {
			return;
		}
		block.setType(Material.AIR);
		block.getDrops().clear();
		if (ThreadLocalRandom.current().nextInt(100) <= 40) {
			if (typeBlock == Material.ACACIA_LEAVES) {
				block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
						new ItemStack(Material.ACACIA_SAPLING, 1));

			} else if (typeBlock == Material.BIRCH_LEAVES) {
				block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
						new ItemStack(Material.BIRCH_SAPLING, 1));

			} else if (typeBlock == Material.DARK_OAK_LEAVES) {
				block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
						new ItemStack(Material.DARK_OAK_SAPLING, 1));

			} else if (typeBlock == Material.JUNGLE_LEAVES) {
				block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
						new ItemStack(Material.JUNGLE_SAPLING, 1));

			} else if (typeBlock == Material.OAK_LEAVES) {
				block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
						new ItemStack(Material.OAK_SAPLING, 1));

			} else if (typeBlock == Material.SPRUCE_LEAVES) {
				block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
						new ItemStack(Material.SPRUCE_SAPLING, 1));

			}
		}
		if (ThreadLocalRandom.current().nextDouble(100) <= 20.0) {
			block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
					new ItemStack(Material.APPLE));
		}
		if (ThreadLocalRandom.current().nextDouble(100) <= 1.5) {
			block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
					new ItemStack(Material.EXPERIENCE_BOTTLE));
		}
		if (ThreadLocalRandom.current().nextDouble(100) <= 0.9) {
			block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
					new ItemStack(Material.GOLDEN_APPLE));
		}
		if (ThreadLocalRandom.current().nextDouble(100) <= 0.2) {
			block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
					new ItemStack(Material.EMERALD));
		}
		if (ThreadLocalRandom.current().nextDouble(100) <= 0.1) {
			block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block),
					new ItemStack(Material.DIAMOND));
		}
	}
}
