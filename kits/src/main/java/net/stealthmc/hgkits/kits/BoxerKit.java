package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.api.AdapterPriority;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class BoxerKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Your fist deals 2 hearts of damage!",
			CC.gray + "You also receive 0.5 hearts less damage.",
			CC.gray + "Critical hits do vanilla damage!"
	);

	private static final int boxerDamage = 4;
	private static final boolean reduceDamage = true;

	public BoxerKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.STONE_SWORD, 0, CC.gold + "Boxer", description);
	}


	@Override
	@AdapterPriority(priority = 2000)
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}
		if (event.isCancelled()) {
			return;
		}
		if (!event.getDamager().getUniqueId().equals(super.getEntityId())) {
			return;
		}
		Player player = (Player) event.getDamager();
		if (player.getInventory().getItemInMainHand().getType() == Material.AIR) {
			event.setDamage(boxerDamage);
		}
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (!((event).getEntity() instanceof Player)) {
			return;
		}
		if (!event.getEntity().getUniqueId().equals(super.getEntityId())) {
			return;
		}
		if (event.getFinalDamage() > 1 && reduceDamage) { //REDUCES THE DAMAGE ONLY IF ITS OVER 0.5 HEARTS
			double damage = event.getDamage();
			event.setDamage(damage - 1);
		}
	}

}
