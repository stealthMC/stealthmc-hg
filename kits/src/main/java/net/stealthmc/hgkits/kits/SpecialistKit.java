package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ExpBottleEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class SpecialistKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Enchanting on the way!",
			CC.gray + "Portable Enchantment Table",
			CC.gray + "that no one can steal it.",
			CC.gray + "You also receive 2 EXP Bottles per kill!",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final ItemStack ABILITY_ITEM = Kit.createItemStack(Material.BOOK, CC.red + "Portable Enchanter");

	public SpecialistKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.ENCHANTING_TABLE, 0, CC.gold + "Specialist", description);
		getStartingItems().add(ABILITY_ITEM);
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		ItemStack itemStack = event.getPlayer().getInventory().getItemInMainHand();
		if (!itemStack.isSimilar(ABILITY_ITEM)) {
			return;
		}
		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {

			player.openEnchanting(player.getLocation(), true);

		}
		event.setCancelled(true);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {

		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}

		Player player = (Player) event.getEntity();
		Player kitUser = (Player) event.getDamager();

		if (player.getUniqueId().equals(super.getEntityId())) { //FILTERS KITUSER OUT AS VICTIM
			return;
		}
		if (!(player.getHealth() - event.getFinalDamage() <= 0)) {
			return;
		}
		kitUser.playSound(kitUser.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
		kitUser.getInventory().addItem(new ItemStack(Material.EXPERIENCE_BOTTLE, 2));
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), kitUser::updateInventory, 1L);
	}

	@Override
	public void onPlayerExpChange(PlayerExpChangeEvent event) {
		event.setAmount((int) ((double) event.getAmount() * 2.0));
	}

	// GEHT IMMERNOCH NICHT!
	@Override
	public void onExpBottle(ExpBottleEvent event) {
		//event.setExperience(event.getExperience() * 1000);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
		}
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
	}
}
