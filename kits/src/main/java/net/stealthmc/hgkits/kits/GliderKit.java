package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGEntity;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import org.spigotmc.event.entity.EntityDismountEvent;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;

/*

Coded by Fundryi & petomka

 */

public class GliderKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You can fly! I mean kinda...",
			CC.gray + "You can glide down from high positions!",
			CC.gray + "You are even rideable,",
			CC.gray + "so don't forget your friends!",
			CC.gray + "Players riding you will cause that you",
			CC.gray + "can only hit them!",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final ItemStack ABILITY_ITEM = Kit.createItemStack(Material.FEATHER, CC.red + "Glider Feather");
	private static final int INVICS = 20 * 60 * 15;

	public GliderKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.FEATHER, 0, CC.gold + "Glider", description);
		getStartingItems().add(ABILITY_ITEM);
	}

	private static void onArmorDismount(EntityDismountEvent event) {
		Entity dismounted = event.getDismounted();
		Entity entity = event.getEntity();
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () -> {
			Stream.of(dismounted, entity)
					.filter(e -> !(e instanceof Player))
					.forEach(Entity::remove);
		}, 1L);
	}

	//This method ONLY gets called for the entity dismounting an entity and the dismounted entity.
	@Override
	public void onDismount(EntityDismountEvent event) {
		onSpaceDismount(event);
	}

	private void onSpaceDismount(EntityDismountEvent event) {
		if (event.getEntity() == null || event.getDismounted() == null) { //NULL CHECK
			return;
		}
		if (!(event.getEntity() instanceof Player) || !(event.getDismounted() instanceof Slime)) { //PLAYER & CHICKEN CHECK
			return;
		}

		Slime spaceEntity = (Slime) event.getDismounted();
		Player player = (Player) event.getEntity();

		if (!player.isInsideVehicle()) {
			return;
		}

		if (!player.getPassengers().isEmpty()) {
			return;
		}

		EntityDismountEvent dismountEvent = new EntityDismountEvent(player, player.getPassengers().get(0));
		onArmorDismount(dismountEvent);
		player.eject();

		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () -> {
			spaceEntity.remove();
			player.teleport(player.getLocation().add(0, 0.2, 0));
		}, 1L);

	}

	//EXTERNAL USER INTERACT WITH GLIDER KIT USER
	@Override
	public void onInteractEntity(PlayerInteractEntityEvent event) {
		if (GameHandler.getInstance().getCurrentPhase().isJoiningEnabled()) {
			return;
		}
		if (!event.getRightClicked().getPassengers().isEmpty()) {
			return;
		}
		if (!(event.getRightClicked() instanceof Player)) {
			return;
		}
		Player glider = (Player) event.getRightClicked();
		if (!glider.getInventory().getItemInMainHand().isSimilar(ABILITY_ITEM)) {
			return;
		}

		Slime spaceHolder = (Slime) glider.getWorld().spawnEntity(glider.getLocation(), EntityType.SLIME);
		HGEntity spaceHolderRemover = HGEntity.from(spaceHolder);

		spaceHolderRemover.getAttachedListeners().add(new AttachedListener(spaceHolderRemover.getEntityId()) {
			@Override
			public void onDismount(EntityDismountEvent event) {
				onArmorDismount(event);
			}
		});

		spaceHolder.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, INVICS, 1));
		spaceHolder.setNoDamageTicks(INVICS);
		spaceHolder.setHealth(Objects.requireNonNull(spaceHolder.getAttribute(Attribute.GENERIC_MAX_HEALTH)).getValue());
		spaceHolder.setSize(2);
		spaceHolder.setAI(false);

		glider.addPassenger(spaceHolder);
		Player rider = event.getPlayer();
		spaceHolder.addPassenger(rider);
	}


	//GLIDER KIT USER onRightClick create Chicken event
	@Override
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Block legs = player.getLocation().getBlock();
		Block legs2 = legs.getRelative(BlockFace.DOWN);
		Block legs3 = legs2.getRelative(BlockFace.DOWN);
		Block chickenSpawn = legs.getRelative(BlockFace.DOWN);

		ItemStack inHand = player.getInventory().getItemInMainHand();
		if (!inHand.isSimilar(ABILITY_ITEM)) {
			return;
		}
		event.setCancelled(true);
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_AIR) {
			return;
		}
		if (!(legs.isEmpty() && legs2.isEmpty() && legs3.isEmpty() && chickenSpawn.isEmpty())) {
			return;
		}

		if (player.getVehicle() != null) {
			return;
		}

		Chicken chicken = player.getWorld().spawn(chickenSpawn.getLocation().add(0.5, 0.2, 0.5), Chicken.class);
		chicken.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, INVICS, 1));
		chicken.setAgeLock(true);
		chicken.setAge(0);
		chicken.setNoDamageTicks(INVICS);
		chicken.setBaby();
		chicken.setHealth(Objects.requireNonNull(chicken.getAttribute(Attribute.GENERIC_MAX_HEALTH)).getValue());

        /*Object handle = NmsEntityUtils.craftEntityGetHandle(chicken);
        if (handle != null) {
            NmsEntityUtils.setSilent(handle, true);
        }*/
		chicken.addPassenger(player);
	}

	//GLIDER KIT USER onSneak KICK PASSANGER (SpaceHolder Entity)
	@Override
	public void onSneak(PlayerToggleSneakEvent event) {

		Player player = event.getPlayer();
		ItemStack inHand = player.getInventory().getItemInMainHand();

		if (inHand == null) {
			return;
		}

		if (!inHand.isSimilar(ABILITY_ITEM)) {
			return;
		}

		if (player.getPassengers().isEmpty() || !(player.getPassengers().get(0) instanceof Slime)) {
			return;
		}

		player.getPassengers().get(0).eject();
	}

	//GLIDER KIT USER CHICKEN
	@Override
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();

		if (!(player.getVehicle() instanceof Chicken)) {
			return;
		} else {
			Vector target = player.getLocation().getDirection().multiply(0.4);
			target.setY(player.getVehicle().getVelocity().getY());
			player.getVehicle().setVelocity(target);
		}
		if (player.getVehicle().isOnGround()) {
			player.getVehicle().remove();
			player.teleport(player.getLocation().add(0, 0.2, 0));
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
