package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableMap;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.util.MapUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.entity.HGEntity;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import javax.annotation.Nullable;
import java.util.*;

/*

Coded by Fundryi

 */

public class SummonerKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Gotta catch 'em all!",
			CC.gray + "You can catch any kind of mob with your eggs!",
			CC.gray + "Chicken will drop eggs if killed."
	);
	private static final Map<EntityType, Map<Material, Material>> SWAPPED_ITEMS = MapUtils.of(HashMap::new,
			EntityType.CHICKEN, ImmutableMap.of(Material.FEATHER, Material.EGG)
	);
	private Set<UUID> attachedEntities = new HashSet<>();

	public SummonerKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.EGG, 0, CC.gold + "Summoner", description);
		getStartingItems().add(new ItemStack(Material.EGG, 5));
	}

	@Override
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if (event.getEntity().getType() != EntityType.EGG) {
			return;
		}
		ProjectileSource source = event.getEntity().getShooter();
		if (!(source instanceof Player)) {
			return;
		}
		HGEntity entity = HGEntity.from(event.getEntity());
		entity.getAttachedListeners().add(
				new AttachedListener(event.getEntity().getUniqueId()) {

					@Override
					public void onEntityDamage(EntityDamageByEntityEvent event) {
						if (!event.getDamager().getUniqueId().equals(getEntityId())) {
							return;
						}
						if (!(event.getEntity() instanceof LivingEntity)) {
							return;
						}

						LivingEntity target = (LivingEntity) event.getEntity();

						if (target instanceof Player) {
							return;
						}
						target.remove();
						//noinspection deprecation
						event.getEntity().getWorld().dropItemNaturally(target.getLocation(), new ItemStack(Material.SHEEP_SPAWN_EGG, 1, target.getType().getTypeId()));
					}

				}
		);
	}

	private void onSummorKill(EntityDeathEvent event) {
		//the stored information is now useless, the entity died.
		attachedEntities.remove(event.getEntity().getUniqueId());
		//The killer could also be someone else! It is just guaranteed that the player at least damaged
		//the now dead entity, we don't know if he is the killer until we checked.
		Player killer = event.getEntity().getKiller();
		if (killer == null) {
			return;
		}
		if (!killer.getUniqueId().equals(this.getEntityId())) {
			return;
		}
		//Replace items corresponding to the now dead entity's type.
		Map<Material, Material> replaceMap = SWAPPED_ITEMS.get(event.getEntityType());
		if (replaceMap == null) {
			return;
		}
		event.getDrops().replaceAll(itemStack -> {
			Material newMaterial = replaceMap.get(itemStack.getType());
			if (newMaterial == null) {
				return itemStack;
			}
			int amount = itemStack.getAmount();
			return new ItemStack(newMaterial, amount);
		});
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		//Could also be called if the player using this kit was damaged!
		//-> Check if the player using this kit was actually the one damaging.
		if (!event.getDamager().getUniqueId().equals(this.getEntityId())) {
			return;
		}
		//Adding the HunterAdapter more than once to the damaged entity would
		//result in wasted computation time (it's probably insignificant, but
		//nevertheless)
		if (!attachedEntities.add(event.getEntity().getUniqueId())) {
			return;
		}
		//Add the HunterAdapter so we know when the entity that was damaged by
		//the player using the hunter kit died.
		HGEntity hgEntity = HGEntity.from(event.getEntity());
		hgEntity.getAttachedListeners().add(new SummonerKit.SummonerAdapter(event.getEntity()));
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.getType() == Material.EGG);
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().getType() == Material.EGG) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	private class SummonerAdapter extends AttachedListener {
		SummonerAdapter(Entity entity) {
			super(entity.getUniqueId());
		}

		//This method will be called when the entity corresponding to this adapter died
		@Override
		public void onEntityDeath(EntityDeathEvent event) {
			//We notify the corresponding hunter that an entity died he at least damaged once
			onSummorKill(event);
		}
	}
}
