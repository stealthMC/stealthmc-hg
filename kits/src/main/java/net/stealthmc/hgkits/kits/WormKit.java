package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class WormKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You like dirt...",
			CC.gray + "Breaking dirt gives you health,",
			CC.gray + "food and even damage resistance!",
			CC.gray + "You also take zero fall damage,",
			CC.gray + "if you land on dirt!"
	);

	private int dirtMined = 0;

	public WormKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.DIRT, 0, CC.gold + "Worm", description);
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			dirtMined = Math.max(0, dirtMined - 3);
		});
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {
		if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
			return;
		}
		Block block = event.getClickedBlock();
		if (block == null || block.getType() != Material.DIRT) {
			return;
		}
		event.setCancelled(true);
		block.setType(Material.AIR);
		dirtMined = Math.min(15, dirtMined + 1);
		dirtBonus(event.getPlayer());
		block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.DIRT));
		block.getWorld().playSound(block.getLocation(), Sound.BLOCK_GRAVEL_BREAK, 1f, 1f);
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (event.getCause() != EntityDamageEvent.DamageCause.FALL) {
			return;
		}
		LivingEntity entity = (LivingEntity) event.getEntity();
		if (isDirtNearby(entity.getLocation().subtract(0, 0.2, 0))) {
			event.setCancelled(true);
		}
	}

	private void dirtBonus(Player player) {
		if (dirtMined >= 1) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION,
					80, 2, false, true));
		}
		if (dirtMined >= 2) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION,
					80, 2, false, true));
		}
		if (dirtMined >= 5) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE,
					80, 0, false, true));
		}
	}

	private boolean isDirtNearby(Location location) {
		for (int x = -1; x < 2; x++) {
			for (int z = -1; z < 2; z++) {
				if (location.clone().add(x, 0, z).getBlock().getType() == Material.DIRT) {
					return true;
				}
			}
		}
		return false;
	}

}
