package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.specialblocks.LauncherSpecialBlock;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class LauncherKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Reach to the sky with your launchpads.",
			CC.gray + "But be careful: if you hide them under",
			CC.gray + "a block, you could break your bones on",
			CC.gray + "impact with the ground!"
	);

	public LauncherKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.SPONGE, 0, CC.gold + "Launcher", description);
		ItemStack startItem = LauncherSpecialBlock.LAUNCHER_ITEM.clone();
		startItem.setAmount(20);
		getStartingItems().add(startItem);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.getType() == Material.SPONGE);
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().getType() == Material.SPONGE) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		Material blockType = block.getType();
		ItemStack wetSpongeFix = LauncherSpecialBlock.LAUNCHER_ITEM.clone();

		if (!KitsMain.getInstance().hasKit(player, LauncherKit.class)) {
			return;
		}
		if (blockType != Material.WET_SPONGE) {
			return;
		}

		block.getDrops().clear();
		block.setType(Material.AIR);

		wetSpongeFix.setAmount(1);
		block.getWorld().dropItemNaturally(LocationUtils.toCenterLocation(block), new ItemStack(wetSpongeFix));
	}
}
