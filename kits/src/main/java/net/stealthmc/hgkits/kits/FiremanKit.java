package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class FiremanKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Fire is a cold breeze for you, and only tickles.",
			CC.gray + "Lava only does half of the usual damage.",
			CC.gray + "You also spawn with a water bucket!"
	);

	private static final ItemStack ABILITY_ITEM = new ItemStack(Material.WATER_BUCKET);

	public FiremanKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.WATER_BUCKET, 0, CC.gold + "Fireman", description);
		getStartingItems().add(ABILITY_ITEM);
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (event.getCause() == EntityDamageEvent.DamageCause.FIRE) {
			event.setCancelled(true);
			return;
		}
		if (event.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK) {
			event.setCancelled(true);
			return;
		}
		if (event.getCause() == EntityDamageEvent.DamageCause.LAVA) {
			double damage = event.getDamage();
			damage *= 0.5;
			event.setDamage(damage);
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
