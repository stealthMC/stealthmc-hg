package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/*

Coded by petomka | PAID

 */

public class JackhammerKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Remove entire pillars of blocks along",
			CC.gray + "the vertical axis depending on your",
			CC.gray + "looking direction.",
			CC.gray + "",
			CC.gray + "Remember: Never dig straight down!"
	);

	private static final int MAX_USES = 6;
	private static final int COOLDOWN = 20;

	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	/*The chance of a broken block to drop it's items, from 0.0 to 1.0*/
	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	private static final double DROP_ITEM_CHANCE = 0.33;

	private static ItemStack ABILITY_ITEM;

	static {
		ABILITY_ITEM = Kit.createItemStack(Material.STONE_AXE, CC.red + "The Jackhammer");
		ItemMeta meta = ABILITY_ITEM.getItemMeta();
		meta.addEnchant(Enchantment.DURABILITY, 3, true);
		ABILITY_ITEM.setItemMeta(meta);
	}

	private int uses = 0;
	private int cooldownSeconds = 0;

	public JackhammerKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.STONE_AXE, 0, CC.gold + "Jackhammer", description);
		getStartingItems().add(ABILITY_ITEM);
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			if (uses < MAX_USES) {
				return;
			}
			cooldownSeconds -= 1;
			if (cooldownSeconds == 0) {
				uses = 0;
			}
		});
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		if (GameHandler.getInstance().getCurrentPhase().isJoiningEnabled()) {
			return;
		}
		ItemStack itemStack = event.getPlayer().getInventory().getItemInMainHand();
		if (!itemStack.isSimilar(ABILITY_ITEM)) {
			return;
		}
		if (uses >= MAX_USES) {
			KitsMain.sendCooldownMessage(event.getPlayer(), cooldownSeconds);
			CooldownTitleAnimation.play(event.getPlayer(), cooldownSeconds);
			return;
		}
		uses += 1;
		if (uses >= MAX_USES) {
			cooldownSeconds = COOLDOWN;
		}
		boolean up = event.getPlayer().getLocation().getPitch() < 0;
		event.setCancelled(true);
		dig(event.getBlock(), up);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		if (event.getItem().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	private void dig(Block origin, boolean up) {
		if (origin.getType() == Material.BEDROCK) {
			return;
		}
		if (ThreadLocalRandom.current().nextDouble() <= DROP_ITEM_CHANCE) {
			if (origin.getType() != Material.AIR) {
				origin.breakNaturally();
			}
		} else {
			origin.setType(Material.AIR);
		}
		int add = up ? 1 : -1;
		if (origin.getY() + add < 0 || origin.getY() + add >= 255) {
			return;
		}
		Block next = origin.getRelative(0, add, 0);
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(),
				() -> dig(next, up), 1L);
	}

}
