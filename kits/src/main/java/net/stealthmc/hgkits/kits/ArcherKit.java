package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableMap;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgcommon.util.MapUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.entity.HGEntity;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.projectiles.ProjectileSource;

import javax.annotation.Nullable;
import java.util.*;

/*

Coded by Fundryi & petomka

 */

public class ArcherKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Wow a bow, with knockback!",
			CC.gray + "You get a free arrow if you hit someone.",
			CC.gray + "You will find more flint & feathers."
	);

	private static final Map<Material, ItemStack> specialBlockBreaks = ImmutableMap.of(
			Material.GRAVEL, new ItemStack(Material.FLINT, 1)
	);

	private static final Map<EntityType, Map<Material, Material>> SWAPPED_ITEMS = MapUtils.of(HashMap::new,
			EntityType.CHICKEN, ImmutableMap.of(Material.FEATHER, Material.FEATHER),
			EntityType.SKELETON, ImmutableMap.of(Material.BONE, Material.ARROW)
	);

	private static ItemStack ABILITY_ITEM;

	static {
		ABILITY_ITEM = Kit.createItemStack(Material.BOW, CC.red + "Sniper Bow");
		ItemMeta meta = ABILITY_ITEM.getItemMeta();
		meta.addEnchant(Enchantment.ARROW_KNOCKBACK, 1, true);
		ABILITY_ITEM.setItemMeta(meta);
	}

	private Set<UUID> attachedEntities = new HashSet<>();

	public ArcherKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.BOW, 0, CC.gold + "Archer", description);
		getStartingItems().add(ABILITY_ITEM);
		getStartingItems().add(new ItemStack(Material.ARROW, 10));
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		specialBlockBreaks.computeIfPresent(event.getBlock().getType(), (material, itemStack) -> {
			event.setCancelled(true);
			Location target = LocationUtils.toCenterBlockLocation(event.getBlock());
			event.getBlock().getWorld().dropItemNaturally(target, itemStack);
			event.getPlayer().playSound(target, Sound.BLOCK_GRAVEL_BREAK, 1f, 1f);
			event.getBlock().setType(Material.AIR);
			return itemStack;
		});
	}

	@Override
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if (event.getEntity().getType() != EntityType.ARROW) {
			return;
		}
		ProjectileSource source = event.getEntity().getShooter();
		if (!(source instanceof Player)) {
			return;
		}
		Player shooter = (Player) source;
		HGEntity entity = HGEntity.from(event.getEntity());
		entity.getAttachedListeners().add(
				new AttachedListener(event.getEntity().getUniqueId()) {

					@Override
					public void onEntityDamage(EntityDamageByEntityEvent event) {
						if (!event.getDamager().getUniqueId().equals(getEntityId())) {
							return;
						}
						if (!(event.getEntity() instanceof LivingEntity)) {
							return;
						}
						shooter.getInventory().addItem(new ItemStack(Material.ARROW));
					}
				}
		);
	}

	private void onArcherKill(EntityDeathEvent event) {
		//the stored information is now useless, the entity died.
		attachedEntities.remove(event.getEntity().getUniqueId());
		//The killer could also be someone else! It is just guaranteed that the player at least damaged
		//the now dead entity, we don't know if he is the killer until we checked.
		Player killer = event.getEntity().getKiller();
		if (killer == null) {
			return;
		}
		if (!killer.getUniqueId().equals(this.getEntityId())) {
			return;
		}
		//Replace items corresponding to the now dead entity's type.
		Map<Material, Material> replaceMap = SWAPPED_ITEMS.get(event.getEntityType());
		if (replaceMap == null) {
			return;
		}
		event.getDrops().replaceAll(itemStack -> {
			Material newMaterial = replaceMap.get(itemStack.getType());
			if (newMaterial == null) {
				return itemStack;
			}
			int amount = 2; //itemStack.getAmount();
			return new ItemStack(newMaterial, amount);
		});
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		//Could also be called if the player using this kit was damaged!
		//-> Check if the player using this kit was actually the one damaging.
		if (!event.getDamager().getUniqueId().equals(this.getEntityId())) {
			return;
		}
		//Adding the HunterAdapter more than once to the damaged entity would
		//result in wasted computation time (it's probably insignificant, but
		//nevertheless)
		if (!attachedEntities.add(event.getEntity().getUniqueId())) {
			return;
		}
		//Add the HunterAdapter so we know when the entity that was damaged by
		//the player using the hunter kit died.
		HGEntity hgEntity = HGEntity.from(event.getEntity());
		hgEntity.getAttachedListeners().add(new ArcherKit.ArcherAdapter(event.getEntity()));
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		if (event.getItem().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	private class ArcherAdapter extends AttachedListener {
		ArcherAdapter(Entity entity) {
			super(entity.getUniqueId());
		}

		//This method will be called when the entity corresponding to this adapter died
		@Override
		public void onEntityDeath(EntityDeathEvent event) {
			//We notify the corresponding hunter that an entity died he at least damaged once
			onArcherKill(event);
		}
	}
}
