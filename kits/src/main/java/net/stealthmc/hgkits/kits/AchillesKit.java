package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class AchillesKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "The only weakness you have is Wood.",
			CC.gray + "Wooden weapons do more damage to you,",
			CC.gray + "but every other weapon does less!",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final double AttackDamageModifier = 1.1;
	private static final double DefendDamageModifier = 0.75;
	private int hitCount = 0;
	private int hitCountW = 0;

	public AchillesKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.WOODEN_SWORD, 0, CC.gold + "Achilles", description);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {

		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}

		Player attacker = (Player) event.getDamager();
		Player kitUser = (Player) event.getEntity();
		if (attacker.getUniqueId().equals(super.getEntityId())) { //FILTERS KIT USER OUT AS ATTACKER
			return;
		}
		if (attacker.getInventory().getItemInMainHand().getType().name().toLowerCase().contains("wood")) {
			event.setDamage(event.getDamage() * AttackDamageModifier);
			if (hitCountW >= 2) { //EVERY 3th hit
				kitUser.getWorld().playEffect(kitUser.getLocation().add(0, 1, 0), Effect.MOBSPAWNER_FLAMES, 10);
				attacker.playSound(attacker.getLocation(), Sound.ENTITY_BAT_HURT, 0.1f, 0.5f);
				hitCountW = 0;
				return;
			}
			hitCountW += 1;
		} else {
			event.setDamage(event.getDamage() * DefendDamageModifier);
			if (hitCount >= 2) { //EVERY 3th hit
				kitUser.getWorld().playEffect(kitUser.getLocation().add(0, 1, 0), Effect.VILLAGER_PLANT_GROW, 60);
				attacker.playSound(attacker.getLocation(), Sound.ENTITY_IRON_GOLEM_HURT, 0.1f, 1.0f);
				PlayerUtils.sendMessage(attacker, CC.gold + "A " + CC.red + "Wooden" + CC.gold + " weapon would deal more damage to this guy...");
				hitCount = 0;
				return;
			}
			hitCount += 1;
		}
	}

}
