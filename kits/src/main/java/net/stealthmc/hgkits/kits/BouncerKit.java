package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class BouncerKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "I guess you bounce a lot?",
			CC.gray + "You take no fall damage and will bounce around!",
			CC.gray + "Sneaking prevents you from bouncing but",
			CC.gray + "you will receive 50% of the normal fall damage!"
	);

	public BouncerKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.SLIME_BLOCK, 0, CC.gold + "Bouncer", description);
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (!(event.getCause() == EntityDamageEvent.DamageCause.FALL)) {
			return;
		}
		Player player = (Player) event.getEntity();
		if (player.isSneaking()) {
			double damage = event.getDamage();
			damage *= 0.5;
			event.setDamage(damage);
			player.getWorld().playEffect(player.getLocation().add(0, 2, 0), Effect.ENDER_SIGNAL, 0);
			return;
		}

		player.getWorld().playEffect(player.getLocation().add(0, 2, 0), Effect.WITHER_SHOOT, 0);
		double velHeight = player.getVelocity().getY() + (player.getFallDistance() / 12);
		Vector vector = player.getEyeLocation().getDirection();
		vector.multiply(2);
		vector.setY(velHeight);
		player.setVelocity(vector);
		event.setCancelled(true);
	}
}
