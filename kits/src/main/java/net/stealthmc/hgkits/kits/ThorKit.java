package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class ThorKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Allows you to create lightning strikes!"
	);

	private static final int COOLDOWN = 6;
	private static ItemStack ABILITY_ITEM;

	static {
		ABILITY_ITEM = Kit.createItemStack(Material.WOODEN_AXE, CC.red + "Thor's Hammer");
	}

	private int cooldownSeconds = 0;

	public ThorKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.WOODEN_AXE, 0, CC.gold + "Thor", description);
		getStartingItems().add(ABILITY_ITEM);
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			cooldownSeconds = Math.max(0, cooldownSeconds - 1);
		});
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {

		Player player = event.getPlayer();
		ItemStack inHand = event.getPlayer().getInventory().getItemInMainHand();
		if (!inHand.isSimilar(ABILITY_ITEM)) {
			return;
		}
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}
		if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
			PlayerUtils.sendMessage(event.getPlayer(), CC.red + "You can't use this yet!");
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
			return;
		}
		if (cooldownSeconds > 0) {
			KitsMain.sendCooldownMessage(event.getPlayer(), cooldownSeconds);
			CooldownTitleAnimation.play(event.getPlayer(), cooldownSeconds);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
			return;
		}
		if (event.getClickedBlock() == null) {
			return;
		}
		StrikeHighestBlockWithLightning(player, event.getClickedBlock());
	}

	private void StrikeHighestBlockWithLightning(Player player, Block block) {
		Block highestBlock = block.getWorld().getHighestBlockAt(block.getX(), block.getZ());

		for (Entity nearbyEntity : block.getWorld().getNearbyEntities(highestBlock.getLocation(), 2, 2, 2)) {
			if (nearbyEntity instanceof Player) {
				Player strikedPlayer = (Player) nearbyEntity;
				if (strikedPlayer != player && !KitsMain.getInstance().hasKit(strikedPlayer, FiremanKit.class) && player.canSee(strikedPlayer)) {
					if (strikedPlayer.getLocation().getY() >= 80) {
						strikedPlayer.damage(4, player);
					} else {
						strikedPlayer.damage(4, player);
						strikedPlayer.setVelocity(strikedPlayer.getVelocity().multiply(0.5));
					}
				}
			} else if (nearbyEntity instanceof LivingEntity) {
				LivingEntity strikedEntity = (LivingEntity) nearbyEntity;
				if (strikedEntity.getLocation().getY() >= 80) {
					strikedEntity.damage(10, player);
				} else {
					strikedEntity.damage(10, player);
					strikedEntity.setVelocity(strikedEntity.getVelocity().multiply(0.5));
				}
			}
		}

		//GETTO FIX UNTIL KICK IS FIXED
		//highestBlock.getWorld().spawnParticle(Particle.DRAGON_BREATH, highestBlock.getLocation(), 10);
		//highestBlock.getWorld().spawnParticle(Particle.EXPLOSION_NORMAL, highestBlock.getLocation(), 10);
		//highestBlock.getWorld().playSound(highestBlock.getLocation(), Sound.ENTITY_LIGHTNING_BOLT_THUNDER, 10, 1);
		highestBlock.getWorld().strikeLightningEffect(highestBlock.getLocation());

		for (int y = 0; y < 255; y++) {
			Location loc = highestBlock.getLocation().clone();
			loc.setY(y);
			Block yBlock = loc.getBlock();
			if (yBlock.getType() == Material.NETHERRACK) {
				thorChainExplosion(yBlock);
			}
		}
		// Got exploded by thor.
		cooldownSeconds = COOLDOWN;
		if (highestBlock.getRelative(BlockFace.DOWN).getType() == Material.AIR) {
			return;
		}
		if (highestBlock.getLocation().getY() >= 80) {
			if (highestBlock.getType() != Material.BEDROCK && highestBlock.getType() != Material.CHEST) {
				highestBlock.setType(Material.NETHERRACK);
				highestBlock.getRelative(BlockFace.UP).setType(Material.FIRE);
			}
		} else {
			highestBlock.setType(Material.FIRE);
		}
	}

	private void thorChainExplosion(Block block) {
		if (block.getType() != Material.NETHERRACK)
			return;
		if (block.getLocation().getY() < 80)
			return;
		block.setType(Material.AIR);
		for (int x = -3; x <= 3; x++) {
			for (int y = -3; y <= 3; y++) {
				for (int z = -3; z <= 3; z++) {
					Block faced = block.getRelative(x, y, z);
					if (faced.getType() == Material.NETHERRACK) {
						thorChainExplosion(faced);
					}
				}
			}
		}
		block.getWorld().createExplosion(block.getLocation(), 2.5F);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		if (event.getItem().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
