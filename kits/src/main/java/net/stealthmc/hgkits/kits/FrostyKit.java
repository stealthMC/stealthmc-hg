package net.stealthmc.hgkits.kits;

import com.google.common.collect.ImmutableMap;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.entity.HGEntity;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class FrostyKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You like it cold.",
			CC.gray + "Beeing on snow gives you speed.",
			CC.gray + "Throwing your snowballs on normal blocks",
			CC.gray + "will create a snow layer but if you",
			CC.gray + "throw them into Water/Lava something",
			CC.gray + "different will happen!"
	);

	private static final Map<Material, ItemStack> specialBlockBreaks = ImmutableMap.of(
			Material.SNOW, new ItemStack(Material.SNOWBALL, 1),
			Material.SNOW_BLOCK, new ItemStack(Material.SNOWBALL, 4)
	);
	private HashSet<Block> WaterStationary;
	private HashSet<Block> WaterFlowing;
	private HashSet<Block> LavaStationary;
	private HashSet<Block> LavaFlowing;

	public FrostyKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.ICE, 0, CC.gold + "Frosty", description);
		getStartingItems().add(new ItemStack(Material.SNOWBALL, 8));
	}

	@Override
	public void onProjectileHit(ProjectileHitEvent event) {
		if (event.getEntityType() == EntityType.SNOWBALL) {
			Snowball snowball = (Snowball) event.getEntity();
			if (snowball.getShooter() instanceof Player) {
				Block block = snowball.getLocation().getBlock();// .getRelative(BlockFace.UP);
				if (block.getType() == Material.AIR && block.getRelative(BlockFace.DOWN).getType() != Material.AIR)
					block.setType(Material.SNOW);
				WaterStationary = new HashSet<>();
				WaterFlowing = new HashSet<>();
				LavaStationary = new HashSet<>();
				LavaFlowing = new HashSet<>();
				ChangeWater(block, 0);
				for (Block b : WaterStationary) {
					b.setType(Material.ICE);
				}
				for (Block b : WaterFlowing) {
					b.setType(Material.SNOW);
				}
				for (Block b : LavaStationary) {
					b.setType(Material.MOSSY_COBBLESTONE);
				}
				for (Block b : LavaFlowing) {
					b.setType(Material.COBBLESTONE);
				}
			}
		}
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		specialBlockBreaks.computeIfPresent(event.getBlock().getType(), (material, itemStack) -> {
			event.setCancelled(true);
			Location target = LocationUtils.toCenterBlockLocation(event.getBlock());
			event.getBlock().getWorld().dropItemNaturally(target, itemStack);
			event.getPlayer().playSound(target, Sound.BLOCK_SNOW_BREAK, 1f, 1f);
			return itemStack;
		});
	}

	private void ChangeWater(Block block, int distance) {
		if (distance > 10)
			return;
		if (WaterStationary.contains(block) || WaterFlowing.contains(block) ||
				LavaStationary.contains(block) || LavaFlowing.contains(block) ||
				(block.getType() != Material.WATER && block.getType() != Material.LAVA)) {
			return;
		}
		if (block.getType() == Material.WATER) {
			WaterFlowing.add(block);
			WaterStationary.add(block);
		} else if (block.getType() == Material.LAVA) {
			LavaFlowing.add(block);
			LavaStationary.add(block);
		}

		ChangeWater(block.getRelative(BlockFace.UP), distance + 1);
		ChangeWater(block.getRelative(BlockFace.DOWN), distance + 1);
		ChangeWater(block.getRelative(BlockFace.NORTH), distance + 1);
		ChangeWater(block.getRelative(BlockFace.EAST), distance + 1);
		ChangeWater(block.getRelative(BlockFace.SOUTH), distance + 1);
		ChangeWater(block.getRelative(BlockFace.WEST), distance + 1);
	}

	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		Material walkingLocBlockUp = player.getLocation().getBlock().getType();
		Material walkingLocBlockDown = player.getLocation().add(0, -1, 0).getBlock().getType();
		Biome currentBiome = player.getLocation().getBlock().getBiome();

		if (currentBiome.name().startsWith("COLD") || currentBiome.name().startsWith("ICE") || currentBiome.name().startsWith("FROZEN")) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 3, 1));
		} else if (walkingLocBlockUp == Material.SNOW
				|| walkingLocBlockUp == Material.SNOW_BLOCK
				|| walkingLocBlockUp == Material.ICE
				|| walkingLocBlockUp == Material.BLUE_ICE
				|| walkingLocBlockUp == Material.FROSTED_ICE
				|| walkingLocBlockUp == Material.PACKED_ICE
				|| walkingLocBlockDown == Material.SNOW
				|| walkingLocBlockDown == Material.SNOW_BLOCK
				|| walkingLocBlockDown == Material.ICE
				|| walkingLocBlockDown == Material.BLUE_ICE
				|| walkingLocBlockDown == Material.FROSTED_ICE
				|| walkingLocBlockDown == Material.PACKED_ICE) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 3, 1));
		}
	}

	@Override
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if (event.getEntity().getType() != EntityType.SNOWBALL) {
			return;
		}
		ProjectileSource source = event.getEntity().getShooter();
		if (!(source instanceof Player)) {
			return;
		}
		Player shooter = (Player) source;
		HGEntity entity = HGEntity.from(event.getEntity());
		entity.getAttachedListeners().add(
				new AttachedListener(event.getEntity().getUniqueId()) {

					@Override
					public void onEntityDamage(EntityDamageByEntityEvent event) {
						if (!event.getDamager().getUniqueId().equals(getEntityId())) {
							return;
						}
						if (!(event.getEntity() instanceof LivingEntity)) {
							return;
						}
						event.setCancelled(true);
						((LivingEntity) event.getEntity()).damage(0, shooter);
						((LivingEntity) event.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 3 * 20, 0));
					}
				}
		);
	}
}
