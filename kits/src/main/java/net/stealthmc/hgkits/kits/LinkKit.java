package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.model.SpecialBlock;
import net.stealthmc.hgkits.specialblocks.LinkSpecialBlock;
import net.stealthmc.hgkits.specialblocks.SpecialBlockHandler;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class LinkKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You start with 2 spawners and a breaking stick!",
			CC.gray + "Your spawners are connected,",
			CC.gray + "if you place them in the world",
			CC.gray + "and then right-click on them, ",
			CC.gray + "you will get teleported to the other spawner.",
			CC.gray + "Only your breaking stick can break them!"
	);

	private static final ItemStack ABILITY_ITEM_SPAWNER;

	private static final ItemStack ABILITY_ITEM_REMOVE_STICK;

	static {
		ABILITY_ITEM_SPAWNER = Kit.createItemStack(Material.SPAWNER, CC.red + "Teleporter");
		ABILITY_ITEM_SPAWNER.setAmount(2);

		ABILITY_ITEM_REMOVE_STICK = Kit.createItemStack(Material.STICK, CC.red + "Breaker Stick" + CC.gray + " | " + CC.green + "Left Click on Block!");
		ItemMeta meta = ABILITY_ITEM_REMOVE_STICK.getItemMeta();
		meta.addEnchant(Enchantment.DURABILITY, 1, true);
		ABILITY_ITEM_REMOVE_STICK.setItemMeta(meta);
	}

	private LinkSpecialBlock waitingLinkBlock;

	public LinkKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.SPAWNER, 0, CC.gold + "Link", description);
		getStartingItems().add(ABILITY_ITEM_SPAWNER);
		getStartingItems().add(ABILITY_ITEM_REMOVE_STICK);
	}

	@Override
	public void onBlockPlace(BlockPlaceEvent event) {
		if (event.getBlock().getType() != Material.SPAWNER) {
			return;
		}
		LinkSpecialBlock placedLink = new LinkSpecialBlock(event.getBlock(), event.getPlayer().getUniqueId());

		CreatureSpawner spawner = (CreatureSpawner) event.getBlock().getState();
		spawner.setSpawnedType(EntityType.AREA_EFFECT_CLOUD);
		spawner.setDelay(Integer.MAX_VALUE);
		spawner.update();

		SpecialBlockHandler.getSpecialBlocks().add(placedLink);
		if (waitingLinkBlock == null) {
			waitingLinkBlock = placedLink;
			PlayerUtils.sendMessage(event.getPlayer(), CC.green + "Successfully placed down the first link block! Place another one to create a link!");
			return;
		}
		if (waitingLinkBlock.getBlock().getType() != waitingLinkBlock.getBlockMaterial()) {
			PlayerUtils.giveOrDropAt(event.getPlayer(), ABILITY_ITEM_SPAWNER, 1);
			PlayerUtils.sendMessage(event.getPlayer(), CC.red + "Your previously placed link block does not longer exist. You received a new one.");
			return;
		}
		waitingLinkBlock.setLinkTarget(placedLink);
		placedLink.setLinkTarget(waitingLinkBlock);
		PlayerUtils.sendMessage(event.getPlayer(), CC.green + "Successfully created a link!");
		waitingLinkBlock = null;
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {
		if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
			return;
		}
		ItemStack inHand = event.getItem();
		if (inHand == null) {
			return;
		}
		if (!inHand.isSimilar(ABILITY_ITEM_REMOVE_STICK)) {
			return;
		}
		Block block = event.getClickedBlock();
		if (block == null) {
			return;
		}
		SpecialBlock specialBlock = SpecialBlockHandler.getSpecialBlockFrom(block).orElse(null);
		if (specialBlock == null) {
			return;
		}
		if (!(specialBlock instanceof LinkSpecialBlock)) {
			return;
		}
		if (!specialBlock.getBlockOwner().equals(event.getPlayer().getUniqueId())) {
			PlayerUtils.sendMessage(event.getPlayer(), CC.red + "You may not remove other player's teleporters.");
			return;
		}
		block.setType(Material.AIR);
		LinkSpecialBlock linkTarget = ((LinkSpecialBlock) specialBlock).getLinkTarget();
		if (linkTarget != null && linkTarget.getLinkTarget() == specialBlock) {
			linkTarget.setLinkTarget(null);
		}
		if (waitingLinkBlock == specialBlock) {
			waitingLinkBlock = null;
		} else {
			waitingLinkBlock = linkTarget;
		}
		PlayerUtils.giveOrDropAt(event.getPlayer(), ABILITY_ITEM_SPAWNER, 1);
		PlayerUtils.sendMessage(event.getPlayer(), CC.green + "A link has been severed.");
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(this::isStartingItem);
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (isStartingItem(event.getItemDrop().getItemStack())) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
