package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.CooldownTitleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class NinjaKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final int TELEPORT_DURATION = 10;
	private static final int LAST_TELEPORT_COOLDOWN_DURATION = 7;
	private static final Map<String, String> ninjaLastHit = new HashMap<>();
	private int TD_cooldownSeconds = 0;
	private int LTCD_cooldownSeconds = 0;

	public NinjaKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.RED_WOOL, 0, CC.gold + "Ninja", description);
		GameHandler.getInstance().getSecondsHandler().add(() -> {
			TD_cooldownSeconds = Math.max(0, TD_cooldownSeconds - 1);
			LTCD_cooldownSeconds = Math.max(0, LTCD_cooldownSeconds - 1);
		});
	}

	private static void clearNinjaAdapter(Player entity) {
		HGPlayer hgPlayer = HGPlayer.from(entity.getUniqueId());
		hgPlayer.getAttachedListeners().removeIf(playerAdapter -> playerAdapter instanceof NinjaKit.NinjaAdapter);
	}

	private static void addNinjaAdapter(Entity entity) {
		if (!(entity instanceof Player)) {
			return;
		}
		HGPlayer hgPlayer = HGPlayer.from(entity.getUniqueId());
		hgPlayer.getAttachedListeners().add(new NinjaKit(entity.getUniqueId()));
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		onNinjaLastHit(event);
		onNinjaDamage(event);
	}

	private void onNinjaLastHit(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		Player victim = (Player) event.getEntity();
		Player attacker = (Player) event.getDamager();
		ninjaLastHit.put(attacker.getName(), victim.getName());
		TD_cooldownSeconds = TELEPORT_DURATION;
	}

	private void onNinjaDamage(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		if (event.getEntity().getUniqueId().equals(super.getEntityId())) { //FILTERS KITUSER OUT AS VICTIM
			return;
		}

		addNinjaAdapter(event.getEntity());
	}

	@Override
	public void onSneak(PlayerToggleSneakEvent event) {
		Player player = event.getPlayer();
		String lastHitName = ninjaLastHit.get(player.getName());
		if (lastHitName == null) {
			return;
		}
		if (TD_cooldownSeconds > 0) {
			KitsMain.sendCooldownMessage(player, TD_cooldownSeconds);
			CooldownTitleAnimation.play(player, TD_cooldownSeconds);
			event.setCancelled(true);
			return;
		}
		if (LTCD_cooldownSeconds > 0) {
			KitsMain.sendCooldownMessage(player, LTCD_cooldownSeconds);
			CooldownTitleAnimation.play(player, LTCD_cooldownSeconds);
			event.setCancelled(true);
			return;
		}
		Player lastHit = Bukkit.getPlayerExact(lastHitName);
		if (lastHit == null || lastHit.isDead() || !lastHit.isOnline()) {
			player.sendMessage(CC.red + "Could not find your target.");
			return;
		}
		player.teleport(lastHit);
		ninjaLastHit.put(player.getName(), null);
		LTCD_cooldownSeconds = LAST_TELEPORT_COOLDOWN_DURATION;
	}

	private static class NinjaAdapter extends AttachedListener {
		NinjaAdapter(UUID playerToAdapt) {
			super(playerToAdapt);
		}

		@Override
		public void onDamage(EntityDamageEvent event) {
			if (!(event.getEntity() instanceof Player)) {
				return;
			}
			Player player = (Player) event.getEntity();
			if (!(player.getHealth() - event.getFinalDamage() <= 0)) {
				return;
			}
			Iterator<Map.Entry<String, String>> iterator = ninjaLastHit.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, String> next = iterator.next();
				String value = next.getValue();
				String key = next.getKey();
				if (value != null && value.equals(player.getName()) || key != null && key.equals(player.getName())) {
					iterator.remove();
					clearNinjaAdapter(player);
				}
			}
		}
	}
}
