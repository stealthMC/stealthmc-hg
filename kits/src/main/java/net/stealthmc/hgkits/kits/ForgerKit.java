package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class ForgerKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You can instantly smelt Iron Ore!",
			CC.gray + "All you need is 3 Coal for each Iron Ore!",
			CC.gray + "Just Click with your Coal on the Iron Ore."
	);

	public ForgerKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.COAL, 0, CC.gold + "Forger", description);
		getStartingItems().add(new ItemStack(Material.COAL, 3));
	}

	@Override
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getCursor() == null || event.getCurrentItem() == null) {
			return;
		}
		if (!(event.getWhoClicked() instanceof Player)) {
			return;
		}
		if (event.getWhoClicked().getGameMode() == GameMode.CREATIVE || event.getWhoClicked().getGameMode() == GameMode.SPECTATOR || event.getWhoClicked().getGameMode() == GameMode.ADVENTURE) {
			return;
		}

		ItemStack coal = null;
		ItemStack ore = null;

		if (event.getCursor().getType() == Material.COAL) {
			coal = event.getCursor();
		} else if (event.getCurrentItem().getType() == Material.COAL) {
			coal = event.getCurrentItem();
		}
		if (coal == null) {
			return;
		}

		for (int i = 0; i < 2; i++) {
			if (ore == null) {
				ore = event.getCurrentItem();
			} else {
				ore = event.getCursor();
			}

			if (ore.getType() == Material.IRON_ORE) {
				//event.setCursor(null); //NO ALTERNATIVE AVAILABLE?
				event.getWhoClicked().setItemOnCursor(null);
				event.setCurrentItem(null);

				int remainder = ore.getAmount() - coal.getAmount();

				if (remainder > 0) {
					event.getWhoClicked().getInventory().addItem(new ItemStack(Material.IRON_INGOT, coal.getAmount()));
				} else if (remainder <= 0) {
					event.getWhoClicked().getInventory().addItem(new ItemStack(Material.IRON_INGOT, ore.getAmount()));
				}
				if (remainder > 0) {
					event.getWhoClicked().getInventory().addItem(new ItemStack(Material.IRON_ORE, remainder));
				} else if (remainder < 0) {
					event.getWhoClicked().getInventory().addItem(new ItemStack(Material.COAL, coal.getAmount() - ore.getAmount()));
				}
			} else if (ore.getType() == Material.GOLD_ORE) {
				//event.setCursor(null); //NO ALTERNATIVE AVAILABLE?
				event.getWhoClicked().setItemOnCursor(null);
				event.setCurrentItem(null);

				int remainder = ore.getAmount() - coal.getAmount();

				if (remainder > 0) {
					event.getWhoClicked().getInventory().addItem(new ItemStack(Material.GOLD_INGOT, coal.getAmount()));
				} else if (remainder <= 0) {
					event.getWhoClicked().getInventory().addItem(new ItemStack(Material.GOLD_INGOT, ore.getAmount()));
				}
				if (remainder > 0) {
					event.getWhoClicked().getInventory().addItem(new ItemStack(Material.GOLD_ORE, remainder));
				} else if (remainder < 0) {
					event.getWhoClicked().getInventory().addItem(new ItemStack(Material.COAL, coal.getAmount() - ore.getAmount()));
				}
			}
		}
	}

}
