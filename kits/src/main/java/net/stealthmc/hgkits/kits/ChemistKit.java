package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class ChemistKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "You spawn with",
			CC.gray + "1x Instant Damage 2 Potion",
			CC.gray + "1x Poison 2 Potion that lasts 10 seconds.",
			CC.gray + "1x Weakness 2 Potion that lasts 15 seconds.",
			CC.gray + "You will get a restock every 10 minutes.",
			CC.gray + "",
			CC.gray + "If your Inventory is full, no restock!"
	);

	private static final int cooldownMinutes = 10 * 60;

	private static final ItemStack ABILITY_ITEM_HARM;
	private static final ItemStack ABILITY_ITEM_POISON;
	private static final ItemStack ABILITY_ITEM_WEAK;

	static {
		ABILITY_ITEM_HARM = new ItemStack(Material.SPLASH_POTION, 1);
		final PotionMeta potionMetaHarm = (PotionMeta) ABILITY_ITEM_HARM.getItemMeta();
		potionMetaHarm.setColor(Color.fromRGB(25, 0, 0));
		final PotionEffect harm = new PotionEffect(PotionEffectType.HARM, 1, 1);
		potionMetaHarm.addCustomEffect(harm, true);
		ABILITY_ITEM_HARM.setItemMeta(potionMetaHarm);

		ABILITY_ITEM_POISON = new ItemStack(Material.SPLASH_POTION, 1);
		final PotionMeta potionMetaPoison = (PotionMeta) ABILITY_ITEM_POISON.getItemMeta();
		potionMetaPoison.setColor(Color.GREEN);
		final PotionEffect poison = new PotionEffect(PotionEffectType.POISON, 15 * 20, 1);
		potionMetaPoison.addCustomEffect(poison, true);
		ABILITY_ITEM_POISON.setItemMeta(potionMetaPoison);

		ABILITY_ITEM_WEAK = new ItemStack(Material.SPLASH_POTION, 1);
		final PotionMeta potionMetaWeak = (PotionMeta) ABILITY_ITEM_WEAK.getItemMeta();
		potionMetaWeak.setColor(Color.NAVY);
		final PotionEffect weak = new PotionEffect(PotionEffectType.WEAKNESS, 5 * 20, 0);
		potionMetaWeak.addCustomEffect(weak, true);
		ABILITY_ITEM_WEAK.setItemMeta(potionMetaWeak);
	}

	private int cooldownSeconds = cooldownMinutes;


	public ChemistKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.POTION, 16428, CC.gold + "Chemist", description);
		getStartingItems().add(ABILITY_ITEM_HARM);
		getStartingItems().add(ABILITY_ITEM_POISON);
		getStartingItems().add(ABILITY_ITEM_WEAK);

		GameHandler.getInstance().getSecondsHandler().add(() -> {
			if (!GameHandler.getInstance().getCurrentPhase().isEntityDamageEnabled()) {
				return;
			}
			cooldownSeconds = Math.max(0, cooldownSeconds - 1);
			if (cooldownSeconds != 0) {
				return;
			}
			if (playerToAdapt == null) {
				return;
			}
			Player player = Bukkit.getPlayer(playerToAdapt);
			if (player == null) {
				return;
			}
			if (!KitsMain.getInstance().hasKit(player, this.getClass())) {
				return;
			}
			if (player.getInventory().firstEmpty() == -1) {
				PlayerUtils.sendMessage(player, CC.red + "Your Inventory was full! Your restock will be dropped where you stand.\n");
				player.getWorld().dropItemNaturally(player.getLocation().add(0, 1, 0), ABILITY_ITEM_HARM);
				player.getWorld().dropItemNaturally(player.getLocation().add(0, 1, 0), ABILITY_ITEM_POISON);
				player.getWorld().dropItemNaturally(player.getLocation().add(0, 1, 0), ABILITY_ITEM_WEAK);
			} else {
				player.getInventory().addItem(ABILITY_ITEM_HARM);
				player.getInventory().addItem(ABILITY_ITEM_POISON);
				player.getInventory().addItem(ABILITY_ITEM_WEAK);
			}
			cooldownSeconds = cooldownMinutes;
		});
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(this::isStartingItem);
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (isStartingItem(event.getItemDrop().getItemStack())) {
			event.setCancelled(true);
		}
	}
}
