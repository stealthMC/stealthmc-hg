package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class TurtleKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Sneak to get max half hearth",
			CC.gray + "damage of any kind.",
			CC.gray + "But keep in mind, while sneaking",
			CC.gray + "you can't deal any damage!"
	);
	private boolean needToBlock = false; //OFF because you cant deal damage when sneaking.
	private double maxTurtleDamage = 1;

	public TurtleKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.DIAMOND_CHESTPLATE, 0, CC.gold + "Turtle", description);
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		Player player = (Player) event.getDamager();
		if (!player.isSneaking()) {
			return;
		}
		if (needToBlock && !player.isBlocking()) {
			return;
		}
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}
		LivingEntity entity = (LivingEntity) event.getEntity();
		event.setCancelled(true);
		//entity.damage(0);
		entity.getWorld().playEffect(entity.getLocation().add(0, 2, 0), Effect.ENDER_SIGNAL, 3);
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		Player player = (Player) event.getEntity();
		if (!player.isSneaking()) {
			return;
		}
		if (player.getHealth() <= 1) {
			return;
		}
		if (needToBlock && !player.isBlocking()) {
			return;
		}
		if (event.getDamage() > maxTurtleDamage) {
			event.setDamage(maxTurtleDamage);
		}
		player.getWorld().playEffect(player.getLocation().add(0, 2, 0), Effect.MOBSPAWNER_FLAMES, 3);
	}

}
