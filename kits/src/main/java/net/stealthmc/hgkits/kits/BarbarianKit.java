package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class BarbarianKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Level Up your sword!",
			CC.gray + "You start with a Wooden sword,",
			CC.gray + "that you can upgrade to a diamond sword!",
			CC.gray + "Your weapon description says it all...",
			"",
			CC.red + CC.thickX + CC.gold + " This kit is WIP! " + CC.red + CC.thickX
	);

	private static final String ABILITY_ITEM_NAME = CC.red + "Tyrfing";
	private static ItemStack ABILITY_ITEM;

	static {
		ABILITY_ITEM = Kit.createItemStack(Material.WOODEN_SWORD, ABILITY_ITEM_NAME);
		ItemMeta meta = ABILITY_ITEM.getItemMeta();
		meta.addEnchant(Enchantment.DURABILITY, 1, true);
		final List<String> itemDescription = KitsMain.formatDescriptionAndTranslateCodes(
				CC.darkAqua + "",
				CC.darkAqua + "Unique sword",
				CC.darkAqua + "",
				CC.darkAqua + "Tyrfing will level up with you",
				CC.darkAqua + "you can follow it's progress by",
				CC.darkAqua + "looking at the durability.",
				CC.darkAqua + "Once the durability is full",
				CC.darkAqua + "Tyrfing will upgrade, becoming a",
				CC.darkAqua + "more powerful sword."

		);
		meta.setLore(itemDescription);
		ABILITY_ITEM.setItemMeta(meta);

		setItemDamage(ABILITY_ITEM, ABILITY_ITEM.getType().getMaxDurability());
	}

	public BarbarianKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.DIAMOND_SWORD, 0, CC.gold + "Barbarian", description);
		getStartingItems().add(ABILITY_ITEM);
	}

	private static boolean isAbilityItem(ItemStack itemStack) {
		if (itemStack == null) {
			return false;
		}
		ItemMeta meta = itemStack.getItemMeta();
		if (meta == null) {
			return false;
		}
		return meta.getDisplayName().equals(ABILITY_ITEM_NAME);
	}

	private static void setItemDamage(ItemStack item, int damage) {
		Damageable im = (Damageable) item.getItemMeta();
		im.setDamage(damage);
		item.setItemMeta((ItemMeta) im);
	}

	private static int getItemDamage(ItemStack item) {
		Damageable im = (Damageable) item.getItemMeta();
		return im.getDamage();
	}

	@Override
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (!(event.getDamager() instanceof Player)) {
			return;
		}

		Player player = (Player) event.getEntity();
		Player kitUser = (Player) event.getDamager();

		if (player.getUniqueId().equals(super.getEntityId())) { //FILTERS KITUSER OUT AS VICTIM
			return;
		}
		if (!(player.getHealth() - event.getFinalDamage() <= 0)) {
			return;
		}

		addPercentageToSword(kitUser, 0.15);
		//PlayerUtils.sendMessage(kitUser, "WORKED PLAYER");
	}

	@Override
	public void onPlayerExpChange(PlayerExpChangeEvent event) {
		if (event.getPlayer().isDead()) {
			return;
		}
		Player barbarian = event.getPlayer();
		double newExp = event.getAmount();
		addPercentageToSword(barbarian, newExp / 100);
		event.setAmount((int) ((double) event.getAmount() * 0.5));
	}

	private void addPercentageToSword(Player player, double percentage) {
		ItemStack sword = getSword(player);
		if (sword == null) {
			return;
		}
		int curr = getItemDamage(sword);
		int max = 0;
		if (sword.getType() == Material.DIAMOND_SWORD) {
			max = Material.IRON_SWORD.getMaxDurability();
		} else {
			max = Material.WOODEN_SWORD.getMaxDurability();
		}
		int added = (int) (max * percentage);
		int total = (short) (curr - added);
		// Transfer to next sword
		if (total < 0) {
			setItemDamage(sword, 0);
			checkSwordUpgrade(player);
			setItemDamage(sword, getItemDamage(sword) + total);
			return;
		}
		setItemDamage(sword, curr - added);
		checkSwordUpgrade(player);
	}

	private void checkSwordUpgrade(Player player) {
		ItemStack sword = getSword(player);
		if (sword == null) {
			return;
		}
		int curr = getItemDamage(sword);
		if (curr <= 0) {
			if (sword.getType() == Material.WOODEN_SWORD) {
				sword.setType(Material.STONE_SWORD);
				setItemDamage(sword, sword.getType().getMaxDurability());
			} else if (sword.getType() == Material.STONE_SWORD) {
				sword.setType(Material.IRON_SWORD);
				setItemDamage(sword, sword.getType().getMaxDurability());
			} else if (sword.getType() == Material.IRON_SWORD) {
				sword.setType(Material.DIAMOND_SWORD);
				setItemDamage(sword, sword.getType().getMaxDurability());
			} else if (sword.getType() == Material.DIAMOND_SWORD && sword.getEnchantments().size() == 1) {
				setItemDamage(sword, 0);
				int chance = (new Random()).nextInt(3);
				if (chance == 1) {
					sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
				} else if (chance == 2) {
					sword.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
				} else if (chance == 3) {
					sword.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 1);
				}
			}
		}
	}

	private ItemStack getSword(Player player) {
		for (ItemStack item : player.getInventory().getContents()) {
			if (isAbilityItem(item)) {
				return item;
			}
		}
		return null;
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		ItemStack itemStack = event.getItem();
		if (isAbilityItem(itemStack)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(BarbarianKit::isAbilityItem);
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		ItemStack dropped = event.getItemDrop().getItemStack();
		if (isAbilityItem(dropped)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}
}
