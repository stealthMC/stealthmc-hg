package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.game.CraftingRecipeHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class CamelKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Beeing in a hot climate gives you a speed!",
			CC.gray + "You can also craft soups from:",
			CC.gray + "1x Sand & 1x Cactus. No bowl needed!"
	);

	private static ShapelessRecipe camelSoupRecipe;

	static {
		camelSoupRecipe = new ShapelessRecipe(new NamespacedKey(HGBukkitMain.getInstance(), "CamelSoup"), new ItemStack(Material.MUSHROOM_STEW));
		camelSoupRecipe.addIngredient(1, Material.SAND);
		camelSoupRecipe.addIngredient(1, Material.CACTUS);

		CraftingRecipeHandler.registerRecipe(camelSoupRecipe);
		KitsMain.getInstance().getSpecialRecipes().add(camelSoupRecipe);
	}

	public CamelKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.SAND, 0, CC.gold + "Camel", description);
		getStartingItems().add(new ItemStack(Material.CACTUS, 1));
		getStartingItems().add(new ItemStack(Material.SAND, 1));
	}

	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		Material walkingLocBlock = player.getLocation().clone().subtract(0, 0.5, 0).getBlock().getType();
		Biome currentBiome = player.getLocation().getBlock().getBiome();

		if (currentBiome.name().contains("DESERT") || currentBiome.name().contains("BADLANDS")) {

			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 3 * 20, 0));

		} else if (walkingLocBlock.name().contains("SAND")) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 3 * 20, 0));
		}
	}

	@Override
	public boolean canCraftSpecialRecipe(Recipe recipe) {
		return recipe.equals(camelSoupRecipe);
	}

}
