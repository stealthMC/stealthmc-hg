package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by petomka | PAID

 */

public class KangarooKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "What sound does a kangaroo make, anyway?",
			CC.gray + "Use your rocket to launch yourself up.",
			CC.gray + "If you sneak while using the rocket,",
			CC.gray + "you will be propelled forwards instead.",
			CC.gray + "",
			CC.gray + "You also receive reduced fall damage."
	);

	private static final ItemStack ABILITY_ITEM = Kit.createItemStack(Material.FIREWORK_ROCKET,
			CC.red + "Kangarocket");

	private static final BlockFace[] fixCheckFaces = new BlockFace[]{BlockFace.NORTH, BlockFace.EAST,
			BlockFace.SOUTH, BlockFace.WEST};

	/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
	/*Upwards force to apply to a upwards jumping Player*/
	/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
	private static final double UPWARDS_FORCE = 0.85;

	/*++++++++++++++++++++++++++++++++++++++++++++*/
	/*Forwards force to apply to a sneaking Player*/
	/*++++++++++++++++++++++++++++++++++++++++++++*/
	private static final double FORWARDS_FORCE = 0.95;
	private int use = 0;
	private int immediateCooldown = 0;

	public KangarooKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.FIREWORK_ROCKET, 0, CC.gold + "Kangaroo", description);
		getStartingItems().add(ABILITY_ITEM);
		GameHandler.getInstance().getTicksHandler().add(() -> {
			if (getEntityId() == null) {
				return;
			}
			if (immediateCooldown > 0) {
				immediateCooldown -= 1;
				return;
			}
			if (use == 0) {
				return;
			}
			if (playerToAdapt == null) {
				return;
			}
			Player player = Bukkit.getPlayer(playerToAdapt);
			if (player == null) {
				return;
			}
			/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
			/*Adjust the y-value below to modify the scanning distance to scan for a block below a player.*/
			/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
			if (player.getLocation().subtract(0, 0.01, 0).getBlock().getType().isSolid()) {
				use = 0;
				immediateCooldown = 0;
			}
		});
	}

	@Override
	public void onInteract(PlayerInteractEvent event) {
		ItemStack itemStack = event.getItem();
		if (itemStack == null) {
			return;
		}
		if (!itemStack.isSimilar(ABILITY_ITEM)) {
			return;
		}
		event.setCancelled(true);
		Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
		/*Remove this condition to have no cooldown in between jumps*/
		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
		//if (immediateCooldown > 0) {
		//	return;
		//}
		if (GameHandler.getInstance().getCurrentPhase().isJoiningEnabled()) {
			return;
		}
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_AIR) {
			return;
		}
		if (use == 2) {
			return;
		}
		Vector dir;
		if (event.getPlayer().isSneaking()) {
			dir = event.getPlayer().getEyeLocation().getDirection();
			dir.setY(UPWARDS_FORCE * 0.9);
			dir = dir.normalize().multiply(FORWARDS_FORCE);
		} else {
			dir = new Vector(0, UPWARDS_FORCE, 0);
		}
		use += 1;
		immediateCooldown = 6;
		//event.getPlayer().setFallDistance((float) -UPWARDS_FORCE * 4);
		event.getPlayer().setVelocity(dir);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(ABILITY_ITEM));
	}

	@Override
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(ABILITY_ITEM)) {
			event.setCancelled(true);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), event.getPlayer()::updateInventory, 1L);
		}
	}

	@Override
	public void onDamage(EntityDamageEvent event) {
		if (event.getCause() == EntityDamageEvent.DamageCause.FALL) {
			double damage = event.getDamage();
			damage *= 0.75;
			event.setDamage(damage);
		}
	}

	private boolean shouldFixLocation(Player player) {
		Location location = player.getLocation();

		for (BlockFace checkFace : fixCheckFaces) {
			Block origin = location.getBlock();
			Block origin2 = location.clone().add(0, 1, 0).getBlock();
			Block toCheck1 = origin.getRelative(checkFace), toCheck2 = origin2.getRelative(checkFace);
			if (toCheck1.getType().isSolid() && toCheck2.getType().isSolid()) {
				return true;
			}
		}

		return false;
	}

	private void fixLocation(Player player) {
		player.teleport(player.getLocation());
	}

	private void softFixLocation(Player player) {
		if (shouldFixLocation(player)) {
			fixLocation(player);
		}
	}
}
