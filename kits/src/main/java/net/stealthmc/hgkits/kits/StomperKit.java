package net.stealthmc.hgkits.kits;

import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import javax.annotation.Nullable;
import java.util.List;
import java.util.UUID;

/*

Coded by Fundryi

 */

public class StomperKit extends Kit {

	private static final List<String> description = KitsMain.formatDescriptionAndTranslateCodes(
			CC.gray + "Fall damage you would receive",
			CC.gray + "will be transferred to the player",
			CC.gray + "in a 2x3x2 radius where you land!"
	);
	private static ItemStack STOMPER_BOOTS;

	static {
		STOMPER_BOOTS = Kit.createItemStack(Material.LEATHER_BOOTS, CC.red + "Stomper Sneakers");
		LeatherArmorMeta meta = (LeatherArmorMeta) STOMPER_BOOTS.getItemMeta();
		meta.setColor(Color.fromRGB(255, 0, 0));
		STOMPER_BOOTS.setItemMeta(meta);
	}

	public StomperKit(@Nullable UUID playerToAdapt) {
		super(playerToAdapt, Material.DIAMOND_BOOTS, 0, CC.gold + "Stomper", description);
	}

	@EventHandler(priority = EventPriority.HIGH)
	@Override
	public void onDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player)) {
			return;
		}
		if (event.getCause() != EntityDamageEvent.DamageCause.FALL) {
			return;
		}
		event.setDamage(4);
		Player stomper = (Player) event.getEntity();
		for (Entity entity : stomper.getNearbyEntities(2, 3, 2)) {
			if (entity instanceof Player) {

				Player stompedPlayer = (Player) entity;

				if (stompedPlayer.isSneaking()) {
					stompedPlayer.damage(6, stomper);
				} else {
					stompedPlayer.damage(event.getDamage(), stomper);
					stompedPlayer.damage(stomper.getFallDistance());
				}

			} else if (entity instanceof LivingEntity) {

				LivingEntity stompedEntity = (LivingEntity) entity;

				stompedEntity.damage(event.getDamage(), stomper);
				stompedEntity.damage(stomper.getFallDistance());

			}
			stomper.playSound(stomper.getLocation(), Sound.BLOCK_ANVIL_LAND, 1, 1);
		}
	}

	@Override
	public void onInventoryClick(InventoryClickEvent event) {
		PlayerInventory playerInventory = event.getWhoClicked().getInventory();
		if (!playerInventory.equals(event.getClickedInventory())) {
			return;
		}
		ItemStack itemStack = event.getCurrentItem();
		if (itemStack == null) {
			return;
		}
		if (!itemStack.isSimilar(STOMPER_BOOTS)) {
			return;
		}
		event.setCancelled(true);
	}

	@Override
	public void onDeath(PlayerDeathEvent event) {
		event.getDrops().removeIf(itemStack -> itemStack.isSimilar(STOMPER_BOOTS));
	}

	@Override
	public void onBattleReady() {
		super.onBattleReady();
		Player player = Bukkit.getPlayer(this.getEntityId());
		if (player == null) {
			return;
		}
		player.getInventory().setBoots(STOMPER_BOOTS);
	}

	@Override
	public void onItemDamage(PlayerItemDamageEvent event) {
		if (event.getItem().isSimilar(STOMPER_BOOTS)) {
			event.setCancelled(true);
		}
	}
}
