package net.stealthmc.hgkits.specialblocks;

import lombok.Getter;
import lombok.Setter;
import net.stealthmc.hgcommon.Permission;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgkits.model.SpecialBlock;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.util.Vector;

import java.util.UUID;

@Getter
public class LinkSpecialBlock extends SpecialBlock {

	private final Material blockMaterial;
	@Setter
	private LinkSpecialBlock linkTarget;

	public LinkSpecialBlock(Block block, UUID blockOwner) {
		super(block, blockOwner);
		this.blockMaterial = block.getType();
	}

	@Override
	public boolean isInProximity(Player player) {
		return false;
	}

	@Override
	public void onStepOver(Player stepped) {
		//noop
	}

	@Override
	public boolean shouldDispose() {
		return getBlock().getType() != blockMaterial;
	}

	@Override
	public void onRightClick(Player player) {
		if (getLinkTarget() == null) {
			PlayerUtils.sendMessage(player, ChatColor.RED + "No destination found!");
			return;
		}
		Location playerLocation = player.getLocation();
		Location targetLoc = LocationUtils.toCenterLocation(getLinkTarget().getBlock()).add(0, 1, 0);
		if (isUnsafe(targetLoc)) {
			targetLoc = getTargetLocation(playerLocation);
		}
		if (isUnsafe(targetLoc)) {
			PlayerUtils.sendMessage(player, ChatColor.RED + "The target location is unsafe.");
			return;
		}
		targetLoc.setYaw(playerLocation.getYaw());
		targetLoc.setPitch(playerLocation.getPitch());
		player.teleport(targetLoc);
		PlayerUtils.sendMessage(player, ChatColor.GREEN + "You have been teleported to this link's target!");
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		if (event.getPlayer().hasPermission(Permission.HG_GAME_ADMIN)) {
			return;
		}
		if (event.getPlayer().getUniqueId().equals(getBlockOwner())) {
			PlayerUtils.sendMessage(event.getPlayer(), ChatColor.RED + "You may remove this link with your link removal tool!");
		} else {
			PlayerUtils.sendMessage(event.getPlayer(), ChatColor.RED + "You cannot break this link!");
		}
		event.setCancelled(true);
	}

	private boolean isUnsafe(Location location) {
		Material targetMaterial = location.getBlock().getType();
		if (targetMaterial.isBlock() && targetMaterial.isSolid()) {
			return true;
		}

		location = location.clone().add(0, 1, 0);
		targetMaterial = location.getBlock().getType();

		return targetMaterial.isBlock() && targetMaterial.isSolid();
	}

	private Location getTargetLocation(Location relativeStart) {
		Vector rel = relativeStart.toVector().subtract(getBlock().getLocation().toVector());
		Location targetLoc = linkTarget.getBlock().getLocation();
		targetLoc.add(rel);
		return targetLoc;
	}

}
