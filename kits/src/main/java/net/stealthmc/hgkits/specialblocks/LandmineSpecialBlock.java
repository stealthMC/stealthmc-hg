package net.stealthmc.hgkits.specialblocks;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.stealthmc.hgkits.kits.DemomanKit;
import net.stealthmc.hgkits.model.SpecialBlock;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.material.PressureSensor;

import java.util.Arrays;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
public class LandmineSpecialBlock extends SpecialBlock {

	private boolean exploded = false;
	private Material originalMaterial;
	//private float yield = 5f;

	public LandmineSpecialBlock(Block block, UUID blockOwner) {
		super(block, blockOwner);
		originalMaterial = block.getType();
	}

	@Override
	public boolean isInProximity(Player player) {
		if (exploded) {
			return false;
		}
		if (!(getBlock().getType() == DemomanKit.defaultTrigger || Arrays.stream(DemomanKit.bonusTrigger).anyMatch(material -> getBlock().getType().equals(material)))) {
			return false;
		}
		if (player.getUniqueId().equals(getBlockOwner())) {
			return false;
		}
		PressureSensor pressurePlate = (PressureSensor) getBlock().getState().getData();
		return pressurePlate.isPressed() && super.isInProximity(player); //comment the latter out to also explode to animals
	}

	@Override
	public void onStepOver(Player stepped) {
		//Fires EntityExplodeEvent with sourceEntity = null
		if (exploded) {
			return;
		}
		getBlock().getWorld().createExplosion(getBlock().getLocation(), DemomanKit.explosionYield);
		exploded = true;
	}

	@Override
	public boolean shouldDispose() {
		return getBlock().getType() != originalMaterial || exploded;
	}

}
