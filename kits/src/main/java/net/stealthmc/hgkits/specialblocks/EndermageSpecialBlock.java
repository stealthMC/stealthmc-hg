package net.stealthmc.hgkits.specialblocks;

import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.SpecialBlock;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.UUID;

public class EndermageSpecialBlock extends SpecialBlock {

	private boolean teleported = false;

	private Material originalMaterial;
	private BlockState previousState;

	public EndermageSpecialBlock(Block block, UUID blockOwner, BlockState previous) {
		super(block, blockOwner);
		this.originalMaterial = block.getType();
		this.previousState = previous;
	}

	public static boolean isTargetBlocked(Location target) {
		return target.getBlock().getType().isSolid() ||
				target.clone().add(0, 1, 0).getBlock().getType().isSolid();
	}

	private static void teleportPlayerPreserveDirection(Player player, Location target) {
		Location playerLoc = player.getLocation();
		Location location = new Location(target.getWorld(), target.getX(), target.getY(), target.getZ(),
				playerLoc.getYaw(), playerLoc.getPitch());
		player.teleport(location);
	}

	@Override
	public boolean isInProximity(Player player) {
		Location playerLocation = player.getLocation();
		return getBlock().getX() - 1 <= playerLocation.getBlockX()
				&& playerLocation.getBlockX() <= getBlock().getX() + 1
				&& getBlock().getZ() - 1 <= playerLocation.getBlockZ()
				&& playerLocation.getBlockZ() <= getBlock().getZ() + 1
				&& (playerLocation.getBlockY() >= getBlock().getY() + 5
				|| playerLocation.getBlockY() <= getBlock().getY() - 5);
	}

	@Override
	public void onStepOver(Player stepped) {
		Player owner = Bukkit.getPlayer(getBlockOwner());
		if (owner == null) {
			return;
		}
		if (stepped.getUniqueId().equals(owner.getUniqueId())) {
			return;
		}
		Location target = getTeleportTarget();
		if (isTargetBlocked(target)) {
			return;
		}
		if (!teleported) {
			previousState.update(true, true);
			teleportPlayerPreserveDirection(owner, target);
			PlayerUtils.sendMessage(owner, ChatColor.RED + "Teleport successful. You are invincible " +
					"for 5 seconds. Prepare to fight!");
			HGPlayer hgOwner = HGPlayer.from(owner);
			hgOwner.getAttachedListeners().add(new EndermageAdapter(owner.getUniqueId()));
		}
		teleported = true;
		teleportPlayerPreserveDirection(stepped, target);
		PlayerUtils.sendMessage(stepped, ChatColor.RED + "You have been teleported by an Endermage! " +
				"You are invincible for 5 seconds. Prepare to fight!");
		HGPlayer hgStepped = HGPlayer.from(stepped);
		hgStepped.getAttachedListeners().add(new EndermageAdapter(stepped.getUniqueId()));
	}

	public Location getTeleportTarget() {
		return LocationUtils.toCenterLocation(getBlock()).add(0, 1.1, 0);
	}

	@Override
	public boolean shouldDispose() {
		return getBlock().getType() != originalMaterial || teleported;
	}

	public void remove() {
		if (teleported) {
			return;
		}
		teleported = true;
		previousState.update(true, true);
	}

	private static class EndermageAdapter extends AttachedListener {

		long startTime = System.currentTimeMillis();

		EndermageAdapter(UUID playerToAdapt) {
			super(playerToAdapt);
		}

		@Override
		public void onDamage(EntityDamageEvent event) {
			if (System.currentTimeMillis() - 5000 < startTime) {
				event.setCancelled(true);
				return;
			}
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () ->
					HGPlayer.from(getEntityId())
							.getAttachedListeners()
							.removeIf(playerAdapter -> playerAdapter == this), 1L);
		}
	}

}
