package net.stealthmc.hgkits.specialblocks;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import net.stealthmc.hgcommon.CC;
import net.stealthmc.hgcommon.bukkit.LocationUtils;
import net.stealthmc.hgcommon.util.MapUtils;
import net.stealthmc.hgcommon.util.MathUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.SpecialBlock;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class LauncherSpecialBlock extends SpecialBlock {

	public static final ItemStack LAUNCHER_ITEM;

	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	/*The default added acceleration per block per direction.   */
	/*Note: The acceleration multiplier is not linear!          */
	/*The multiplier is the harmonic series from 0 to n (blocks)*/
	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	private static final double ACCELERATION_PER_BLOCK = 1.5;

	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	/*4 is the default maximum value applied by the server.   */
	/*Having higher values may print out a lot of warnings to */
	/*the console, if the internal server maximum has not been*/
	/*adjusted prior to this value.                           */
	/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	private static final double MAX_ACCELERATION = 4;

	private static final Map<BlockFace, Vector> blockFaceDirections = MapUtils.of(Maps::newHashMap,
			BlockFace.EAST, new Vector(-ACCELERATION_PER_BLOCK, 0, 0),
			BlockFace.SOUTH, new Vector(0, 0, -ACCELERATION_PER_BLOCK),
			BlockFace.WEST, new Vector(ACCELERATION_PER_BLOCK, 0, 0),
			BlockFace.NORTH, new Vector(0, 0, ACCELERATION_PER_BLOCK),
			BlockFace.DOWN, new Vector(0, ACCELERATION_PER_BLOCK, 0)); //Not so much upwards
	private static Set<UUID> adapterJustAdded = Sets.newHashSet();

	static {
		LAUNCHER_ITEM = new ItemStack(Material.SPONGE);
		ItemMeta meta = LauncherSpecialBlock.LAUNCHER_ITEM.getItemMeta();
		meta.setDisplayName(CC.red + "Launchpad");
		LauncherSpecialBlock.LAUNCHER_ITEM.setItemMeta(meta);

		GameHandler.getInstance().getTicksHandler().add(() -> {
			GameHandler.getInstance().getHandlablePlayersStream()
					.filter(player -> !adapterJustAdded.contains(player.getUniqueId()))
					.filter(player -> player.getLocation().subtract(0, 0.5, 0).getBlock().getType().isSolid())
					.map(HGPlayer::from)
					.map(HGPlayer::getAttachedListeners)
					.filter(l -> l.stream().anyMatch(e -> e instanceof FallDamageLauncherAdapter))
					.forEach(list -> Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () -> {
						list.removeIf(adapter -> !adapterJustAdded.contains(adapter.getEntityId()) &&
								adapter instanceof FallDamageLauncherAdapter);
					}, 10L));
		});
	}

	private Block originBlock;
	private Material blockMaterial;

	public LauncherSpecialBlock(Block block, UUID blockOwner) {
		super(block, blockOwner);
		//necessary for correct/good fall damage prevention
		detectionRadiusSquared = 0.5;
		blockMaterial = block.getType();
		originBlock = block;
	}

	@Override
	public boolean shouldDispose() {
		return getBlock().getType() != blockMaterial;
	}

	@Override
	public void onStepOver(Player stepped) {
		Vector direction = calculateLaunchVector(originBlock);
		stepped.setVelocity(direction);

		boolean fallDamage = stepped.getLocation().getBlock().getType() != Material.AIR;
		if (!fallDamage) {
			adapterJustAdded.add(stepped.getUniqueId());
			HGPlayer hgPlayer = HGPlayer.from(stepped);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () -> {
				hgPlayer.getAttachedListeners().add(new FallDamageLauncherAdapter(hgPlayer.getEntityId()));
			}, 1L);
			Bukkit.getScheduler().runTaskLater(KitsMain.getInstance(), () ->
					adapterJustAdded.remove(stepped.getUniqueId()), 20L);
			return;
		}

		if (stepped.getUniqueId().equals(getBlockOwner())) {
			return;
		}

		class LauncherAdapter extends AttachedListener {

			private LauncherAdapter(UUID playerToAdapt) {
				super(playerToAdapt);
			}

			@Override
			public void onDamage(EntityDamageEvent event) {
				if (!event.getEntity().getUniqueId().equals(getEntityId())) {
					return;
				}
				HGPlayer hgPlayer = HGPlayer.from(event.getEntity().getUniqueId());
				hgPlayer.getAttachedListeners().removeIf(playerAdapter -> playerAdapter == LauncherAdapter.this);
				if (event.getCause() != EntityDamageEvent.DamageCause.FALL) {
					return;
				}
				hgPlayer.setLastDamager(getBlockOwner());
			}

		}

		HGPlayer hgStepped = HGPlayer.from(stepped);
		hgStepped.getAttachedListeners().add(new LauncherAdapter(stepped.getUniqueId()));
	}

	@Override
	public boolean isInProximity(Player player) {
		return super.isInProximity(player) ||
				super.isInProximity(player.getLocation().subtract(0, 0.5, 0)) && player.getLocation().getBlock().getType() != Material.AIR;
	}

	@Override
	public void onBlockBreak(BlockBreakEvent event) {
		event.setCancelled(true);
		event.getBlock().setType(Material.AIR);
		ItemStack drop = LauncherSpecialBlock.LAUNCHER_ITEM.clone();
		drop.setAmount(1);
		event.getBlock().getWorld().dropItemNaturally(LocationUtils.toCenterBlockLocation(event.getBlock()), drop);
	}

	private Vector calculateLaunchVector(Block origin) {
		Vector launchDirection = new Vector(0, ACCELERATION_PER_BLOCK / 3, 0);
		for (Map.Entry<BlockFace, Vector> entry : blockFaceDirections.entrySet()) {
			double mul = calcMulti(origin, entry.getKey());
			Vector result = entry.getValue().clone().multiply(mul);
			result.setX(MathUtils.clamp(-MAX_ACCELERATION, MAX_ACCELERATION, result.getX()));
			result.setY(MathUtils.clamp(-MAX_ACCELERATION, MAX_ACCELERATION, result.getY()));
			result.setZ(MathUtils.clamp(-MAX_ACCELERATION, MAX_ACCELERATION, result.getZ()));
			launchDirection.add(entry.getValue().clone().multiply(mul));
		}
		return launchDirection;
	}

	private double calcMulti(Block origin, BlockFace direction) {
		double multi = 0;
		int iteration = 1;
		while (origin.getType() == blockMaterial) {
			origin = origin.getRelative(direction);

			//Harmonic series
			multi = multi + 1D / iteration;
			++iteration;
		}
		return multi;
	}

	private static class FallDamageLauncherAdapter extends AttachedListener {

		public FallDamageLauncherAdapter(UUID playerToAdapt) {
			super(playerToAdapt);
		}

		@Override
		public void onDamage(EntityDamageEvent event) {
			if (!event.getEntity().getUniqueId().equals(getEntityId())) {
				return;
			}
			if (event.getCause() != EntityDamageEvent.DamageCause.FALL) {
				return;
			}
			event.setCancelled(true);
			HGPlayer hgPlayer = HGPlayer.from(getEntityId());
			hgPlayer.getAttachedListeners().removeIf(adapter -> adapter == this);
		}

	}

}
