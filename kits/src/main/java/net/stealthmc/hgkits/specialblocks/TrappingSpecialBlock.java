package net.stealthmc.hgkits.specialblocks;

import net.stealthmc.hgkits.model.SpecialBlock;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.UUID;

public class TrappingSpecialBlock extends SpecialBlock {

	private boolean toWeb = false;

	public TrappingSpecialBlock(Block block, UUID blockOwner) {
		super(block, blockOwner);
		detectionRadiusSquared = 1;
	}

	@Override
	public void onStepOver(Player stepped) {
		if (toWeb || stepped.getUniqueId().equals(this.getBlockOwner())) {
			return;
		}
		this.getBlock().setType(Material.COBWEB);
		stepped.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 3 * 20, 0));
		stepped.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 3 * 20, 2));
		stepped.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 4 * 20, 0));
		toWeb = true;
	}

	@Override
	public boolean shouldDispose() {
		return super.getBlock().getType() == Material.COBWEB || toWeb;
	}
}
