package net.stealthmc.hgkits.specialblocks;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.experimental.UtilityClass;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgkits.model.SpecialBlock;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@UtilityClass
public class SpecialBlockHandler {

	@Getter
	private List<SpecialBlock> specialBlocks = Lists.newArrayList();

	public Optional<SpecialBlock> getSpecialBlockFrom(Block block) {
		return specialBlocks.stream()
				.filter(specialBlock -> specialBlock.getBlock().equals(block))
				.findFirst();
	}

	public Map<Player, List<SpecialBlock>> checkProximitiesForPlayingPlayers() {
		return checkProximities(GameHandler.getInstance().getPlayingPlayersStream());
	}

	public Map<Player, List<SpecialBlock>> checkProximities(Stream<Player> toCheckFor) {
		return toCheckFor
				.collect(Collectors.toMap(p -> p, SpecialBlockHandler::checkProximitiesFor));
	}

	public List<SpecialBlock> checkProximitiesFor(Player player) {
		return specialBlocks.stream()
				.filter(block -> block.isInProximity(player))
				.collect(Collectors.toList());
	}

}
