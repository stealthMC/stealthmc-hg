package net.stealthmc.hgkits.model;

import com.google.common.collect.Lists;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import net.stealthmc.hgcommon.bukkit.inventory.ItemStackUtils;
import net.stealthmc.hgcommon.util.ListUtils;
import net.stealthmc.hgcore.api.AttachedListener;
import net.stealthmc.hgcore.game.WeaponDamageHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.KitsMain;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Derivative classes that are registered via KitsMain#registerKit must
 * offer a single argument constructor that solely takes the UUID of the
 * player that owns the kit.
 */
@SuppressWarnings("Lombok") @Data
@EqualsAndHashCode(callSuper = false, of = {"kitUUID"})
public abstract class Kit extends AttachedListener {

	private static final String PREPEND_DESCRIPTION = ChatColor.LIGHT_PURPLE + "Description:";

	//Gets appended to lore if a player does not own a Kit
	private static final String NOT_SELECTABLE_LORE = ChatColor.RED + "You do not own this kit.";

	//Gets appended to lore if a player does own a Kit
	private static final String OWN_KIT_LORE = ChatColor.GREEN + "You own this kit.";

	@Getter(value = AccessLevel.PRIVATE)
	private final UUID kitUUID = UUID.randomUUID();
	private Material icon;
	private int iconDamage;
	private String displayName;
	private List<String> description;

	private List<ItemStack> startingItems = new ArrayList<>();

	public Kit(@Nullable UUID playerToAdapt,
			   @Nonnull Material icon,
			   int iconDamage,
			   @Nonnull String displayName,
			   @Nonnull List<String> description) {
		super(playerToAdapt);
		this.icon = icon;
		this.iconDamage = iconDamage;
		this.displayName = displayName;
		this.description = description;
	}

	public static ItemStack createItemStack(Material material, String displayName) {
		ItemStack result = ItemStackUtils.createItemStack(material, displayName);
		result = WeaponDamageHandler.processItemStack(result);
		return result;
	}

	public ItemStack constructIconItemStack(Player player) {
		ItemStack icon = new ItemStack(this.getIcon(), 1);
		ItemMeta meta = icon.getItemMeta();
		if (meta == null) {
			return icon;
		}
		meta.setDisplayName(this.getDisplayName());

		List<String> lore = ListUtils.prependCopy(Lists::newArrayList, PREPEND_DESCRIPTION, description);
		String append = getLoreAppendLine(player);
		if (append != null) {
			lore.add("");
			lore.add(append);
		}
		meta.setLore(lore);

		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS);
		icon.setItemMeta(meta);
		return icon;
	}

	@Override
	public void onBattleReady() {
		HGPlayer hgPlayer = HGPlayer.from(getEntityId());
		Player player = hgPlayer.getPlayer();
		if (player == null) {
			KitsMain.getInstance().getLogger().log(Level.SEVERE, "Could not ready player for battle, UUID=" + getEntityId());
			return;
		}
		player.getInventory().addItem(startingItems.stream()
				.map(WeaponDamageHandler::processItemStack)
				.toArray(ItemStack[]::new));
	}

	public boolean canCraftSpecialRecipe(Recipe recipe) {
		return false;
	}

	public boolean canChangeKit() {
		return false;
	}

	public boolean isStartingItem(ItemStack itemStack) {
		return getStartingItems().stream()
				.anyMatch(startingItem -> startingItem.isSimilar(itemStack));
	}

	//For latter ease of use
	public boolean canSelect(Player player) {
		return player.hasPermission(getPermission());
	}

	public String getLoreAppendLine(Player player) {
		if (player.hasPermission(getPermission())) {
			return OWN_KIT_LORE;
		}
		//For latter ease of use
		return NOT_SELECTABLE_LORE;
	}

	public String getPermission() {
		String permSuffix = this.getClass().getSimpleName().toLowerCase();
		if (permSuffix.endsWith("kit")) {
			permSuffix = permSuffix.substring(0, permSuffix.length() - 3);
		}
		return "hg.kits." + permSuffix;
	}

}
