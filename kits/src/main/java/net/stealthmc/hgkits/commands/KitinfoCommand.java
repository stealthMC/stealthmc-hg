package net.stealthmc.hgkits.commands;

import com.google.common.collect.ImmutableList;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.util.CommandUtils;
import net.stealthmc.hgcommon.util.Menu;
import net.stealthmc.hgkits.KitsMain;
import net.stealthmc.hgkits.model.Kit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.UUID;

public class KitinfoCommand implements TabExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (args.length >= 1) {
			onKitInfo(sender, CommandUtils.joinArgs(args, 0, args.length, " "));
			return true;
		}

		onSyntax(sender, alias);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
		String token = args[args.length - 1].toLowerCase();

		return KitCommand.completeKits(token);
	}

	private void onSyntax(CommandSender sender, String alias) {
		PlayerUtils.sendMessage(sender, CommandUtils.getSyntax(alias, ImmutableList.of("<Kit>")));
	}

	private void onKitInfo(CommandSender sender, String kitName) {
		Class<? extends Kit> chosen = KitsMain.getInstance().getByAlias(kitName);
		if (chosen == null) {
			PlayerUtils.sendMessage(sender, ChatColor.RED + "Kit " + ChatColor.WHITE + kitName + ChatColor.RED + " not found!");
			return;
		}
		try {
			Kit kit = chosen.getConstructor(UUID.class).newInstance(new Object[]{null});
			Menu menu = new Menu(ChatColor.GOLD + "Kitinfo: " + KitsMain.getInstance().getPrimaryKitNameNormalized(kit));
			kit.getDescription().stream()
					.map(Menu::new)
					.forEach(menu::addSub);
			PlayerUtils.sendMessage(sender, menu.toComponents());
		} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

}
