package net.stealthmc.hgkits;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import net.stealthmc.hgcommon.bukkit.PlayerUtils;
import net.stealthmc.hgcommon.bukkit.scoreboard.PlayerScoreboard;
import net.stealthmc.hgcommon.bukkit.scoreboard.ScoreboardLine;
import net.stealthmc.hgcommon.util.StringUtils;
import net.stealthmc.hgcore.HGBukkitMain;
import net.stealthmc.hgcore.game.GameHandler;
import net.stealthmc.hgcore.game.ScoreboardHandler;
import net.stealthmc.hgcore.game.entity.HGPlayer;
import net.stealthmc.hgkits.commands.KitCommand;
import net.stealthmc.hgkits.commands.KitinfoCommand;
import net.stealthmc.hgkits.kits.*;
import net.stealthmc.hgkits.listeners.BlockListener;
import net.stealthmc.hgkits.listeners.PlayerListener;
import net.stealthmc.hgkits.model.Kit;
import net.stealthmc.hgkits.tasks.LookingAtKitTask;
import net.stealthmc.hgkits.tasks.StepOnSpecialBlockDetectorTask;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Recipe;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class KitsMain extends JavaPlugin {

	@Getter
	private static KitsMain instance;

	@Getter
	private Map<Class<? extends Kit>, Function<UUID, Kit>> registeredKits = Maps.newHashMap();

	@Getter
	private Map<Class<? extends Kit>, List<String>> registeredKitNames = Maps.newHashMap();

	@Getter
	private List<Kit> dummyKits = Lists.newArrayList();

	@Getter
	private List<Recipe> specialRecipes = Lists.newArrayList();

	@Getter
	private Map<UUID, ScoreboardLine> kitStartLines = Maps.newHashMap();

	public static List<String> formatDescriptionAndTranslateCodes(String... lines) {
		for (int i = 0; i < lines.length; i++) {
			lines[i] = PlayerUtils.colorize(lines[i]);
		}
		return Lists.newArrayList(lines);
	}

	public static void forXYZBlocks(World world, int xFrom, int xTo, int yFrom, int yTo, int zFrom, int zTo, Consumer<Block> consumer) {
		for (int x = xFrom; x < xTo; x++) {
			for (int y = yFrom; y < yTo; y++) {
				for (int z = zFrom; z < zTo; z++) {
					Block block = world.getBlockAt(x, y, z);
					consumer.accept(block);
				}
			}
		}
	}

	public static void sendCooldownMessage(Player player, int cooldown) {
		PlayerUtils.sendMessage(player, ChatColor.RED + "This ability is on cooldown for " +
				cooldown + " " + HGBukkitMain.SECONDS_PHRASES.get(cooldown) + ".");
	}

	public void onEnable() {
		instance = this;

		registerCommands();
		registerTasks();
		registerListeners();
		registerKits();
	}

	public void onDisable() {

	}

	private void registerCommands() {
		getCommand("kit").setExecutor(new KitCommand());
		getCommand("kitinfo").setExecutor(new KitinfoCommand());
	}

	private void registerTasks() {
		new StepOnSpecialBlockDetectorTask().runTaskTimer(this, 1L, 1L);
		new LookingAtKitTask().runTaskTimerAsynchronously(this, 5L, 5L);
	}

	private void registerListeners() {
		PluginManager manager = Bukkit.getPluginManager();
		manager.registerEvents(new PlayerListener(), this);
		manager.registerEvents(new BlockListener(), this);
	}

	private void registerKits() {
		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
		/*The first given kit name will be used for display on scoreboard!*/
		/*Use lowercase! Case is automatically corrected.                 */
		/*Colors are added later.                                         */
		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

		registerKit(AchillesKit.class, "achilles");
		registerKit(AnchorKit.class, "anchor");
		registerKit(ArcherKit.class, "archer");
		registerKit(BarbarianKit.class, "babarian", "baba");
		registerKit(BouncerKit.class, "bouncer");
		registerKit(BoxerKit.class, "boxer");
		registerKit(CamelKit.class, "camel");
		registerKit(CannibalKit.class, "cannibal", "canni");
		registerKit(ChemistKit.class, "chemist");
		registerKit(CrafterKit.class, "crafter");
		registerKit(CultivatorKit.class, "cultivator", "culti");
		registerKit(DemomanKit.class, "demoman", "demo");
		registerKit(DiggerKit.class, "digger");
		registerKit(DoctorKit.class, "doctor");
		registerKit(DwarfKit.class, "dwarf");
		registerKit(EndermageKit.class, "endermage", "mage", "ender mage");
		registerKit(FiremanKit.class, "fireman", "fire");
		//registerKit(GliderKit.class, "glider");
		registerKit(FishermanKit.class, "fisherman", "fisher");
		registerKit(ForgerKit.class, "forger");
		registerKit(FrostyKit.class, "frosty", "frost");
		registerKit(GrandpaKit.class, "grandpa", "opa");
		registerKit(HulkKit.class, "hulk");
		registerKit(HunterKit.class, "hunter");
		registerKit(JackhammerKit.class, "jackhammer", "jack");
		registerKit(JumperKit.class, "jumper");
		registerKit(KangarooKit.class, "kangaroo", "kanga");
		registerKit(KayaKit.class, "kaya");
		registerKit(LauncherKit.class, "launcher", "launch");
		registerKit(LinkKit.class, "link");
		registerKit(LumberjackKit.class, "lumberjack", "lumber");
		registerKit(MagmaKit.class, "magma");
		registerKit(MinerKit.class, "miner");
		registerKit(MonkKit.class, "monk");
		//registerKit(NeoKit.class, "neo");
		//registerKit(NinjaKit.class, "ninja");
		registerKit(PoseidonKit.class, "poseidon");
		registerKit(PurgerKit.class, "purger");
		registerKit(ReaperKit.class, "reaper");
		registerKit(RedstonerKit.class, "redstone", "redstoner");
		registerKit(ScoutKit.class, "scout");
		registerKit(SnailKit.class, "snail");
		//registerKit(SpecialistKit.class, "specialist", "special");
		registerKit(StomperKit.class, "stomper", "stomp");
		registerKit(SummonerKit.class, "summoner");
		registerKit(SwitcherKit.class, "switcher", "switch");
		registerKit(TankKit.class, "tank");
		registerKit(ThermoKit.class, "thermo");
		registerKit(ThorKit.class, "thor");
		registerKit(TrapperKit.class, "trapper", "trap");
		registerKit(TurtleKit.class, "turtle");
		//registerKit(UrgalKit.class, "urgal");
		registerKit(VacuumKit.class, "vacuum", "vacum");
		registerKit(VikingKit.class, "viking");
		registerKit(ViperKit.class, "viper");
		registerKit(WormKit.class, "worm");
	}

	/**
	 * Registers a kit. Registered kits will be selectable via command. <br>
	 * If kitNames is not empty, the kit command can also take an argument for this
	 * kit to be selected. If more than one name is given, all names will be
	 * available to the kit command. Only the first given name will be shown in the </br>
	 * scoreboard. <br><br>
	 * <p>
	 * If kitNames is however left empty, the given classes simple name will be added to the
	 * kitNames list. If the class ends with "Kit", this ending will be stripped.<br><br>
	 *
	 * <b>It's important to enter the kit names in <u>LOWERCASE!!!</u></b>
	 *
	 * @param kitClass The class of that contains the code for the Kit to register
	 * @param kitNames The name and aliases of this kit
	 */
	private void registerKit(Class<? extends Kit> kitClass, String... kitNames) {
		try {
			Constructor<? extends Kit> kitConstructor = kitClass.getConstructor(UUID.class);
			Kit dummyKit = kitConstructor.newInstance(new Object[]{null});
			dummyKits.add(dummyKit);
			List<String> aliases = Lists.newArrayList(kitNames);
			if (aliases.isEmpty()) {
				String name = kitClass.getSimpleName();
				if (name.endsWith("Kit")) {
					name = name.substring(0, name.length() - 3);
				}
				name = name.toLowerCase();
				aliases.add(name);
			}
			registeredKitNames.put(kitClass, Lists.newArrayList(kitNames));
			Function<UUID, Kit> kitFunction = uuid -> {
				try {
					return kitConstructor.newInstance(uuid);
				} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
					getLogger().log(Level.SEVERE, "Error instantiating Kit", e);
				}
				return null;
			};
			registeredKits.put(kitClass, kitFunction);
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			getLogger().log(Level.SEVERE, "Error registering Kit", e);
		}
	}

	public Class<? extends Kit> getByAlias(String kitName) {
		return getRegisteredKitNames().entrySet().stream()
				.filter(entry -> entry.getValue().contains(kitName.toLowerCase()))
				.map(Map.Entry::getKey)
				.findAny()
				.orElse(null);
	}

	public ScoreboardLine getKitStartScoreboardLine(Player player) {
		PlayerScoreboard scoreboard = ScoreboardHandler.getPlayerScoreboard(player);
		return kitStartLines.compute(player.getUniqueId(), (uuid, line) -> {
			ScoreboardLine oldLine = line;
			if (oldLine == null || oldLine.isDisposed()) {
				oldLine = scoreboard.getLine(GameHandler.INITIAL_SCOREBOARD_TIMER_TITLE_LINE)
						.belowBlock();
			}
			return oldLine;
		});
	}

	public List<String> getPlayerKitNames(Player player) {
		HGPlayer hgPlayer = HGPlayer.from(player);
		return hgPlayer.getAttachedListeners().stream()
				.filter(playerAdapter -> playerAdapter instanceof Kit)
				.map(playerAdapter -> (Kit) playerAdapter)
				.map(kit -> registeredKitNames.get(kit.getClass()))
				/*This line should never do anything*/
				//.filter(list -> list.size() > 0)
				/*This line should always be safe*/
				.map(list -> list.get(0))
				.map(StringUtils::firstLetterUpper)
				.collect(Collectors.toList());
	}

	public List<Class<? extends Kit>> getKits(Player player) {
		HGPlayer hgPlayer = HGPlayer.from(player);
		return hgPlayer.getAttachedListeners().stream()
				.filter(playerAdapter -> playerAdapter instanceof Kit)
				.map(playerAdapter -> (Kit) playerAdapter)
				.map(Kit::getClass)
				.collect(Collectors.toList());
	}

	public boolean hasKit(Player player, Class<? extends Kit> kitClass) {
		return getKits(player).contains(kitClass);
	}

	public void updatePlayerKitScoreboard(Player player) {
		List<String> kits = KitsMain.getInstance().getPlayerKitNames(player);
		if (kits.isEmpty()) {
			kits.add(ChatColor.RED + "None");
		}
		ScoreboardLine line = KitsMain.getInstance().getKitStartScoreboardLine(player)
				.update("Kit");
		for (String kit : kits) {
			line = line.getBelow();
			line.update(ChatColor.GOLD + kit);
		}
		line.makeSpaceBelow();
	}

	public String getPrimaryKitNameNormalized(Kit kit) {
		return registeredKitNames.entrySet().stream()
				.filter(entry -> entry.getKey().equals(kit.getClass()))
				.map(Map.Entry::getValue)
				.map(list -> list.get(0))
				.map(StringUtils::firstLetterUpper)
				.findAny()
				.orElse("null");
	}

}
